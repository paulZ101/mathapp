<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/activityLogs.php'; 

$page['title'] = 'Activity Logs';
$label = 'results';


authorize('admin'); 
 

$database = new Database();
$db = $database->getConnection(); 

$ActivityLogs = new ActivityLogs($db);

$ActivityLogs->account_type = 'admin';
$ActivityLogs->account_id = $_SESSION['mathapp']['login']['admin'];

$stmt = $ActivityLogs->get();  
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "id" => $id,
            "activity" => $activity, 
            "description" => $description, 
            "date_added" => $date_added
        );
        
        array_push($dataArray["body"], $e);
    } 
}
 
$page['datatable'] = true;
 
require_once DOCUMENT_ROOT.'system/pages/admin/header.php';
require_once DOCUMENT_ROOT.'system/pages/admin/activity_logs.php';
require_once DOCUMENT_ROOT.'system/pages/admin/footer.php';
