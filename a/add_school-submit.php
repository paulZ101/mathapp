<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/schools.php';

authorize('admin');

$label = 'school';
$main_page = $label.'s.php';

if (empty($_POST['school_name'])) {
    $_SESSION['mathapp']['error'] = 'Please enter a school_name'; 
    header('location: '.FILE_BASENAME);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$$label = new Schools($db);
$$label->school_name = $_POST['school_name'];

$$label->getByName();

if($$label->school_name != null){
    $_SESSION['mathapp']['error'] = 'school_name already exists';
    header('location: '.FILE_BASENAME);
    exit;
}

$$label->school_name = $_POST['school_name'];
$$label->school_address = $_POST['school_address'];   
$$label->school_id = $_POST['school_id'];
$$label->status = $_POST['status'];   
$$label->admin_id = $_SESSION['mathapp']['login']['admin']; 
$$label->language = $_POST['language'];  
 
if($$label->create()){
    $_SESSION['mathapp']['success'] = 'school created successfully.';
} else {
    $_SESSION['mathapp']['error'] = 'school could not be created.';
}

header('location: '.FILE_BASENAME);