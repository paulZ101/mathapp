<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/teachers.php';

authorize('admin');

$label = 'teacher';
$main_page = $label.'s.php';

if (empty($_POST['email'])) {
    $_SESSION['mathapp']['error'] = 'Please enter a email'; 
    header('location: '.FILE_BASENAME);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$$label = new Teachers($db);
$$label->email = $_POST['email'];

$$label->getByEmail();

if($$label->email != null){
    $_SESSION['mathapp']['error'] = 'email already exists';
    header('location: '.FILE_BASENAME);
    exit;
}


$$label->email = $_POST['email'];
$$label->first_name = $_POST['first_name'];
$$label->last_name = $_POST['last_name'];  
$$label->teacher_id = $_POST['teacher_id'];  
$$label->status = $_POST['status'];  
$$label->password = password_hash($_POST['password'], PASSWORD_DEFAULT);  
$$label->school_reg_id = $_POST['school_id'];  
$$label->admin_id = $_SESSION['mathapp']['login']['admin']; 
 
if($$label->create()){
    $_SESSION['mathapp']['success'] = $label.' created successfully.';
} else {
    $_SESSION['mathapp']['error'] = $label.' could not be created.';
}

header('location: '.FILE_BASENAME);