<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/teachers.php'; 

authorize('admin');

$label = 'teacher';
$main_page = $label.'s.php';


if(empty($_GET['id'])){
    header('location:'.$main_page);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_GET['id']);

$database = new Database();
$db = $database->getConnection();

$$label = new Teachers($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

if($$label->admin_id != $_SESSION['mathapp']['login']['admin']){
    $_SESSION['mathapp']['error'] = 'invalid request';
    header('location: '.$main_page);
    exit;
}

if($$label->delete()){ 
    $_SESSION['mathapp']['success'] = $label.' deleted successfully.';
} else { 
    $_SESSION['mathapp']['error'] = $label.' could not be deleted.';
}

header('location: '.$main_page);