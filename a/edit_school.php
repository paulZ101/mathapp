<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/schools.php'; 

$page['title'] = 'Edit School';

authorize('admin');

$label = 'school';
$main_page = $label.'s.php';

if(empty($_GET['id'])){
    header('location:'.$main_page);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_GET['id']);

$database = new Database();
$db = $database->getConnection();

$$label = new Schools($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

$data = array(
    "reg_id" =>  $$label->reg_id,
    "school_id" => $$label->school_id,
    "school_name" => $$label->school_name,
    "school_address" => $$label->school_address,
    "status" => $$label->status,
    "language" => $$label->language
 
); 

require_once DOCUMENT_ROOT . 'system/pages/admin/header.php';
require_once DOCUMENT_ROOT . 'system/pages/admin/add_school.php';
require_once DOCUMENT_ROOT . 'system/pages/admin/footer.php';