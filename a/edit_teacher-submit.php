<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/teachers.php'; 

authorize('admin');

$label = 'teacher';
$main_page = $label.'s.php';

if(empty($_POST['id'])){
    header('location: '.$main_page);
    exit;
} 

$decryptedID = encrypt_decrypt('decrypt', $_POST['id']);

if (empty($_POST['email'])) {
    $_SESSION['mathapp']['error'] = 'Please enter a email'; 
    header('location: '.FILE_BASENAME.'?id='.$decryptedID);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$$label = new Teachers($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

$$label->email = $_POST['email'];
$$label->getByEmail();

if($$label->email != null AND $$label->email != $_POST['email']){
    $_SESSION['mathapp']['error'] = 'email already exists';
    header('location: '.FILE_BASENAME.'?id='.$_POST['id']);
    exit;
}

 
$$label->email = $_POST['email'];
$$label->first_name = $_POST['first_name'];
$$label->last_name = $_POST['last_name'];  
$$label->teacher_id = $_POST['teacher_id'];  
$$label->status = $_POST['status'];  
$$label->password = !empty($_POST['password']) ? password_hash($_POST['password'], PASSWORD_DEFAULT) : '';  
$$label->school_reg_id = $_POST['school_id'];   

$$label->reg_id = $decryptedID;  

if($$label->update()){
    $_SESSION['mathapp']['success'] = $label.' updated successfully.';
} else {
    $_SESSION['mathapp']['error'] = $label.' could not be updated.';
}

header('location: '.FILE_BASENAME.'?id='.$_POST['id']);