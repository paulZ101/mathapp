<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/teachers.php'; 
require_once DOCUMENT_ROOT.'system/classes/schools.php'; 

$page['title'] = 'Edit Teacher';

authorize('admin');

$label = 'teacher';
$main_page = $label.'s.php';

if(empty($_GET['id'])){
    header('location:'.$main_page);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_GET['id']);

$database = new Database();
$db = $database->getConnection();

$$label = new Teachers($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

$data = array(
    "reg_id" =>  $$label->reg_id,
    "email" => $$label->email,
    "first_name" => $$label->first_name,
    "last_name" => $$label->last_name,
    "teacher_id" => $$label->teacher_id,
    "status" => $$label->status,
    "password" => $$label->password,
    "school_id" => $$label->school_reg_id,
    "admin_id" => $$label->admin_id
 
);  


# SCHOOLS
$school = new Schools($db); 
$stmt = $school->getActive();  
$schoolCount = $stmt->rowCount();

$schoolArr = array();
$schoolArr["body"] = array();
$schoolArr["itemCount"] = $schoolCount;

if ($schoolCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "school_name" => $school_name
        );
        
        array_push($schoolArr["body"], $e);
    } 
}

require_once DOCUMENT_ROOT . 'system/pages/admin/header.php';
require_once DOCUMENT_ROOT . 'system/pages/admin/add_teacher.php';
require_once DOCUMENT_ROOT . 'system/pages/admin/footer.php';