<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/schools.php'; 
require_once DOCUMENT_ROOT.'system/classes/teachers.php'; 

$page['title'] = 'Admin Dashboard';

authorize('admin');


$database = new Database();
$db = $database->getConnection(); 


$schools = new Schools($db); 
$stmt = $schools->get();  
$schoolsCount = $stmt->rowCount();


$teachers = new Teachers($db); 
$stmt = $teachers->get();  
$teachersCount = $stmt->rowCount();

require_once DOCUMENT_ROOT . 'system/pages/admin/header.php';
require_once DOCUMENT_ROOT . 'system/pages/admin/index.php';
require_once DOCUMENT_ROOT . 'system/pages/admin/footer.php';