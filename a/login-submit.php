<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/admin.php'; 
require_once DOCUMENT_ROOT.'system/classes/activityLogs.php'; 


if (empty($_POST['email']) || empty($_POST['password'])) {
    $_SESSION['mathapp']['error'] = 'Please enter your email and password'; 
    header('location: '.FILE_BASENAME);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$admin = new Admin($db);
$admin->email = $_POST['email'];  
$admin->getByEmail(); 

if($admin->id == null){
    echo $_SESSION['mathapp']['error'] = 'wrong email or password';
    header('location: '.FILE_BASENAME);
    exit;
}  

if($admin->status != 1){
    echo $_SESSION['mathapp']['error'] = 'invalid request';
    header('location: '.FILE_BASENAME);
    exit;
} 

if(!password_verify($_POST['password'], $admin->password)){
    echo $_SESSION['mathapp']['error'] = 'wrong email or password';
    header('location: '.FILE_BASENAME);
    exit;
}


$logs = new ActivityLogs($db);
$logs->account_type = 'admin'; 
$logs->account_id = $admin->id;   
$logs->activity = 'admin login'; 
$logs->description = REQUEST_URI;
$logs->ip_address = '';
$logs->create(); 
 
$_SESSION['mathapp']['login']['admin'] = $admin->id;
$_SESSION['mathapp']['login']['account_name'] = 'Administrator';
 
header('location: '.ADMIN_DASHBOARD);