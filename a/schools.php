<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/schools.php'; 

$page['title'] = 'Schools';
$page['label'] = "school";

authorize('admin');

$database = new Database();
$db = $database->getConnection(); 

$school = new Schools($db); 
$stmt = $school->get();  
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "school_name" => $school_name, 
            "school_id" => $school_id,
            "school_address" => $school_address, 
            "date_added" => $date_added,
            "status" => $status 
        );
        
        array_push($dataArray["body"], $e);
    } 
}

$page['datatable'] = true;

require_once DOCUMENT_ROOT . 'system/pages/admin/header.php';
require_once DOCUMENT_ROOT . 'system/pages/admin/schools.php';
require_once DOCUMENT_ROOT . 'system/pages/admin/footer.php';