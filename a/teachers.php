<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/teachers.php'; 

$page['title'] = 'Teachers';
$page['label'] = "teacher";

authorize('admin');

$database = new Database();
$db = $database->getConnection(); 

$teacher = new Teachers($db); 
$stmt = $teacher->get();  
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "email" => $email, 
            "password" => $password,
            "first_name" => $first_name, 
            "last_name" => $last_name,
            "teacher_id" => $teacher_id,
            "admin_id" => $admin_id,
            "school_reg_id" => $school_reg_id,
            "reg_type" => $reg_type,
            "date_added" => $date_added,
            "status" => $status ,
            "school_name" => $school_name
        );
        
        array_push($dataArray["body"], $e);
    } 
}

$page['datatable'] = true;

require_once DOCUMENT_ROOT . 'system/pages/admin/header.php';
require_once DOCUMENT_ROOT . 'system/pages/admin/teachers.php';
require_once DOCUMENT_ROOT . 'system/pages/admin/footer.php';