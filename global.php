<?php
# /global.php 
session_start();

ini_set('display_errors', 0);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

$DIRECTORY_ROOT = dirname(__DIR__); 
$DIRECTORY_ROOT = str_replace("\\","/",$DIRECTORY_ROOT).'/';
define('DIRECTORY_ROOT',$DIRECTORY_ROOT); 
  
$DOCUMENT_ROOT = __DIR__; 
$DOCUMENT_ROOT = str_replace("\\","/",$DOCUMENT_ROOT).'/'; 
define('DOCUMENT_ROOT',$DOCUMENT_ROOT);

$WEB_ROOT = substr($_SERVER['SCRIPT_FILENAME'],0,-strlen($_SERVER['SCRIPT_NAME'])); 
$WEB_ROOT = str_replace("\\","/",$WEB_ROOT).'/'; 
$WEB_ROOT2 = str_replace("home1","home",$WEB_ROOT); // CRAZYDOMAIN
define('WEB_ROOT',$WEB_ROOT);

$BASEPATH = substr($_SERVER['SCRIPT_NAME'],0,-strlen(basename($_SERVER['SCRIPT_NAME'])));
$BASEPATH = substr($BASEPATH,strlen($BASEPATH)).'/';
$BASEPATH = str_replace('\\',"/",$BASEPATH).str_replace($WEB_ROOT2,"",DOCUMENT_ROOT); 
define('BASEPATH',$BASEPATH); 

define('REQUEST_URI',$_SERVER['REQUEST_URI']);  

$HTTP_TYPE = (!empty($_SERVER['HTTPS'])&&$_SERVER['HTTPS']!=='off')||$_SERVER['SERVER_PORT']===443?'https':'http';
define('HTTP_TYPE',$HTTP_TYPE);

$HTTP_ROOT = $_SERVER['HTTP_HOST'];
define('HTTP_ROOT',$HTTP_ROOT);

$BASE_URL = HTTP_TYPE.'://'.HTTP_ROOT.BASEPATH;  
define('BASE_URL',$BASE_URL);

$FILE_NAME=basename($_SERVER['PHP_SELF'], ".php");
$BASENAME_ARRAY=explode("-",$FILE_NAME);
$BASENAME=$BASENAME_ARRAY[0];

define('BASENAME',$BASENAME.'');
define('FILE_BASENAME',$BASENAME.'.php');

define('ADMIN_LOGIN', BASEPATH.'a/login.php');
define('ADMIN_DASHBOARD', BASEPATH.'a/index.php');

define('TEACHER_LOGIN', BASEPATH.'t/login.php');
define('TEACHER_SIGNUP', BASEPATH.'t/signup.php');
define('TEACHER_DASHBOARD', BASEPATH.'t/index.php');

define('PUPIL_LOGIN', BASEPATH.'p/v2/');
define('PUPIL_DASHBOARD', BASEPATH.'p/index.php');

define('SECRET_KEY_MATHAPP', 'CSBATCH2023');
define('SECRET_AVI_MATHAPP', 'MATHOFGRADE3');
define('AES_METHOD_MATHAPP', 'AES-256-CBC'); 

define('THEME1', BASEPATH.'assets/theme1/');
define('THEME2', BASEPATH.'assets/theme2/');

define('LOGOUT', BASEPATH.'system/logout.php');
define('SUBMIT', BASENAME.'-submit.php');

$page['title'] = 'MathApp3.com'; 
$page['account_name'] = !empty($_SESSION['mathapp']['login']['account_name']) ? $_SESSION['mathapp']['login']['account_name']:'User Account';



$system_config['name'] = "MathApp3";
$system_config['company'] = "MathApp3";
$system_config['domain'] = "mathapp3.com";
$system_config['code'] = "MathApp3";

$system_config['url'] = "https://mathapp3.com/";

$system_config['email_host'] = "cp-wc22.syd02.ds.network";
$system_config['email_port'] = "465";
$system_config['email_SMTPSecure'] = "ssl";

$system_config['email_from'] = "no-reply@mathapp3.com";
$system_config['email_username'] = "no-reply@mathapp3.com";
$system_config['email_password'] = "qwertyuiop22@";

$system_config['sms_notification'] = "enabled"; // enabled disabled


define('EMAIL_HOST', 'cp-wc22.syd02.ds.network');
define('EMAIL_PORT', '465');
define('EMAIL_SMTPSecure', 'ssl');
define('EMAIL_FROM', 'no-reply@mathapp3.com');
define('EMAIL_USERNAME', 'no-reply@mathapp3.com');
define('EMAIL_PASSWORD', 'qwertyuiop22@');
define('SMS_NOTIFICATION', 'enabled');

define('EMAIL_FROM_NAME', 'MathApp3');
 

function pr($array){
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

function authorize($account_type){ 
    if(!isset($_SESSION['mathapp']['login'][$account_type])){
        header('location: '.BASEPATH.'system/logout.php?account='.$account_type);
        exit;
    }
}

function activeMenu($menu_name){ 
    if(FILE_BASENAME == $menu_name){
        return 'active';
    }
}

function resultMessage(){
    if(!empty($_SESSION['mathapp']['error'])){
        $message = $_SESSION['mathapp']['error'];
        unset($_SESSION['mathapp']['error']); 

            $result = '<div class="alert alert-warning" role="alert">
            <small>'.ucwords($message).'</small>
            </div>';  

        echo $result; 
    } 

    if(!empty($_SESSION['mathapp']['success'])){
        $message = $_SESSION['mathapp']['success'];
        unset($_SESSION['mathapp']['success']);
        
            $result = '<div class="alert alert-success" role="alert">
            <small>'.ucwords($message).'</small>
            </div>';
                
        echo $result; 
    } 
}

function encrypt_decrypt($action,$string){  
 
    $output = false;
    $key = hash('sha256', SECRET_KEY_MATHAPP); 
    $iv  = substr(hash('sha256', SECRET_AVI_MATHAPP), 0, 16);
    $res = '';

    if ( $action == 'encrypt' ){
        $output = openssl_encrypt($string, AES_METHOD_MATHAPP, $key, 0, $iv);
        $res = base64_encode($output);
    }
        
    if( $action == 'decrypt' ){
        $res = openssl_decrypt(base64_decode($string), AES_METHOD_MATHAPP, $key, 0, $iv);
    } 

    return $res; 

}

function addLink(){
    return 'add_'.rtrim(BASENAME, "s").'.php';
}

function newguid() { 
    $s = strtoupper(time().md5(uniqid(rand(),true))); 
    $guidText = 
        substr($s,0,8) . '-' . 
        substr($s,8,4) . '-' . 
        substr($s,12,4). '-' . 
        substr($s,16,4). '-' . 
        substr($s,20); 
    return $guidText;
}

function operatorSign($i){
    switch ($i) {
        case "1":
          echo "+";
          break;
        case "2":
          echo "-";
          break;
        case "3":
          echo "*";
          break;
        case "4":
            echo "/";
            break;
        default:
          echo "";
      }
}

function operatorText($i){
    switch ($i) {
        case "1":
          echo "Addition";
          break;
        case "2":
          echo "Subtraction";
          break;
        case "3":
          echo "*";
          break;
        case "4":
            echo "/";
            break;
        default:
          echo "";
      }
}
  
function statusText($status){
    if($status  == 1){
     echo "<span style='color:green'>Active</span>";
    }else{
     echo "<span style='color:red'>Inactive</span>";
    }
   }
   
function avatar($num){  
    if(empty($num)){
        $num = '0';
    }
    echo BASEPATH.'assets/avatar/'.$num.'.png'; 
}

function language(){
    
    $res = 'english';

    if(!empty($_SESSION['mathapp']['prelogin']['language'])){
        $res = $_SESSION['mathapp']['prelogin']['language'];
    } 

    return $res;
}
   


require_once DOCUMENT_ROOT.'system/language.php';




//    https://demo.themesberg.com/pixel-pro/v5/html/pages/blog-post.html
// https://demo.themesberg.com/pixel-pro/v5/html/pages/career-single.html
//    https://demo.themesberg.com/pixel-pro/v5/html/pages/career-single.html 
//    https://demo.themesberg.com/pixel-pro/v5/html/pages/profile.html

//    echo '<pre>';
//    print_r($stmt->errorInfo());
//    echo '</pre>';
//    exit;