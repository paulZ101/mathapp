<?php

// header('location: p/');

// exit;

require_once 'global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/schools.php'; 
require_once DOCUMENT_ROOT.'system/classes/teachers.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 

$database = new Database();
$db = $database->getConnection(); 

$schools = new Schools($db); 
$stmt = $schools->get();  
$schoolsCount = $stmt->rowCount();

$teachers = new Teachers($db); 
$stmt = $teachers->get();  
$teachersCount = $stmt->rowCount();

$pupils = new Pupils($db); 
$stmt = $pupils->get();  
$pupilsCount = $stmt->rowCount();
 

require_once DOCUMENT_ROOT.'system/pages/index.php';