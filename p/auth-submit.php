<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 

$main_page = 'class.php';

if (empty($_SESSION['mathapp']['login']['pid'])) {
    $_SESSION['mathapp']['error'] = 'Please enter your email and password'; 
    header('location: '.FILE_BASENAME);
    exit;
}

if (empty($_POST['password'])) {
    $_SESSION['mathapp']['error'] = 'Please enter your password'; 
    header('location: '.FILE_BASENAME);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_SESSION['mathapp']['login']['pid']);

$database = new Database();
$db = $database->getConnection();

$pupil = new Pupils($db);
$pupil->reg_id = $decryptedID;  
$pupil->getSingle(); 

if($pupil->reg_id == null){
    echo $_SESSION['mathapp']['error'] = 'invalid request';
    header('location: '.$main_page);
    exit;
}  

if($pupil->status != 1){
    echo $_SESSION['mathapp']['error'] = 'invalid request';
    header('location: '.$main_page);
    exit;
} 

if( ($_POST['password'] != $pupil->password) ){
    $_SESSION['mathapp']['error'] = 'wrong password';
    header('location: '.FILE_BASENAME);
    exit;
}
 
$_SESSION['mathapp']['login']['pupil'] = $pupil->reg_id;
$_SESSION['mathapp']['login']['account_name'] = $pupil->first_name.' '.$pupil->last_name;

header('location: '.PUPIL_DASHBOARD);