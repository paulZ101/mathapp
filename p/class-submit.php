<?php

require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/admin.php'; 
require_once DOCUMENT_ROOT.'system/classes/sections.php'; 

if (empty($_POST['code'])) {
    $_SESSION['mathapp']['error'] = 'Please enter your section code'; 
    header('location: '.FILE_BASENAME);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$section = new Sections($db);
$section->section_code = $_POST['code'];  
$section->getByCode(); 

if($section->reg_id == null){
    echo $_SESSION['mathapp']['error'] = 'wrong section code';
    header('location: '.FILE_BASENAME);
    exit;
}  

if($section->status != 1){
    echo $_SESSION['mathapp']['error'] = 'invalid request';
    header('location: '.FILE_BASENAME);
    exit;
} 
 
 
$_SESSION['mathapp']['login']['scode'] = $section->section_code;
header('location: '.BASEPATH.'p/login.php');