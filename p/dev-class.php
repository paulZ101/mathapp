<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Pixel Pro - Coming Soon Subscribe</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="title" content="Pixel Pro - Coming Soon Subscribe">
    <meta name="author" content="Themesberg">
    <meta name="description" content="Premium Bootstrap 5 UI Kit featuring over 1000 components and 35 example pages created by Themesberg.">
    <meta name="keywords" content="bootstrap, bootstrap ui kit, premium Bootstrap 5 UI Kit, bootstrap dashboard, bootstrap messaging, bootstrap billing, bootstrap item list, bootstrap charts, bootstrap timelines, bootstrap cards, bootstrap pricing cards, bootstrap profile cards, bootstrap 5, gulp, npm, sass, javascript, jquery, themesberg, Bootstrap 5 UI Kit theme, Bootstrap 5 UI Kit, premium bootstrap ui kit, bootstrap design system, themesberg ui kit, pixel ui kit, pixel pro ui kit">
    <link rel="canonical" href="https://themesberg.com/product/ui-kit/pixel-pro-premium-bootstrap-5-ui-kit">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://demo.themesberg.com/pixel-pro">
    <meta property="og:title" content="Pixel Pro - Coming Soon Subscribe">
    <meta property="og:description" content="Premium Bootstrap 5 UI Kit featuring over 1000 components and 35 example pages created by Themesberg.">
    <meta property="og:image" content="https://themesberg.s3.us-east-2.amazonaws.com/public/products/pixel-pro/pixel-pro-preview.jpg">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://demo.themesberg.com/pixel-pro">
    <meta property="twitter:title" content="Pixel Pro - Coming Soon Subscribe">
    <meta property="twitter:description" content="Premium Bootstrap 5 UI Kit featuring over 1000 components and 35 example pages created by Themesberg.">
    <meta property="twitter:image" content="https://themesberg.s3.us-east-2.amazonaws.com/public/products/pixel-pro/pixel-pro-preview.jpg">
    <link rel="apple-touch-icon" sizes="120x120" href="https://demo.themesberg.com/pixel-pro/v5/assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://demo.themesberg.com/pixel-pro/v5/assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://demo.themesberg.com/pixel-pro/v5/assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="https://demo.themesberg.com/pixel-pro/v5/assets/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="https://demo.themesberg.com/pixel-pro/v5/assets/img/favicon/safari-pinned-tab.svg" color="#ffffff">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link type="text/css" href="https://demo.themesberg.com/pixel-pro/v5/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://demo.themesberg.com/pixel-pro/v5/vendor/@glidejs/glide/dist/css/glide.core.min.css">
    <link rel="stylesheet" href="https://demo.themesberg.com/pixel-pro/v5/vendor/@glidejs/glide/dist/css/glide.theme.min.css">
    <link type="text/css" href="https://demo.themesberg.com/pixel-pro/v5/css/pixel.css" rel="stylesheet">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-141734189-6"></script>
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      gtag('config', 'UA-141734189-6');
    </script>
    <script>
      (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
          'gtm.start': new Date().getTime(),
          event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
          j = d.createElement(s),
          dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
      })(window, document, 'script', 'dataLayer', 'GTM-THQTXJ7');
    </script>
  </head>
  <body>
  
    <header class="header-global">
      <nav id="navbar-main" aria-label="Primary navigation" class="navbar navbar-main navbar-expand-lg navbar-theme-primary headroom navbar-dark navbar-theme-primary">
        <div class="container position-relative">
          <a class="navbar-brand me-lg-5" href="https://demo.themesberg.com/pixel-pro/v5/index.html">
            <img class="navbar-brand-dark" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/brand/light.svg" alt="Logo light">
            <img class="navbar-brand-light" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/brand/dark.svg" alt="Logo dark">
          </a>
          <div class="navbar-collapse collapse me-auto" id="navbar_global">
            <div class="navbar-collapse-header">
              <div class="row">
                <div class="col-6 collapse-brand">
                  <a href="https://demo.themesberg.com/pixel-pro/v5/index.html">
                    <img src="https://demo.themesberg.com/pixel-pro/v5/assets/img/brand/dark.svg" alt="Themesberg logo">
                  </a>
                </div>
                <div class="col-6 collapse-close">
                  <a href="#navbar_global" class="fas fa-times" data-bs-toggle="collapse" data-bs-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" title="close" aria-label="Toggle navigation"></a>
                </div>
              </div>
            </div>
            <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
              <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" id="frontPagesDropdown" aria-expanded="false" data-bs-toggle="dropdown">Pages <span class="fas fa-angle-down nav-link-arrow ms-1"></span>
                </a>
                <div class="dropdown-menu dropdown-megamenu px-0 py-2 p-lg-4" aria-labelledby="frontPagesDropdown">
                  <div class="row">
                    <div class="col-6 col-lg-4">
                      <h6 class="d-block mb-3 text-primary">Main pages</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/about.html">About</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/pricing.html">Pricing</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/team.html">Team</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/services.html">Services</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/profile.html">Profile</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/contact.html">Contact</a>
                        </li>
                      </ul>
                      <h6 class="d-block text-primary">Legal</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/legal.html">Legal center</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/terms.html">Terms & agreement</a>
                        </li>
                      </ul>
                      <h6 class="d-block text-primary">Career</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/careers.html">Careers</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/career-single.html">Career Single</a>
                        </li>
                      </ul>
                    </div>
                    <div class="col-6 col-lg-4">
                      <h6 class="d-block mb-3 text-primary">Landings</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/landing-app.html">App</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/landing-crypto.html">Crypto</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/landing-freelancer.html">Freelancer</a>
                        </li>
                      </ul>
                      <h6 class="d-block mb-3 text-primary">Support</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/support.html">Support center</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/support-topic.html">Support topic</a>
                        </li>
                      </ul>
                      <h6 class="d-block mb-3 text-primary">Blog</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/blog.html">Blog</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/blog-post.html">Blog post</a>
                        </li>
                      </ul>
                    </div>
                    <div class="col-6 col-lg-4">
                      <h6 class="d-block mb-3 text-primary">User</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/sign-in.html">Sign in</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/sign-up.html">Sign up</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/forgot-password.html">Forgot password</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/reset-password.html">Reset password</a>
                        </li>
                      </ul>
                      <h6 class="d-block mb-3 text-primary">Special</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/404.html">404 Not Found</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/500.html">500 Server Error</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/maintenance.html">Maintenance</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/coming-soon.html">Coming soon</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/blank.html">Blank page</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" id="dashboardDropdown" aria-expanded="false" data-bs-toggle="dropdown">Dashboard <span class="fas fa-angle-down nav-link-arrow ms-1"></span>
                </a>
                <div class="dropdown-menu dropdown-megamenu-sm px-0 py-2 p-lg-4" aria-labelledby="dashboardDropdown">
                  <div class="row">
                    <div class="col-6">
                      <h6 class="d-block mb-3 text-primary">User dashboard</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/account.html">My account</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/settings.html">Settings</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/security.html">Security</a>
                        </li>
                      </ul>
                      <h6 class="d-block mb-3 text-primary">Items</h6>
                      <ul class="list-style-none">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/my-items.html">My items</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/edit-item.html">Edit item</a>
                        </li>
                      </ul>
                    </div>
                    <div class="col-6">
                      <h6 class="d-block mb-3 text-primary">Messaging</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/messages.html">Messages</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/single-message.html">Chat</a>
                        </li>
                      </ul>
                      <h6 class="d-block mb-3 text-primary">Billing</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/billing.html">Billing details</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/invoice.html">Invoice</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" id="componentsDropdown" aria-expanded="false" data-bs-toggle="dropdown">Components <span class="fas fa-angle-down nav-link-arrow ms-1"></span>
                </a>
                <div class="dropdown-menu dropdown-megamenu-md p-0" aria-labelledby="componentsDropdown">
                  <div class="row g-0">
                    <div class="col-lg-6 bg-dark d-none d-lg-block me-0 me-3">
                      <div class="px-0 py-3 text-center">
                        <img src="https://demo.themesberg.com/pixel-pro/v5/assets/img/megamenu_image.png" alt="Pixel Components">
                      </div>
                      <div class="z-2 pb-3 text-center">
                        <a href="https://demo.themesberg.com/pixel-pro/v5/html/components/all.html" class="btn btn-outline-white animate-up-2 mb-2 mb-sm-0 me-3">
                          <span class="me-1">
                            <span class="fas fa-th-large"></span>
                          </span> All components </a>
                        <a href="https://demo.themesberg.com/pixel-pro/v5/html/sections/all-sections.html" class="btn btn-tertiary animate-up-2 mb-2 mb-sm-0">
                          <span class="me-1">
                            <span class="fas fa-pager"></span>
                          </span> All sections </a>
                      </div>
                    </div>
                    <div class="col ps-0 py-3">
                      <ul class="list-style-none">
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/accordions.html">Accordions</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/alerts.html">Alerts</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/badges.html">Badges</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/cards.html">Cards</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/charts.html">Charts</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/bootstrap-carousels.html">Bootstrap Carousels</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/breadcrumbs.html">Breadcrumbs</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/buttons.html">Buttons</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/counters.html">Counters</a>
                        </li>
                      </ul>
                    </div>
                    <div class="col ps-0 py-3">
                      <ul class="list-style-none">
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/dropdowns.html">Dropdowns</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/e-commerce.html">E-commerce</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/forms.html">Forms</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/icon-boxes.html">Icon Boxes</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/modals.html">Modals</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/navs.html">Navs</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/glidejs-carousels.html">GlideJS Carousels</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/pagination.html">Pagination</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/popovers.html">Popovers</a>
                        </li>
                      </ul>
                    </div>
                    <div class="col ps-0 py-3">
                      <ul class="list-style-none">
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/progress-bars.html">Progress Bars</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/steps.html">Steps</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/tables.html">Tables</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/tabs.html">Tabs</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/toasts.html">Toasts</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/timelines.html">Timelines</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/tooltips.html">Tooltips</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/typography.html">Typography</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/widgets.html">Widgets</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" id="supportDropdown" aria-expanded="false">Support <span class="fas fa-angle-down nav-link-arrow ms-1"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg" aria-labelledby="supportDropdown">
                  <div class="col-auto px-0">
                    <div class="list-group list-group-flush">
                      <a href="https://themesberg.com/docs/bootstrap-5/pixel/getting-started/quick-start/" target="_blank" class="list-group-item list-group-item-action d-flex align-items-center p-0 py-3 px-lg-4">
                        <span class="icon icon-sm">
                          <span class="fas fa-file-alt"></span>
                        </span>
                        <div class="ms-4">
                          <span class="d-block font-small fw-bold mb-0">Documentation <span class="badge badge-sm badge-secondary ms-2">v3.1</span>
                          </span>
                          <span class="small">Examples and guides</span>
                        </div>
                      </a>
                      <a href="https://themesberg.com/contact" target="_blank" class="list-group-item list-group-item-action d-flex align-items-center p-0 py-3 px-lg-4">
                        <span class="icon icon-sm">
                          <span class="fas fa-microphone-alt"></span>
                        </span>
                        <div class="ms-4">
                          <span class="d-block font-small fw-bold mb-0">Support</span>
                          <span class="small">Need help? Ask us!</span>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <div class="d-flex align-items-center">
            <a href="https://themesberg.com/docs/bootstrap-5/pixel/getting-started/quick-start/" target="_blank" class="btn btn-outline-gray-100 d-none d-lg-inline me-md-3">
              <span class="fas fa-book me-2"></span> Docs v5.4 </a>
            <a href="https://themesberg.com/product/ui-kit/pixel-pro-premium-bootstrap-5-ui-kit" target="_blank" class="btn btn-tertiary">
              <span class="fas fa-shopping-cart me-2"></span> Buy now </a>
            <button class="navbar-toggler ms-2" type="button" data-bs-toggle="collapse" data-bs-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          </div>
        </div>
      </nav>
    </header>
    <main>
      <div class="preloader bg-dark flex-column justify-content-center align-items-center">
        <svg id="loader-logo" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 64 78.4">
          <path fill="#fff" d="M10,0h1.2V11.2H0V10A10,10,0,0,1,10,0Z" />
          <rect fill="none" stroke="#fff" stroke-width="11.2" x="40" y="17.6" width="0" height="25.6" />
          <rect fill="none" stroke="#fff" stroke-opacity="0.4" stroke-width="11.2" x="23" y="35.2" width="0" height="25.6" />
          <path fill="#fff" d="M52.8,35.2H64V53.8a7,7,0,0,1-7,7H52.8V35.2Z" />
          <rect fill="none" stroke="#fff" stroke-width="11.2" x="6" y="52.8" width="0" height="25.6" />
          <path fill="#fff" d="M52.8,0H57a7,7,0,0,1,7,7h0v4.2H52.8V0Z" />
          <rect fill="none" stroke="#fff" stroke-opacity="0.4" stroke-width="11.2" x="57.8" y="17.6" width="0" height="11.2" />
          <rect fill="none" stroke="#fff" stroke-width="11.2" x="6" y="35.2" width="0" height="11.2" />
          <rect fill="none" stroke="#fff" stroke-width="11.2" x="40.2" y="49.6" width="0" height="11.2" />
          <path fill="#fff" d="M17.6,67.2H28.8v1.2a10,10,0,0,1-10,10H17.6V67.2Z" />
          <rect fill="none" stroke="#fff" stroke-opacity="0.4" stroke-width="28.8" x="31.6" width="0" height="11.2" />
          <rect fill="none" stroke="#fff" x="14" stroke-width="28.8" y="17.6" width="0" height="11.2" />
        </svg>
      </div>
      <section class="section section-header pb-6 bg-primary text-white overflow-hidden z-2">
        <div class="container z-2">
          <div class="row justify-content-center mb-2">
            <div class="col-lg-8 col-xl-8">
              <div class="text-center">
                <h1 class="display-2 mb-3">We're coming soon.</h1>
                <p class="lead text-muted">Our website is <span class="text-italic">under construction</span>. <br>
                  <strong>Subscribe</strong> to be one of the first to use our product.
                </p>
              </div>
              <form class="row d-flex align-items-top justify-content-center mt-5">
                <div class="col-12 col-xl-10">
                  <div class="form-group">
                    <label class="h6" for="email">Email address</label>
                    <div class="d-flex flex-row justify-content-center">
                      <input class="form-control form-control-lg border-0 rounded" id="email" placeholder="name@compeny.com" type="email" required>
                      <button type="submit" class="btn btn-tertiary btn-lg ms-2">Subscribe</button>
                    </div>
                  </div>
                  <p class="small text-left text-muted mt-2">No spam. Pinky swear!</p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
      <div class="section section-md">
        <div class="container">
          <ul class="nav justify-content-center mb-4">
            <li class="nav-item">
              <a class="nav-link text-instagram h3 px-4" aria-label="instagram social link" href="#">
                <span class="fab fa-instagram"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-facebook h3 px-4" aria-label="facebook social link" href="#">
                <span class="fab fa-facebook"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-github h3 px-4" aria-label="github social link" href="#">
                <span class="fab fa-github"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-dribbble h3 px-4" aria-label="dribbble social link" href="#">
                <span class="fab fa-dribbble"></span>
              </a>
            </li>
          </ul>
          <div class="row justify-content-center align-items-center">
            <div class="col-lg-8 text-center">
              <p class="h3 fw-normal lh-160">Pixel Website UI Kit is a unique and beautiful collection of UI elements. So what are you waiting for ?</p>
              <div class="mt-5">
                <a href="https://themesberg.com/product/ui-kit/pixel-pro-premium-bootstrap-5-ui-kit" class="btn btn-secondary animate-up-1">Purchase now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/@popperjs/core/dist/umd/popper.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/headroom.js/dist/headroom.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/onscreen/dist/on-screen.umd.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/nouislider/distribute/nouislider.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/jarallax/dist/jarallax.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/vivus/dist/vivus.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/chartist/dist/chartist.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/@glidejs/glide/dist/glide.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/countup.js/dist/countUp.umd.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/vanillajs-datepicker/dist/js/datepicker.min.js"></script>
    <script async defer="defer" src="https://buttons.github.io/buttons.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/assets/js/pixel.js"></script>
    <script>
      (function() {
        var js = "window['__CF$cv$params']={r:'78a554df8bd720ef',m:'FWGGZz41Q_K6djbjhgUz5rL0Q97_WQnr4UMQD2yqIHY-1673856174-0-AUlMJFCyEMRutSSYGIEDwK1MEHCzwmu1BwKCXk4qbOImCNWMKx6AtrKi2xqejfgskAeZ8F3o+Wd2rM2Q7xqG9xgDOFWXhnWMRGxkitKCwqBSDpdaWwckbzCubFu5lCG2fYjFrS2pUFW8y6SGs0v4hFqbBUdzX4kxuKqFqBF1UqY2',s:[0x986f58b096,0xa7f69a6591],u:'/cdn-cgi/challenge-platform/h/g'};var now=Date.now()/1000,offset=14400,ts=''+(Math.floor(now)-Math.floor(now%offset)),_cpo=document.createElement('script');_cpo.nonce='',_cpo.src='/cdn-cgi/challenge-platform/h/g/scripts/alpha/invisible.js?ts='+ts,document.getElementsByTagName('head')[0].appendChild(_cpo);";
        var _0xh = document.createElement('iframe');
        _0xh.height = 1;
        _0xh.width = 1;
        _0xh.style.position = 'absolute';
        _0xh.style.top = 0;
        _0xh.style.left = 0;
        _0xh.style.border = 'none';
        _0xh.style.visibility = 'hidden';
        document.body.appendChild(_0xh);

        function handler() {
          var _0xi = _0xh.contentDocument || _0xh.contentWindow.document;
          if (_0xi) {
            var _0xj = _0xi.createElement('script');
            _0xj.nonce = '';
            _0xj.innerHTML = js;
            _0xi.getElementsByTagName('head')[0].appendChild(_0xj);
          }
        }
        if (document.readyState !== 'loading') {
          handler();
        } else if (window.addEventListener) {
          document.addEventListener('DOMContentLoaded', handler);
        } else {
          var prev = document.onreadystatechange || function() {};
          document.onreadystatechange = function(e) {
            prev(e);
            if (document.readyState !== 'loading') {
              document.onreadystatechange = prev;
              handler();
            }
          };
        }
      })();
    </script>
    <script defer src="https://static.cloudflareinsights.com/beacon.min.js/vaafb692b2aea4879b33c060e79fe94621666317369993" integrity="sha512-0ahDYl866UMhKuYcW078ScMalXqtFJggm7TmlUtp0UlD4eQk0Ixfnm5ykXKvGJNFjLMoortdseTfsRT8oCfgGA==" data-cf-beacon='{"rayId":"78a554df8bd720ef","version":"2022.11.3","r":1,"token":"3a2c60bab7654724a0f7e5946db4ea5a","si":100}' crossorigin="anonymous"></script>
  </body>
</html>