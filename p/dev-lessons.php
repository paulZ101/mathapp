<!DOCTYPE html>
<html lang="zxx">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Pixel Pro - Blog</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="title" content="Pixel Pro - Blog">
    <meta name="author" content="Themesberg">
    <meta name="description" content="Premium Bootstrap 5 UI Kit featuring over 1000 components and 35 example pages created by Themesberg.">
    <meta name="keywords" content="bootstrap, bootstrap ui kit, premium Bootstrap 5 UI Kit, bootstrap dashboard, bootstrap messaging, bootstrap billing, bootstrap item list, bootstrap charts, bootstrap timelines, bootstrap cards, bootstrap pricing cards, bootstrap profile cards, bootstrap 5, gulp, npm, sass, javascript, jquery, themesberg, Bootstrap 5 UI Kit theme, Bootstrap 5 UI Kit, premium bootstrap ui kit, bootstrap design system, themesberg ui kit, pixel ui kit, pixel pro ui kit">
    <link rel="canonical" href="https://themesberg.com/product/ui-kit/pixel-pro-premium-bootstrap-5-ui-kit">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://demo.themesberg.com/pixel-pro">
    <meta property="og:title" content="Pixel Pro - Blog">
    <meta property="og:description" content="Premium Bootstrap 5 UI Kit featuring over 1000 components and 35 example pages created by Themesberg.">
    <meta property="og:image" content="https://themesberg.s3.us-east-2.amazonaws.com/public/products/pixel-pro/pixel-pro-preview.jpg">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://demo.themesberg.com/pixel-pro">
    <meta property="twitter:title" content="Pixel Pro - Blog">
    <meta property="twitter:description" content="Premium Bootstrap 5 UI Kit featuring over 1000 components and 35 example pages created by Themesberg.">
    <meta property="twitter:image" content="https://themesberg.s3.us-east-2.amazonaws.com/public/products/pixel-pro/pixel-pro-preview.jpg">
    <link rel="apple-touch-icon" sizes="120x120" href="https://demo.themesberg.com/pixel-pro/v5/assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://demo.themesberg.com/pixel-pro/v5/assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://demo.themesberg.com/pixel-pro/v5/assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="https://demo.themesberg.com/pixel-pro/v5/assets/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="https://demo.themesberg.com/pixel-pro/v5/assets/img/favicon/safari-pinned-tab.svg" color="#ffffff">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link type="text/css" href="https://demo.themesberg.com/pixel-pro/v5/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://demo.themesberg.com/pixel-pro/v5/vendor/@glidejs/glide/dist/css/glide.core.min.css">
    <link rel="stylesheet" href="https://demo.themesberg.com/pixel-pro/v5/vendor/@glidejs/glide/dist/css/glide.theme.min.css">
    <link type="text/css" href="https://demo.themesberg.com/pixel-pro/v5/css/pixel.css" rel="stylesheet">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-141734189-6"></script>
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      gtag('config', 'UA-141734189-6');
    </script>
    <script>
      (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
          'gtm.start': new Date().getTime(),
          event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
          j = d.createElement(s),
          dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
      })(window, document, 'script', 'dataLayer', 'GTM-THQTXJ7');
    </script>
  </head>
  <body>
    <noscript>
      <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-THQTXJ7" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <header class="header-global">
      <nav id="navbar-main" aria-label="Primary navigation" class="navbar navbar-main navbar-expand-lg navbar-theme-primary headroom navbar-dark navbar-theme-primary">
        <div class="container position-relative">
          <a class="navbar-brand me-lg-5" href="https://demo.themesberg.com/pixel-pro/v5/index.html">
            <img class="navbar-brand-dark" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/brand/light.svg" alt="Logo light">
            <img class="navbar-brand-light" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/brand/dark.svg" alt="Logo dark">
          </a>
          <div class="navbar-collapse collapse me-auto" id="navbar_global">
            <div class="navbar-collapse-header">
              <div class="row">
                <div class="col-6 collapse-brand">
                  <a href="https://demo.themesberg.com/pixel-pro/v5/index.html">
                    <img src="https://demo.themesberg.com/pixel-pro/v5/assets/img/brand/dark.svg" alt="Themesberg logo">
                  </a>
                </div>
                <div class="col-6 collapse-close">
                  <a href="#navbar_global" class="fas fa-times" data-bs-toggle="collapse" data-bs-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" title="close" aria-label="Toggle navigation"></a>
                </div>
              </div>
            </div>
            <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
              <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" id="frontPagesDropdown" aria-expanded="false" data-bs-toggle="dropdown">Pages <span class="fas fa-angle-down nav-link-arrow ms-1"></span>
                </a>
                <div class="dropdown-menu dropdown-megamenu px-0 py-2 p-lg-4" aria-labelledby="frontPagesDropdown">
                  <div class="row">
                    <div class="col-6 col-lg-4">
                      <h6 class="d-block mb-3 text-primary">Main pages</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/about.html">About</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/pricing.html">Pricing</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/team.html">Team</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/services.html">Services</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/profile.html">Profile</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/contact.html">Contact</a>
                        </li>
                      </ul>
                      <h6 class="d-block text-primary">Legal</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/legal.html">Legal center</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/terms.html">Terms & agreement</a>
                        </li>
                      </ul>
                      <h6 class="d-block text-primary">Career</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/careers.html">Careers</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/career-single.html">Career Single</a>
                        </li>
                      </ul>
                    </div>
                    <div class="col-6 col-lg-4">
                      <h6 class="d-block mb-3 text-primary">Landings</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/landing-app.html">App</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/landing-crypto.html">Crypto</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/landing-freelancer.html">Freelancer</a>
                        </li>
                      </ul>
                      <h6 class="d-block mb-3 text-primary">Support</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/support.html">Support center</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/support-topic.html">Support topic</a>
                        </li>
                      </ul>
                      <h6 class="d-block mb-3 text-primary">Blog</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/blog.html">Blog</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/blog-post.html">Blog post</a>
                        </li>
                      </ul>
                    </div>
                    <div class="col-6 col-lg-4">
                      <h6 class="d-block mb-3 text-primary">User</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/sign-in.html">Sign in</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/sign-up.html">Sign up</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/forgot-password.html">Forgot password</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/reset-password.html">Reset password</a>
                        </li>
                      </ul>
                      <h6 class="d-block mb-3 text-primary">Special</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/404.html">404 Not Found</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/500.html">500 Server Error</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/maintenance.html">Maintenance</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/coming-soon.html">Coming soon</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/pages/blank.html">Blank page</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" id="dashboardDropdown" aria-expanded="false" data-bs-toggle="dropdown">Dashboard <span class="fas fa-angle-down nav-link-arrow ms-1"></span>
                </a>
                <div class="dropdown-menu dropdown-megamenu-sm px-0 py-2 p-lg-4" aria-labelledby="dashboardDropdown">
                  <div class="row">
                    <div class="col-6">
                      <h6 class="d-block mb-3 text-primary">User dashboard</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/account.html">My account</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/settings.html">Settings</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/security.html">Security</a>
                        </li>
                      </ul>
                      <h6 class="d-block mb-3 text-primary">Items</h6>
                      <ul class="list-style-none">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/my-items.html">My items</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/edit-item.html">Edit item</a>
                        </li>
                      </ul>
                    </div>
                    <div class="col-6">
                      <h6 class="d-block mb-3 text-primary">Messaging</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/messages.html">Messages</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/single-message.html">Chat</a>
                        </li>
                      </ul>
                      <h6 class="d-block mb-3 text-primary">Billing</h6>
                      <ul class="list-style-none mb-4">
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/billing.html">Billing details</a>
                        </li>
                        <li class="mb-2 megamenu-item">
                          <a class="megamenu-link" href="https://demo.themesberg.com/pixel-pro/v5/html/dashboard/invoice.html">Invoice</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" id="componentsDropdown" aria-expanded="false" data-bs-toggle="dropdown">Components <span class="fas fa-angle-down nav-link-arrow ms-1"></span>
                </a>
                <div class="dropdown-menu dropdown-megamenu-md p-0" aria-labelledby="componentsDropdown">
                  <div class="row g-0">
                    <div class="col-lg-6 bg-dark d-none d-lg-block me-0 me-3">
                      <div class="px-0 py-3 text-center">
                        <img src="https://demo.themesberg.com/pixel-pro/v5/assets/img/megamenu_image.png" alt="Pixel Components">
                      </div>
                      <div class="z-2 pb-3 text-center">
                        <a href="https://demo.themesberg.com/pixel-pro/v5/html/components/all.html" class="btn btn-outline-white animate-up-2 mb-2 mb-sm-0 me-3">
                          <span class="me-1">
                            <span class="fas fa-th-large"></span>
                          </span> All components </a>
                        <a href="https://demo.themesberg.com/pixel-pro/v5/html/sections/all-sections.html" class="btn btn-tertiary animate-up-2 mb-2 mb-sm-0">
                          <span class="me-1">
                            <span class="fas fa-pager"></span>
                          </span> All sections </a>
                      </div>
                    </div>
                    <div class="col ps-0 py-3">
                      <ul class="list-style-none">
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/accordions.html">Accordions</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/alerts.html">Alerts</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/badges.html">Badges</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/cards.html">Cards</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/charts.html">Charts</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/bootstrap-carousels.html">Bootstrap Carousels</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/breadcrumbs.html">Breadcrumbs</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/buttons.html">Buttons</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/counters.html">Counters</a>
                        </li>
                      </ul>
                    </div>
                    <div class="col ps-0 py-3">
                      <ul class="list-style-none">
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/dropdowns.html">Dropdowns</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/e-commerce.html">E-commerce</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/forms.html">Forms</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/icon-boxes.html">Icon Boxes</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/modals.html">Modals</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/navs.html">Navs</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/glidejs-carousels.html">GlideJS Carousels</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/pagination.html">Pagination</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/popovers.html">Popovers</a>
                        </li>
                      </ul>
                    </div>
                    <div class="col ps-0 py-3">
                      <ul class="list-style-none">
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/progress-bars.html">Progress Bars</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/steps.html">Steps</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/tables.html">Tables</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/tabs.html">Tabs</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/toasts.html">Toasts</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/timelines.html">Timelines</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/tooltips.html">Tooltips</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/typography.html">Typography</a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="https://demo.themesberg.com/pixel-pro/v5/html/components/widgets.html">Widgets</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" id="supportDropdown" aria-expanded="false">Support <span class="fas fa-angle-down nav-link-arrow ms-1"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg" aria-labelledby="supportDropdown">
                  <div class="col-auto px-0">
                    <div class="list-group list-group-flush">
                      <a href="https://themesberg.com/docs/bootstrap-5/pixel/getting-started/quick-start/" target="_blank" class="list-group-item list-group-item-action d-flex align-items-center p-0 py-3 px-lg-4">
                        <span class="icon icon-sm">
                          <span class="fas fa-file-alt"></span>
                        </span>
                        <div class="ms-4">
                          <span class="d-block font-small fw-bold mb-0">Documentation <span class="badge badge-sm badge-secondary ms-2">v3.1</span>
                          </span>
                          <span class="small">Examples and guides</span>
                        </div>
                      </a>
                      <a href="https://themesberg.com/contact" target="_blank" class="list-group-item list-group-item-action d-flex align-items-center p-0 py-3 px-lg-4">
                        <span class="icon icon-sm">
                          <span class="fas fa-microphone-alt"></span>
                        </span>
                        <div class="ms-4">
                          <span class="d-block font-small fw-bold mb-0">Support</span>
                          <span class="small">Need help? Ask us!</span>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <div class="d-flex align-items-center">
            <a href="https://themesberg.com/docs/bootstrap-5/pixel/getting-started/quick-start/" target="_blank" class="btn btn-outline-gray-100 d-none d-lg-inline me-md-3">
              <span class="fas fa-book me-2"></span> Docs v5.4 </a>
            <a href="https://themesberg.com/product/ui-kit/pixel-pro-premium-bootstrap-5-ui-kit" target="_blank" class="btn btn-tertiary">
              <span class="fas fa-shopping-cart me-2"></span> Buy now </a>
            <button class="navbar-toggler ms-2" type="button" data-bs-toggle="collapse" data-bs-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          </div>
        </div>
      </nav>
    </header>
    <main>
      <div class="preloader bg-dark flex-column justify-content-center align-items-center">
        <svg id="loader-logo" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 64 78.4">
          <path fill="#fff" d="M10,0h1.2V11.2H0V10A10,10,0,0,1,10,0Z" />
          <rect fill="none" stroke="#fff" stroke-width="11.2" x="40" y="17.6" width="0" height="25.6" />
          <rect fill="none" stroke="#fff" stroke-opacity="0.4" stroke-width="11.2" x="23" y="35.2" width="0" height="25.6" />
          <path fill="#fff" d="M52.8,35.2H64V53.8a7,7,0,0,1-7,7H52.8V35.2Z" />
          <rect fill="none" stroke="#fff" stroke-width="11.2" x="6" y="52.8" width="0" height="25.6" />
          <path fill="#fff" d="M52.8,0H57a7,7,0,0,1,7,7h0v4.2H52.8V0Z" />
          <rect fill="none" stroke="#fff" stroke-opacity="0.4" stroke-width="11.2" x="57.8" y="17.6" width="0" height="11.2" />
          <rect fill="none" stroke="#fff" stroke-width="11.2" x="6" y="35.2" width="0" height="11.2" />
          <rect fill="none" stroke="#fff" stroke-width="11.2" x="40.2" y="49.6" width="0" height="11.2" />
          <path fill="#fff" d="M17.6,67.2H28.8v1.2a10,10,0,0,1-10,10H17.6V67.2Z" />
          <rect fill="none" stroke="#fff" stroke-opacity="0.4" stroke-width="28.8" x="31.6" width="0" height="11.2" />
          <rect fill="none" stroke="#fff" x="14" stroke-width="28.8" y="17.6" width="0" height="11.2" />
        </svg>
      </div>
      <section class="section-header bg-primary text-white pb-10 pb-sm-8 pb-lg-11">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-12 col-md-8 text-center">
              <h1 class="display-2 mb-4">Our Blog</h1>
              <p class="lead">We help you get better at SEO and marketing: detailed tutorials, case studies and opinion pieces from marketing practitioners and industry experts alike</p>
            </div>
          </div>
        </div>
      </section>
      <section class="section section-lg line-bottom-light">
        <div class="container mt-n10 mt-lg-n12 z-2">
          <div class="row">
            <div class="col-lg-12 mb-5">
              <div class="card shadow bg-white border-gray-300 flex-lg-row align-items-center g-0 p-4">
                <a href="./blog-post.html" class="col-12 col-lg-6">
                  <img src="https://demo.themesberg.com/pixel-pro/v5/assets/img/blog/image-1.jpg" alt="themesberg office" class="card-img-top rounded">
                </a>
                <div class="card-body d-flex flex-column justify-content-between col-auto py-4 p-0 p-lg-3 p-xl-5">
                  <a href="./blog-post.html">
                    <h2>Designing a dashboard that increases business value</h2>
                  </a>
                  <p>Today we are overwhelmed by content, and inspiration can strike anywhere. The point is to collect it and share it in a structured way to inspire your team, client, and users. In this article, I will show you how to design a dashboard.</p>
                  <div class="d-flex align-items-center mt-3">
                    <img class="avatar avatar-sm rounded-circle" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/team/profile-picture-1.jpg" alt="Richard avatar">
                    <h3 class="h6 small ms-2 mb-0">Richard Thomas</h3>
                    <span class="h6 text-muted small fw-normal mb-0 ms-auto">
                      <time datetime="2019-04-25">21 February, 2019</time>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-5">
              <div class="card shadow bg-white border-gray-300 p-4 rounded">
                <a href="./blog-post.html">
                  <img src="https://demo.themesberg.com/pixel-pro/v5/assets/img/blog/image-2.jpg" class="card-img-top rounded" alt="our desk">
                </a>
                <div class="card-body p-0 pt-4">
                  <a href="./blog-post.html" class="h4">Google launches Cloud AI Platform Pipelines</a>
                  <div class="d-flex align-items-center my-3">
                    <img class="avatar avatar-sm rounded-circle" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/team/profile-picture-2.jpg" alt="Neil avatar">
                    <h3 class="h6 small ms-2 mb-0">Neil Sims Curran</h3>
                  </div>
                  <p class="mb-0">Richard Thomas was born in 1990, and at only 29 years old, his trajectory is good. When he is asked how he describes himself, he responds, "designer but in a broad sense". His projects?</p>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-5">
              <div class="card shadow bg-white border-gray-300 p-4 rounded">
                <a href="./blog-post.html">
                  <img src="https://demo.themesberg.com/pixel-pro/v5/assets/img/blog/image-3.jpg" class="card-img-top rounded" alt="web desk">
                </a>
                <div class="card-body p-0 pt-4">
                  <a href="./blog-post.html" class="h4">Apple Details Reveal Remarkable MacBook</a>
                  <div class="d-flex align-items-center my-3">
                    <img class="avatar avatar-sm rounded-circle" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/team/profile-picture-3.jpg" alt="David avatar">
                    <h3 class="h6 small ms-2 mb-0">David L. Brown</h3>
                  </div>
                  <p class="mb-0">Following the release of the 16-inch MacBook Pro in late 2019, Apple was praised for the larger screen, the increased performance, premium materials and the improved keyboard.</p>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-5">
              <div class="card shadow bg-white border-gray-300 p-4 rounded">
                <a href="./blog-post.html">
                  <img src="https://demo.themesberg.com/pixel-pro/v5/assets/img/blog/image-1.jpg" class="card-img-top rounded" alt="pixel room">
                </a>
                <div class="card-body p-0 pt-4">
                  <a href="./blog-post.html" class="h4">One of Google Maps’ best new features</a>
                  <div class="d-flex align-items-center my-3">
                    <img class="avatar avatar-sm rounded-circle" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/team/profile-picture-1.jpg" alt="Jose avatar">
                    <h3 class="h6 small ms-2 mb-0">Jose Evans</h3>
                  </div>
                  <p class="mb-0">As great as Google Maps might be, not all users get to take advantage of the app’s brand new features at the same time, as these updates are rolled out gradually to more and more people.</p>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-5">
              <div class="card shadow bg-white border-gray-300 p-4 rounded">
                <a href="./blog-post.html">
                  <img src="https://demo.themesberg.com/pixel-pro/v5/assets/img/blog/image-3.jpg" class="card-img-top rounded" alt="designer office">
                </a>
                <div class="card-body p-0 pt-4">
                  <a href="./blog-post.html" class="h4">Google launches Cloud AI Platform Pipelines</a>
                  <div class="d-flex align-items-center my-3">
                    <img class="avatar avatar-sm rounded-circle" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/team/profile-picture-3.jpg" alt="James avatar">
                    <h3 class="h6 small ms-2 mb-0">James Curran</h3>
                  </div>
                  <p class="mb-0">Richard Thomas was born in 1990, and at only 29 years old, his trajectory is good. When he is asked how he describes himself, he responds, "designer but in a broad sense". His projects?</p>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-5">
              <div class="card shadow bg-white border-gray-300 p-4 rounded">
                <a href="./blog-post.html">
                  <img src="https://demo.themesberg.com/pixel-pro/v5/assets/img/blog/image-2.jpg" class="card-img-top rounded" alt="white laptop">
                </a>
                <div class="card-body p-0 pt-4">
                  <a href="./blog-post.html" class="h4">Apple Details Reveal Remarkable MacBook</a>
                  <div class="d-flex align-items-center my-3">
                    <img class="avatar avatar-sm rounded-circle" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/team/profile-picture-5.jpg" alt="Bonnie avatar">
                    <h3 class="h6 small ms-2 mb-0">Bonnie Green</h3>
                  </div>
                  <p class="mb-0">Following the release of the 16-inch MacBook Pro in late 2019, Apple was praised for the larger screen, the increased performance, premium materials and the improved keyboard.</p>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-5">
              <div class="card shadow bg-white border-gray-300 p-4 rounded">
                <a href="./blog-post.html">
                  <img src="https://demo.themesberg.com/pixel-pro/v5/assets/img/blog/image-1.jpg" class="card-img-top rounded" alt="photoshop books">
                </a>
                <div class="card-body p-0 pt-4">
                  <a href="./blog-post.html" class="h4">One of Google Maps’ best new features</a>
                  <div class="d-flex align-items-center my-3">
                    <img class="avatar avatar-sm rounded-circle" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/team/profile-picture-4.jpg" alt="Joseph avatar">
                    <h3 class="h6 small ms-2 mb-0">Joseph Garth</h3>
                  </div>
                  <p class="mb-0">As great as Google Maps might be, not all users get to take advantage of the app’s brand new features at the same time, as these updates are rolled out gradually to more and more people.</p>
                </div>
              </div>
            </div>
            <div class="col-12">
              <div class="d-flex justify-content-center">
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <li class="page-item">
                      <a class="page-link" href="#">Previous</a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" href="#">1</a>
                    </li>
                    <li class="page-item active">
                      <a class="page-link" href="#">2</a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" href="#">3</a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" href="#">4</a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" href="#">5</a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" href="#">Next</a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <footer class="footer pt-6 pb-5 bg-dark text-white">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <img class="navbar-brand-dark mb-4" height="35" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/brand/light.svg" alt="Logo light">
            <p>Pixel Pro is a premium Bootstrap 5 UI Kit that will help you prototype and design beautiful, creative and modern websites.</p>
            <ul class="social-buttons mb-5 mb-lg-0">
              <li>
                <a href="https://twitter.com/themesberg" aria-label="twitter social link" class="icon-white me-2">
                  <span class="fab fa-twitter"></span>
                </a>
              </li>
              <li>
                <a href="https://www.facebook.com/themesberg/" class="icon-white me-2" aria-label="facebook social link">
                  <span class="fab fa-facebook"></span>
                </a>
              </li>
              <li>
                <a href="https://github.com/themesberg" aria-label="github social link" class="icon-white me-2">
                  <span class="fab fa-github"></span>
                </a>
              </li>
              <li>
                <a href="https://dribbble.com/themesberg" class="icon-white" aria-label="dribbble social link">
                  <span class="fab fa-dribbble"></span>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-6 col-md-2 mb-5 mb-lg-0">
            <span class="h5">Themesberg</span>
            <ul class="footer-links mt-2">
              <li>
                <a target="_blank" href="https://themesberg.com/blog">Blog</a>
              </li>
              <li>
                <a target="_blank" href="https://themesberg.com/themes">Products</a>
              </li>
              <li>
                <a target="_blank" href="https://themesberg.com/about">About Us</a>
              </li>
              <li>
                <a target="_blank" href="https://themesberg.com/contact">Contact Us</a>
              </li>
            </ul>
          </div>
          <div class="col-6 col-md-2 mb-5 mb-lg-0">
            <span class="h5">Other</span>
            <ul class="footer-links mt-2">
              <li>
                <a href="https://themesberg.com/docs/bootstrap-5/pixel/getting-started/quick-start/" target="_blank">Docs <span class="badge badge-sm bg-success ms-2">v5.4</span>
                </a>
              </li>
              <li>
                <a href="https://themesberg.com/docs/pixel-bootstrap/getting-started/changelog/" target="_blank">Changelog</a>
              </li>
              <li>
                <a target="_blank" href="https://themesberg.com/licensing">License</a>
              </li>
              <li>
                <a target="_blank" href="https://github.com/themesberg/pixel-bootstrap-ui-kit/issues">Support</a>
              </li>
            </ul>
          </div>
          <div class="col-12 col-md-4 mb-5 mb-lg-0">
            <span class="h5">Subscribe</span>
            <p class="text-muted font-small mt-2">Join our mailing list. We write rarely, but only the best content.</p>
            <form action="#">
              <div class="form-row mb-2">
                <div class="col-12">
                  <label class="h6 fw-normal text-muted" for="exampleInputEmail3">Email address</label>
                  <input type="email" class="form-control mb-2" placeholder="example@company.com" name="email" aria-label="Subscribe form" id="exampleInputEmail3" required>
                </div>
                <div class="col-12 d-grid">
                  <button type="submit" class="btn btn-tertiary" data-loading-text="Sending">
                    <span>Subscribe</span>
                  </button>
                </div>
              </div>
            </form>
            <p class="text-muted font-small m-0">We’ll never share your details. See our <a class="text-white" href="#">Privacy Policy</a>
            </p>
          </div>
        </div>
        <hr class="bg-secondary my-3 my-lg-5">
        <div class="row">
          <div class="col mb-md-0">
            <a href="https://themesberg.com" target="_blank" class="d-flex justify-content-center mb-3">
              <img src="https://demo.themesberg.com/pixel-pro/v5/assets/img/themesberg.svg" height="30" class="me-2" alt="Themesberg Logo">
              <p class="text-white fw-bold footer-logo-text m-0">Themesberg</p>
            </a>
            <div class="d-flex text-center justify-content-center align-items-center" role="contentinfo">
              <p class="fw-normal font-small mb-0">Copyright © Themesberg 2019- <span class="current-year">2021</span>. All rights reserved. </p>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/@popperjs/core/dist/umd/popper.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/headroom.js/dist/headroom.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/onscreen/dist/on-screen.umd.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/nouislider/distribute/nouislider.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/jarallax/dist/jarallax.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/vivus/dist/vivus.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/chartist/dist/chartist.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/@glidejs/glide/dist/glide.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/countup.js/dist/countUp.umd.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/vanillajs-datepicker/dist/js/datepicker.min.js"></script>
    <script async defer="defer" src="https://buttons.github.io/buttons.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/assets/js/pixel.js"></script>
    <script>
      // here you should load content from an Ajax request and when it
      // loads you can disable the button from loading
      $('#loadOnClick').click(function() {
        $button = $(this);
        $loadContent = $('#extraContent');
        $allLoaded = $('#allLoadedText');
        $button.addClass('btn-loading');
        $button.attr('disabled', true);
        setTimeout(function() {
          $loadContent.show();
          $button.hide();
          $allLoaded.show();
        }, 1500);
      });
    </script>
    <script defer src="https://static.cloudflareinsights.com/beacon.min.js/vaafb692b2aea4879b33c060e79fe94621666317369993" integrity="sha512-0ahDYl866UMhKuYcW078ScMalXqtFJggm7TmlUtp0UlD4eQk0Ixfnm5ykXKvGJNFjLMoortdseTfsRT8oCfgGA==" data-cf-beacon='{"rayId":"78a22b6588170b59","version":"2022.11.3","r":1,"token":"3a2c60bab7654724a0f7e5946db4ea5a","si":100}' crossorigin="anonymous"></script>
  </body>
</html>