<?php
require_once '../global.php';

require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 
require_once DOCUMENT_ROOT.'system/classes/sections.php'; 
require_once DOCUMENT_ROOT.'system/classes/numeracyResults.php'; 
 
authorize('pupil'); 
 
$main_page ='index.php'; 

$database = new Database();
$db = $database->getConnection();

$pupil = new Pupils($db);
$pupil->reg_id = $_SESSION['mathapp']['login']['pupil'];  

$pupil->getClass();

if($pupil->reg_id == null){
    header('location: '.$main_page);
    exit;
}

$numeracyResultX = new numeracyResults($db);

$numeracyResultX->pupil_id = $pupil->reg_id; 
$numeracyResultX->numeracy = 1;  
$numeracyResultX->check();

$numeracyResult1 = false;
if($numeracyResultX->reg_id == null AND $pupil->n1 == true){
    $numeracyResult1 = true;
} 

$numeracyResultY = new numeracyResults($db);

$numeracyResultY->pupil_id = $pupil->reg_id; 
$numeracyResultY->numeracy = 2;  
$numeracyResultY->check();

$numeracyResult2 = false;
if($numeracyResultY->reg_id == null AND $pupil->n2 == true){
    $numeracyResult2 = true;
}

 
require_once DOCUMENT_ROOT . 'system/pages/pupil/header.php'; 
require_once DOCUMENT_ROOT . 'system/pages/pupil/index2.php'; 
require_once DOCUMENT_ROOT . 'system/pages/pupil/footer.php'; 