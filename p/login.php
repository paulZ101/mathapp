<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 
require_once DOCUMENT_ROOT.'system/classes/sections.php'; 

$main_page = 'class.php';

if (empty($_SESSION['mathapp']['login']['scode'])) {
    $_SESSION['mathapp']['error'] = 'Please enter your section code'; 
    header('location: '.$main_page);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$section = new Sections($db);
$section->section_code = $_SESSION['mathapp']['login']['scode'];  
$section->getByCode(); 

if($section->reg_id == null){
    echo $_SESSION['mathapp']['error'] = 'wrong section code';
    header('location: '.$main_page);
    exit;
}  

if($section->status != 1){
    echo $_SESSION['mathapp']['error'] = 'invalid request';
    header('location: '.$main_page);
    exit;
}

$pupil = new Pupils($db);

$pupil->section_code = $section->section_code;

$stmt = $pupil->getBySectionCode();  
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "first_name" => $first_name,  
            "last_name" => $last_name,  
            "student_id" => $student_id,  
            "username" => $username,  
            "status" => $status,
            "section_name" => $section_name  
        );
        
        array_push($dataArray["body"], $e);
    } 
}
 
require_once DOCUMENT_ROOT . 'system/pages/pupil/login.php';