<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 
require_once DOCUMENT_ROOT.'system/classes/category.php'; 
require_once DOCUMENT_ROOT.'system/classes/modules.php'; 
require_once DOCUMENT_ROOT.'system/classes/lessons.php'; 
require_once DOCUMENT_ROOT.'system/classes/questions.php'; 

authorize('pupil');

$label = 'quiz';
$main_page = 'index.php';

if(empty($_GET['lid'])){
    header('location:'.$main_page);
    exit;
}



$decryptedID = encrypt_decrypt('decrypt', $_GET['lid']);

if(isset($_SESSION['mathapp']['login']['quiz']['lesson'])){
    unset($_SESSION['mathapp']['login']['quiz']['lesson']);
}

$_SESSION['mathapp']['login']['quiz']['lesson'] = $decryptedID;

$database = new Database();
$db = $database->getConnection();




$lesson = new Lessons($db);
$lesson->reg_id = $decryptedID;  

$lesson->getSingle();

if($lesson->reg_id == null){
    header('location: '.$main_page);
    exit;
}



$pupil = new Pupils($db);
$pupil->reg_id = $_SESSION['mathapp']['login']['pupil'];  

$pupil->getClass();

if($pupil->reg_id == null){
    header('location: '.$main_page);
    exit;
}


 



$question = new Questions($db);
$question->lesson_id = $lesson->reg_id;  
$question->teacher_reg_id = $pupil->teacher_reg_id; 

$stmt = $question->getByLessonIDLIMIT5();
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "question" => $question,
            "option1" => $option1,
            "option2" => $option2,
            "option3" => $option3,
            "option4" => $option4,
            "status" => $status ,
            "imagefile" => $imagefile
        );
        
        array_push($dataArray["body"], $e);
    } 
}  else {
    header('location: '.BASEPATH.'p/');
    exit; 
    
}

 




$results = array();
$number_question = 1;

$rowcount = $dataCount;

$remainder = $rowcount/$number_question;
$results['number_question'] = $number_question;
$results['remainder'] = $remainder;
$results['rowcount'] = $rowcount;

foreach ( $dataArray["body"] as $result ) {
    $results['questions'][] = $result;
}


// pr($results);

// exit;

 
require_once DOCUMENT_ROOT.'system/pages/pupil/quiz2.php';
 