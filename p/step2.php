<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 
require_once DOCUMENT_ROOT.'system/classes/category.php'; 
require_once DOCUMENT_ROOT.'system/classes/modules.php'; 

authorize('pupil');

$label = 'step2';
$main_page = 'index.php';

if(empty($_GET['c'])){
    header('location:'.$main_page);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_GET['c']); 

$database = new Database();
$db = $database->getConnection();

$category = new Category($db);
$category->id = $decryptedID;  

$category->getSingle();

if($category->id == null){
    header('location: '.$main_page);
    exit;
}

$pupil = new Pupils($db);
$pupil->reg_id = $_SESSION['mathapp']['login']['pupil'];  

$pupil->getClass();

if($pupil->reg_id == null){
    header('location: '.$main_page);
    exit;
}
 
$pupilArray = (array) $pupil; 
if($pupilArray['q'.$category->id] != 1){
    header('location: '.$main_page);
    exit;
}

 
$module = new Modules($db);
$module->category_id = $category->id;  
$module->teacher_reg_id = $pupil->teacher_reg_id; 

$stmt = $module->getByCategoryTeacher();
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "module_title" => $module_title, 
            "category_name" => $category_name, 
            "status" => $status 
        );
        
        array_push($dataArray["body"], $e);
    } 
} 

require_once DOCUMENT_ROOT . 'system/pages/pupil/new/header.php'; 
require_once DOCUMENT_ROOT . 'system/pages/pupil/new/step2.php'; 
require_once DOCUMENT_ROOT . 'system/pages/pupil/new/footer.php'; 