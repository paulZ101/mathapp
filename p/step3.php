<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 
require_once DOCUMENT_ROOT.'system/classes/category.php'; 
require_once DOCUMENT_ROOT.'system/classes/modules.php'; 
require_once DOCUMENT_ROOT.'system/classes/lessons.php'; 

authorize('pupil');

$label = 'step3';
$main_page = 'index.php';

if(empty($_GET['mid'])){
    header('location:'.$main_page);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_GET['mid']); 

$database = new Database();
$db = $database->getConnection();


$module = new Modules($db);
$module->reg_id = $decryptedID;  

$module->getSingle();

if($module->reg_id == null){
    header('location: '.$main_page);
    exit;
}


$pupil = new Pupils($db);
$pupil->reg_id = $_SESSION['mathapp']['login']['pupil'];  

$pupil->getClass();

if($pupil->reg_id == null){
    header('location: '.$main_page);
    exit;
}

$lesson = new Lessons($db);
$lesson->module_id = $module->reg_id;  
$lesson->teacher_reg_id = $pupil->teacher_reg_id; 

$stmt = $lesson->getByModuleID();
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "lesson_title" => $lesson_title,  
            "status" => $status 
        );
        
        array_push($dataArray["body"], $e);
    } 
} 



 
require_once DOCUMENT_ROOT . 'system/pages/pupil/new/header.php'; 
require_once DOCUMENT_ROOT . 'system/pages/pupil/new/step3.php'; 
require_once DOCUMENT_ROOT . 'system/pages/pupil/new/footer.php'; 