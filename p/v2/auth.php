<?php
require_once '../../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 

$main_page = 'class.php';

if (!empty($_GET['pid'])) {
    $_SESSION['mathapp']['login']['pid'] = $_GET['pid'];
    header('Location: auth.php');
    die;  
}

if (empty($_SESSION['mathapp']['login']['pid'])) {
    $_SESSION['mathapp']['error'] = 'invalid request'; 
    header('location: '.$main_page);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_SESSION['mathapp']['login']['pid']);

$database = new Database();
$db = $database->getConnection();

$pupil = new Pupils($db);

$pupil->reg_id = $decryptedID;

$pupil->getClass();

 

 

if($pupil->reg_id == null){
    header('location: '.$main_page);
    exit;
}

// pr($pupil);

require_once DOCUMENT_ROOT . 'system/pages/pupil/auth.php';