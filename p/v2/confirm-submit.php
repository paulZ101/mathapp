<?php
require_once '../../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 

$main_page = 'auth.php';

if(empty($_SESSION['mathapp']['prelogin']['pupil'])){
    header('location: '.$main_page);
    exit;
}

if(empty($_SESSION['mathapp']['prelogin']['account_name'])){
    header('location: '.$main_page);
    exit;
}


$_SESSION['mathapp']['login']['avatar'] = $_SESSION['mathapp']['prelogin']['avatar'];

$_SESSION['mathapp']['login']['language'] = $_SESSION['mathapp']['prelogin']['language'];
$_SESSION['mathapp']['login']['pupil'] = $_SESSION['mathapp']['prelogin']['pupil'];
$_SESSION['mathapp']['login']['account_name'] = $_SESSION['mathapp']['prelogin']['account_name'];

header('location: '.PUPIL_DASHBOARD);