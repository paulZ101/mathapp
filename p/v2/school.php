<?php
require_once '../../global.php';

require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/schools.php';  


$database = new Database();
$db = $database->getConnection(); 


$school = new Schools($db); 
$stmt = $school->get();  
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "school_name" => $school_name, 
            "school_id" => $school_id,
            "school_address" => $school_address, 
            "date_added" => $date_added,
            "status" => $status,
            "language" => $language
        );
        
        array_push($dataArray["body"], $e);
    } 
}

// pr($dataArray);

// foreach($dataArray["body"] as $data){
//     echo '<a href="section.php?sid='.encrypt_decrypt('encrypt',$data['reg_id']).'">'.$data['school_name'].'</a><br>';
// }

// require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/header.php';
// require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/school.php';
// require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/footer.php';


    
require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/header.php';
require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/school.php';
require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/footer.php';