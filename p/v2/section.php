<?php
require_once '../../global.php';

require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/schools.php';  
require_once DOCUMENT_ROOT.'system/classes/sections.php';  

$label = 'section';
$main_page = 'school.php';


if(empty($_GET['sid'])){
    // header('location:'.$main_page);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_GET['sid']);

$database = new Database();
$db = $database->getConnection();

$$label = new Schools($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

$data = array(
    "reg_id" =>  $$label->reg_id,
    "school_id" => $$label->school_id,
    "school_name" => $$label->school_name,
    "school_address" => $$label->school_address,
    "status" => $$label->status,
    "language" => $$label->language 
);

$_SESSION['mathapp']['prelogin']['school_name'] = $data['school_name'];
$_SESSION['mathapp']['prelogin']['language'] = $data['language'];


 

// echo $languageX;
// exit;

$sections = new Sections($db);
$sections->school_reg_id = $data['reg_id'];  

$stmt = $sections->getBySchool(); 
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "section_name" => $section_name 
        );
        
        array_push($dataArray["body"], $e);
    } 
}


// pr($dataArray);

// foreach($dataArray["body"] as $data){
//     echo '<a href="teacher.php?cid='.encrypt_decrypt('encrypt',$data['reg_id']).'">'.$data['section_name'].'</a><br>';
// }



require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/header.php';
require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/section.php';
require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/footer.php';

 