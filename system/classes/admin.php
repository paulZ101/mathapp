<?php
class Admin
{
    private $conn;
    private $db_table = "tbl_admin";
    private $columns = ['id','email','password','status'];
     
    public $id;
    public $email;
    public $password;
    public $status;
    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function getByEmail(){

        $this->email = htmlspecialchars(strip_tags($this->email)); 

        $columns = implode(', ', $this->columns);

        $sqlQuery = "SELECT ".$columns." FROM ". $this->db_table ."
                WHERE 
                    email = ?  
                LIMIT 0,1"; 

        $stmt = $this->conn->prepare($sqlQuery);  
        $stmt->bindParam(1, $this->email); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC); 
        
        $this->id = $dataRow['id']; 
        $this->email = $dataRow['email'];
        $this->password = $dataRow['password'];  
        $this->status = $dataRow['status'];  

    }    
}