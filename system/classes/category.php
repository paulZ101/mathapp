<?php
class Category
{
    // Connection
    private $conn;
    // Table
    private $db_table = "tbl_categories";
    private $columns = 
        [   
            'id',
            'category_name' 
        ];
    // Columns
    public $id;
    public $category_name; 
    // Db connection
    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function get(){

        $columns = implode(', ', $this->columns);

        $sqlQuery = "SELECT ".$columns." FROM " . $this->db_table . " ORDER BY id";
        $stmt = $this->conn->prepare($sqlQuery); 
        
        $stmt->execute();
        return $stmt;

    }  

    public function getSingle(){

        $columns = implode(', ', $this->columns);

        $sqlQuery = "SELECT
                     ".$columns." 
                FROM
                    ". $this->db_table ."
                WHERE 
                    id = ?  
                LIMIT 0,1";

        $this->id=htmlspecialchars(strip_tags($this->id));

        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->bindParam(1, $this->id); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $this->id = $dataRow['id'];
        $this->category_name = $dataRow['category_name'];
        

    } 
    
}