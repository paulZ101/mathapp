<?php 
    class Database {
        private $host = DB_HOST;
        private $database_name = DB_NAME;
        private $username = DB_USERNAME;
        private $password = DB_PASSWORD;
        public $conn;
        public function getConnection(){
            $this->conn = null;
            try{
                $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->database_name, $this->username, $this->password);
                $this->conn->exec("set names utf8");
            }catch(PDOException $exception){
                echo "Database could not be connected: " . $exception->getMessage();
            }
            return $this->conn;
        }
    } 