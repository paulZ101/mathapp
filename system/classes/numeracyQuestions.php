<?php
class numeracyQuestions
{
    // Connection
    private $conn;
    // Table
    private $db_table = "tbl_numeracy_questions";
    // Columns
    public $reg_id;
    public $number1; 
    public $number2; 
    public $answer;
    public $operator;
    public $numeracy;
    public $status;
    public $date_added;
    public $teacher_reg_id;
 
    // Db connection
    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function get()
    {
        $sqlQuery = "
            SELECT 
                reg_id, 
                number1, 
                number2, 
                answer, 
                operator,  
                numeracy,  
                status, 
                teacher_reg_id, 
                date_added
            FROM " . $this->db_table . " 
            WHERE teacher_reg_id = :teacher_reg_id ";  
 
        
        $stmt = $this->conn->prepare($sqlQuery);

        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 

        $stmt->execute();
        return $stmt;
        
    }

   


    public function getByOperator(){

        $sqlQuery = "
            SELECT 
                reg_id, 
                number1, 
                number2, 
                answer, 
                operator,  
                numeracy,  
                status, 
                teacher_reg_id, 
                date_added
            FROM " . $this->db_table . " 
            WHERE teacher_reg_id = :teacher_reg_id AND operator = :operator AND status=1 LIMIT 3";  
 
        
        $stmt = $this->conn->prepare($sqlQuery);

        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 
        
        $this->operator=htmlspecialchars(strip_tags($this->operator)); 
        $stmt->bindParam(":operator", $this->operator); 

        $stmt->execute();
        return $stmt;
        
        

    } 





    public function getSingle(){

        $sqlQuery = "SELECT
                        reg_id, 
                        number1, 
                        number2,  
                        answer, 
                        operator,  
                        numeracy,  
                        status, 
                        teacher_reg_id, 
                        date_added
                FROM
                    ". $this->db_table ."
                WHERE 
                    reg_id = ?  
                LIMIT 0,1";

        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->bindParam(1, $this->reg_id); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $this->reg_id = $dataRow['reg_id'];
        $this->number1 = $dataRow['number1'];
        $this->number2 = $dataRow['number2'];
        $this->answer = $dataRow['answer'];
        $this->operator = $dataRow['operator'];
        $this->numeracy = $dataRow['numeracy'];
        $this->status = $dataRow['status'];
        $this->teacher_reg_id = $dataRow['teacher_reg_id'];
        $this->date_added = $dataRow['date_added']; 
        

    } 

    public function create(){

        $sqlQuery = "INSERT INTO
                    ". $this->db_table ."
                SET
                    number1 = :number1,
                    number2 = :number2,
                    status = :status,
                    answer = :answer, 
                    operator = :operator, 
                    numeracy = :numeracy, 
               
                  
                    teacher_reg_id = :teacher_reg_id";  
                   

        $stmt = $this->conn->prepare($sqlQuery);

        $this->number1=htmlspecialchars(strip_tags($this->number1)); 
        $this->number2=htmlspecialchars(strip_tags($this->number2)); 
        $this->status=htmlspecialchars(strip_tags($this->status)); 
        $this->answer=htmlspecialchars(strip_tags($this->answer)); 
        $this->operator=htmlspecialchars(strip_tags($this->operator)); 
        $this->numeracy=htmlspecialchars(strip_tags($this->numeracy)); 
       
        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
       
      
        $stmt->bindParam(":number1", $this->number1);
        $stmt->bindParam(":number2", $this->number2);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":answer", $this->answer);
        $stmt->bindParam(":operator", $this->operator);
        $stmt->bindParam(":numeracy", $this->numeracy);
    
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id);  


        if($stmt->execute()){
            return true;
        } 

      
        return false;

    }

    public function update(){

        $sqlQuery = "UPDATE
                    ". $this->db_table ."
                SET
                    number1 = :number1,
                    number2 = :number2,
                    status = :status,
                    answer = :answer, 
                    operator = :operator, 
                    numeracy = :numeracy, 
                 
                    teacher_reg_id = :teacher_reg_id
                WHERE 
                    reg_id = :reg_id";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->number1=htmlspecialchars(strip_tags($this->number1)); 
        $this->number2=htmlspecialchars(strip_tags($this->number2)); 
        $this->status=htmlspecialchars(strip_tags($this->status)); 
        $this->answer=htmlspecialchars(strip_tags($this->answer)); 
        $this->operator=htmlspecialchars(strip_tags($this->operator)); 
        $this->numeracy=htmlspecialchars(strip_tags($this->numeracy)); 
      
        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
       
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));  
 
        $stmt->bindParam(":number1", $this->number1);
        $stmt->bindParam(":number2", $this->number2);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":answer", $this->answer);
        $stmt->bindParam(":operator", $this->operator);
        $stmt->bindParam(":numeracy", $this->numeracy);
     
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id);   
       
        $stmt->bindParam(":reg_id", $this->reg_id);

        if($stmt->execute()){
            return true;
        }
        return false;
    }

    function delete(){

        $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE reg_id = ?";
        $stmt = $this->conn->prepare($sqlQuery);
    
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt->bindParam(1, $this->reg_id);

        if($stmt->execute()){
            return true;
        }

        return false;
    }

}