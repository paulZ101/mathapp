<?php
class Pupils
{
    // Connection
    private $conn;
    // Table
    private $db_table = "tbl_pupils";
    // Columns
    public $reg_id;
    public $username;
    public $password;   
    public $status;
    public $date_added;
    public $admin_id;
    public $teacher_reg_id;
    public $section_id;
    public $reg_type;
    // Db connection
    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function get(){

        $sqlQuery = "
            SELECT P.reg_id, P.username, P.section_id, P.status, S.section_name
            
            FROM " . $this->db_table . " P
            JOIN tbl_sections  S ON P.section_id = S.reg_id
            
            WHERE P.teacher_reg_id = :teacher_reg_id "; 

        $stmt = $this->conn->prepare($sqlQuery); 

        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 

        $stmt->execute();
        return $stmt;
    } 

    public function getAll(){

        $sqlQuery = "
            SELECT P.reg_id, P.username, P.section_id, P.status, S.section_name
            
            FROM " . $this->db_table . " P
            JOIN tbl_sections  S ON P.section_id = S.reg_id"; 

        $stmt = $this->conn->prepare($sqlQuery); 

        $stmt->execute();
        return $stmt;
    } 

    public function getSingle(){

        $sqlQuery = "SELECT
                    reg_id,
                    username,
                    status,
                    admin_id,
                    teacher_reg_id 
                FROM
                    ". $this->db_table ."
                WHERE 
                    reg_id = ?  
                LIMIT 0,1";

        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->bindParam(1, $this->reg_id); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $this->reg_id = $dataRow['reg_id'];
        $this->username = $dataRow['username'];
        $this->teacher_reg_id = $dataRow['teacher_reg_id'];

    } 

    public function getByUsername(){
        $sqlQuery = "SELECT
            reg_id, 
            username,
            password,
            status,
            admin_id,
            teacher_reg_id  
        FROM
            ". $this->db_table ."
        WHERE 
            username = ?  
        LIMIT 0,1";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->username=htmlspecialchars(strip_tags($this->username));

        $stmt->bindParam(1, $this->username); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->reg_id = $dataRow['reg_id'];
        $this->username = $dataRow['username'];
        $this->password = $dataRow['password'];
        $this->teacher_reg_id = $dataRow['teacher_reg_id']; 
    } 

    public function create(){

        $sqlQuery = "INSERT INTO
                    ". $this->db_table ."
                SET
                    username = :username,
                        
                    status = '1',
                    section_id = :section_id, 
                    reg_type = :reg_type,
                    teacher_reg_id = :teacher_reg_id"; 

        $stmt = $this->conn->prepare($sqlQuery);

        $this->username=htmlspecialchars(strip_tags($this->username));
        $this->section_id=htmlspecialchars(strip_tags($this->section_id)); 
        $this->reg_type=htmlspecialchars(strip_tags($this->reg_type)); 
        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
      
        $stmt->bindParam(":username", $this->username);
        $stmt->bindParam(":section_id", $this->section_id); 
        $stmt->bindParam(":reg_type", $this->reg_type); 
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 


        if($stmt->execute()){
            return true;
        }

        return false;

    }

    public function update(){

        $sqlQuery = "UPDATE
                    ". $this->db_table ."
                SET
                    username = :username
                WHERE 
                    reg_id = :reg_id";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->username=htmlspecialchars(strip_tags($this->username));   
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));  
 
        $stmt->bindParam(":username", $this->username);   
        $stmt->bindParam(":reg_id", $this->reg_id);

        if($stmt->execute()){
            return true;
        }
        return false;
    }

    function delete(){

        $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE reg_id = ?";
        $stmt = $this->conn->prepare($sqlQuery);
    
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt->bindParam(1, $this->reg_id);

        if($stmt->execute()){
            return true;
        }

        return false;
    }

}