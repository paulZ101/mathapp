<?php
class Questions
{
    // Connection
    private $conn;
    // Table
    private $db_table = "tbl_questions";
    // Columns
    public $reg_id;
    public $question;
    public $option1;
    public $option2;
    public $option3;
    public $option4; 
    public $answer;
    public $lesson_id;
    public $status;
    public $date_added;
    public $teacher_reg_id;
    public $imagefile;
    // Db connection
    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function get()
    {
        
        $sqlQuery = "
            SELECT 
                Q.reg_id, 
                Q.question, 
                Q.option1, 
                Q.option2, 
                Q.option3, 
                Q.option4, 
                Q.answer, 
                Q.lesson_id,  
                Q.status, 
                Q.teacher_reg_id, 
                Q.date_added, 
                L.lesson_title,
                M.module_title,
                C.category_name,
                Q.imagefile
            FROM " . $this->db_table . " Q
            LEFT JOIN tbl_lessons L ON Q.lesson_id = L.reg_id
            LEFT JOIN tbl_modules M ON L.module_id = M.reg_id
			LEFT JOIN tbl_categories C ON M.category_id = C.id
            WHERE Q.teacher_reg_id = :teacher_reg_id ";  
 
        
        $stmt = $this->conn->prepare($sqlQuery);

        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 

        $stmt->execute();
        return $stmt;
        
    }

    public function getByLessonID()
    {
        $sqlQuery = "
            SELECT 
                Q.reg_id, 
                Q.question, 
                Q.option1, 
                Q.option2, 
                Q.option3, 
                Q.option4, 
                Q.answer, 
                Q.lesson_id,  
                Q.status, 
                Q.teacher_reg_id, 
                Q.date_added, 
                L.lesson_title,
                Q.imagefile
            FROM " . $this->db_table . " Q
            LEFT JOIN tbl_lessons L ON Q.lesson_id = L.reg_id
            WHERE Q.lesson_id = :lesson_id
            ORDER BY RAND() ";

      

        $stmt = $this->conn->prepare($sqlQuery); 
        
        $this->lesson_id=htmlspecialchars(strip_tags($this->lesson_id)); 
        $stmt->bindParam(":lesson_id", $this->lesson_id); 

        $stmt->execute();
        return $stmt;
        
    }


    public function getByLessonIDLIMIT5()
    {
        $sqlQuery = "
            SELECT 
                Q.reg_id, 
                Q.question, 
                Q.option1, 
                Q.option2, 
                Q.option3, 
                Q.option4, 
                Q.answer, 
                Q.lesson_id,  
                Q.status, 
                Q.teacher_reg_id, 
                Q.date_added, 
                L.lesson_title,
                Q.imagefile
            FROM " . $this->db_table . " Q
            LEFT JOIN tbl_lessons L ON Q.lesson_id = L.reg_id
            WHERE Q.lesson_id = :lesson_id
            ORDER BY RAND() LIMIT 5 ";

      

        $stmt = $this->conn->prepare($sqlQuery); 
        
        $this->lesson_id=htmlspecialchars(strip_tags($this->lesson_id)); 
        $stmt->bindParam(":lesson_id", $this->lesson_id); 

        $stmt->execute();
        return $stmt;
        
    }


    public function getSingle(){

        $sqlQuery = "SELECT
                    reg_id,
                    question,
                    option1, 
                    option2, 
                    option3, 
                    option4, 
                    answer, 
                    status, 
                    lesson_id,
                    teacher_reg_id,
                    imagefile
                FROM
                    ". $this->db_table ."
                WHERE 
                    reg_id = ?  
                LIMIT 0,1";

        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->bindParam(1, $this->reg_id); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $this->reg_id = $dataRow['reg_id'];
        $this->question = $dataRow['question'];
        $this->option1 = $dataRow['option1'];
        $this->option2 = $dataRow['option2'];
        $this->option3 = $dataRow['option3'];
        $this->option4 = $dataRow['option4'];
        $this->answer = $dataRow['answer'];
        $this->status = $dataRow['status'];
        $this->lesson_id = $dataRow['lesson_id'];
        $this->teacher_reg_id = $dataRow['teacher_reg_id'];
        $this->imagefile = $dataRow['imagefile'];
        

    } 

    public function create(){

        $sqlQuery = "INSERT INTO
                    ". $this->db_table ."
                SET
                    question = :question,
                    status = :status,
                    lesson_id = :lesson_id, 
                    option1 = :option1, 
                    option2 = :option2, 
                    option3 = :option3, 
                    option4 = :option4, 
                    answer = :answer, 
                    imagefile = :imagefile, 
                    teacher_reg_id = :teacher_reg_id";  

        $stmt = $this->conn->prepare($sqlQuery);

        $this->question=htmlspecialchars(strip_tags($this->question)); 
        $this->status=htmlspecialchars(strip_tags($this->status)); 
        $this->option1=htmlspecialchars(strip_tags($this->option1)); 
        $this->option2=htmlspecialchars(strip_tags($this->option2)); 
        $this->option3=htmlspecialchars(strip_tags($this->option3)); 
        $this->option4=htmlspecialchars(strip_tags($this->option4)); 
        $this->answer=htmlspecialchars(strip_tags($this->answer)); 
        $this->imagefile=htmlspecialchars(strip_tags($this->imagefile)); 
      
        $this->lesson_id=htmlspecialchars(strip_tags($this->lesson_id)); 
        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
      
        $stmt->bindParam(":question", $this->question);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":option1", $this->option1);
        $stmt->bindParam(":option2", $this->option2);
        $stmt->bindParam(":option3", $this->option3);
        $stmt->bindParam(":option4", $this->option4);
        $stmt->bindParam(":answer", $this->answer);
        $stmt->bindParam(":imagefile", $this->imagefile);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":lesson_id", $this->lesson_id, PDO::PARAM_INT);  
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 


        if($stmt->execute()){
            return true;
        } 

        return false;

    }

    public function update(){

        $sqlQuery = "UPDATE
                    ". $this->db_table ."
                SET
                    question = :question,
                    lesson_id = :lesson_id, 

                    option1 = :option1, 
                    option2 = :option2, 
                    option3 = :option3, 
                    option4 = :option4, 
                    answer = :answer, 
                    imagefile = :imagefile,
                    status = :status
                WHERE 
                    reg_id = :reg_id";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->question=htmlspecialchars(strip_tags($this->question));   
        $this->status=htmlspecialchars(strip_tags($this->status));   
        $this->option1=htmlspecialchars(strip_tags($this->option1)); 
        $this->option2=htmlspecialchars(strip_tags($this->option2)); 
        $this->option3=htmlspecialchars(strip_tags($this->option3)); 
        $this->option4=htmlspecialchars(strip_tags($this->option4)); 
        $this->answer=htmlspecialchars(strip_tags($this->answer)); 
        $this->imagefile=htmlspecialchars(strip_tags($this->imagefile)); 
      
        $this->lesson_id=htmlspecialchars(strip_tags($this->lesson_id)); 
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));  
 
        $stmt->bindParam(":question", $this->question);   
        $stmt->bindParam(":status", $this->status);   
        $stmt->bindParam(":option1", $this->option1);
        $stmt->bindParam(":option2", $this->option2);
        $stmt->bindParam(":option3", $this->option3);
        $stmt->bindParam(":option4", $this->option4);
        $stmt->bindParam(":answer", $this->answer);
        $stmt->bindParam(":imagefile", $this->imagefile);
        $stmt->bindParam(":lesson_id", $this->lesson_id, PDO::PARAM_INT);  
       
        $stmt->bindParam(":reg_id", $this->reg_id);

        if($stmt->execute()){
            return true;
        }
        return false;
    }

    function delete(){

        $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE reg_id = ?";
        $stmt = $this->conn->prepare($sqlQuery);
    
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt->bindParam(1, $this->reg_id);

        if($stmt->execute()){
            return true;
        }

        return false;
    }

}