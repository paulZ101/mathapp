<?php
class QuizResults
{
    // Connection
    private $conn;
    // Table
    private $db_table = "tbl_results_quiz";
    private $columns =
        [
            'reg_id',
            'right_answer',
            'wrong_answer',
            'unanswered',
            'pupil_id',
            'lesson_id',
            'status',
            'date_added' 
        ];
    // Columns
    public $reg_id;
    public $right_answer;
    public $wrong_answer;
    public $status;
    public $unanswered;
    public $pupil_id;
    public $lesson_id;
    public $date_added; 
    public $teacher_reg_id;

    // Db connection
    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function get()
    {

        // T.teacher_reg_id, T.first_name as Tfirst_name, T.last_name as Tlast_name

        $sqlQuery = "
            SELECT R.right_answer, R.wrong_answer, R.unanswered , R.date_added,
            P.reg_id, P.username, P.section_id, P.status, S.section_name,
            P.first_name, P.last_name, P.student_id,  M.module_title, L.lesson_title,
            
            C.category_name
            
            
            FROM " . $this->db_table . " R
            LEFT JOIN tbl_pupils P ON P.reg_id = R.pupil_id
            LEFT JOIN tbl_sections S ON P.section_id = S.reg_id
            LEFT JOIN tbl_lessons L ON R.lesson_id = L.reg_id
            LEFT JOIN tbl_modules M ON L.module_id = M.reg_id
            LEFT JOIN tbl_categories C ON M.category_id = C.id

            LEFT JOIN tbl_teachers T ON P.teacher_reg_id = T.reg_id
            
            WHERE T.reg_id = :teacher_reg_id ORDER by date_added DESC"; 

        $stmt = $this->conn->prepare($sqlQuery); 

        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 

 

        $stmt->execute();
        return $stmt;

    }


}