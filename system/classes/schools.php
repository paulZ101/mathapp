<?php
class Schools
{
    // Connection
    private $conn;
    // Table
    private $db_table = "tbl_schools";
    private $columns = 
        [   
            'reg_id',
            'school_id',
            'school_name',
            'school_address',
            'date_added',
            'status',
            'admin_id',
            'language'

        ];
    // Columns
    public $reg_id;
    public $school_id;
    public $school_name;    
    public $school_address;
    public $date_added;
    public $status;
    public $admin_id;
    public $language;
    
    // Db connection
    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function get(){

        $columns = implode(', ', $this->columns);

        $sqlQuery = "SELECT ".$columns." FROM " . $this->db_table . "";
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        return $stmt;
        
    }

    public function getActive(){
        $sqlQuery = "SELECT reg_id, school_name, school_id, school_address, date_added, admin_id, status FROM " . $this->db_table . " WHERE status = 1";
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        return $stmt;
    } 


    public function getSingle(){

        $columns = implode(', ', $this->columns);

        $sqlQuery = "SELECT  ".$columns." 
                FROM
                    ". $this->db_table ."
                WHERE 
                    reg_id = ?  
                LIMIT 0,1";

        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->bindParam(1, $this->reg_id); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $this->reg_id = $dataRow['reg_id'];
        $this->school_name = $dataRow['school_name'];
        $this->school_id = $dataRow['school_id'];
        $this->school_address = $dataRow['school_address']; 
        $this->status = $dataRow['status']; 
        $this->admin_id = $dataRow['admin_id']; 
        $this->language = $dataRow['language']; 

    } 

    public function getByName(){
        $sqlQuery = "SELECT
            reg_id, 
            school_name,
            school_id, 
            school_address, 
            date_added, 
            admin_id 
        FROM
            ". $this->db_table ."
        WHERE 
            school_name = ?  
        LIMIT 0,1";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->school_name=htmlspecialchars(strip_tags($this->school_name));

        $stmt->bindParam(1, $this->school_name); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->reg_id = $dataRow['reg_id'];
        $this->school_name = $dataRow['school_name'];
        $this->school_id = $dataRow['school_id'];
        $this->school_address = $dataRow['school_address'];
        $this->status = $dataRow['status'];
        $this->date_added = $dataRow['date_added']; 
    } 

    public function create(){

        $sqlQuery = "INSERT INTO
                    ". $this->db_table ."
                SET
                    school_name = :school_name, 
                    school_id = :school_id, 
                    school_address = :school_address,  
                    status = :status,
                    admin_id = :admin_id,
                    language = :language";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->school_name=htmlspecialchars(strip_tags($this->school_name));
        $this->school_id=htmlspecialchars(strip_tags($this->school_id));
        $this->school_address=htmlspecialchars(strip_tags($this->school_address)); 
        $this->status=htmlspecialchars(strip_tags($this->status));
        $this->admin_id=htmlspecialchars(strip_tags($this->admin_id));
        $this->language=htmlspecialchars(strip_tags($this->language));
    
        $stmt->bindParam(":school_name", $this->school_name);
        $stmt->bindParam(":school_id", $this->school_id);
        $stmt->bindParam(":school_address", $this->school_address); 
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":admin_id", $this->admin_id);
        $stmt->bindParam(":language", $this->language);
 
        if($stmt->execute()){
            return true;
        }

        return false;

    }

    public function update(){

        $sqlQuery = "UPDATE
                    ". $this->db_table ."
                SET
                    school_name = :school_name,  
                    school_id = :school_id, 
                    school_address = :school_address, 
                    status = :status,
                    language = :language
                WHERE 
                    reg_id = :reg_id";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->school_id=htmlspecialchars(strip_tags($this->school_id));
        $this->school_name=htmlspecialchars(strip_tags($this->school_name));
        $this->school_address=htmlspecialchars(strip_tags($this->school_address));  
        $this->status=htmlspecialchars(strip_tags($this->status));  
        $this->language=htmlspecialchars(strip_tags($this->language));  
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));  
        
        $stmt->bindParam(":school_id", $this->school_id);
        $stmt->bindParam(":school_name", $this->school_name);
        $stmt->bindParam(":school_address", $this->school_address);  
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":language", $this->language);
        $stmt->bindParam(":reg_id", $this->reg_id);

        if($stmt->execute()){
            return true;
        }
        return false;
    }

    function delete(){

        $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE reg_id = ?";
        $stmt = $this->conn->prepare($sqlQuery);
    
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt->bindParam(1, $this->reg_id);

        if($stmt->execute()){
            return true;
        }

        return false;
    }

}