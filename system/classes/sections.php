<?php
class Sections
{
    // Connection
    private $conn;
    // Table
    private $db_table = "tbl_sections";
    private $columns = 
        [   
            'reg_id',
            'section_name',
            'section_code',
            'status',
            'teacher_reg_id',
            'date_added',
            'admin_id',
            'q1',
            'q2',
            'q3',
            'q4',
            'n1',
            'n2'
        ];
    // Columns
    public $reg_id;
    public $section_name;
    public $section_code;    
    public $status;
    public $date_added;
    public $admin_id;
    public $teacher_reg_id;
    public $q1,$q2,$q3,$q4;
    public $n1,$n2;
    public $school_reg_id;
    // Db connection
    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function get(){

        $columns = implode(', ', $this->columns);

        $sqlQuery = "SELECT ".$columns." FROM " . $this->db_table . " WHERE teacher_reg_id = :teacher_reg_id ";
        $stmt = $this->conn->prepare($sqlQuery);

        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 

        
        $stmt->execute();
        return $stmt;

    } 

 
    public function getActive(){

        $columns = implode(', ', $this->columns);

        $sqlQuery = "SELECT ".$columns."  FROM " . $this->db_table . " WHERE status = 1 AND teacher_reg_id = :teacher_reg_id";
        $stmt = $this->conn->prepare($sqlQuery);

        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 



        $stmt->execute();
        return $stmt;
    } 


    public function getBySchool(){  
        
        $sqlQuery = "SELECT SE.reg_id, SE.section_name
     
            FROM tbl_sections SE
            LEFT JOIN tbl_teachers T ON SE.teacher_reg_id = T.reg_id
            LEFT JOIN tbl_schools SC ON T.school_reg_id = SC.reg_id
                        
            WHERE SC.reg_id = :school_reg_id AND SE.status = 1 ";
        
        $stmt = $this->conn->prepare($sqlQuery);

        $this->school_reg_id=htmlspecialchars(strip_tags($this->school_reg_id)); 
        $stmt->bindParam(":school_reg_id", $this->school_reg_id);  

        $stmt->execute();
        return $stmt;
    } 



    public function getSingle(){

        $columns = implode(', ', $this->columns);

        $sqlQuery = "SELECT
                    ".$columns."
                FROM
                    ". $this->db_table ."
                WHERE 
                    reg_id = ?  
                LIMIT 0,1";

        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->bindParam(1, $this->reg_id); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $this->reg_id = $dataRow['reg_id'];
        $this->section_name = $dataRow['section_name'];
        $this->section_code = $dataRow['section_code'];
        $this->status = $dataRow['status'];
        $this->teacher_reg_id = $dataRow['teacher_reg_id'];
        $this->teacher_reg_id = $dataRow['teacher_reg_id'];
        $this->q1 = $dataRow['q1'];
        $this->q2 = $dataRow['q2'];
        $this->q3 = $dataRow['q3'];
        $this->q4 = $dataRow['q4'];
        $this->n1 = $dataRow['n1'];
        $this->n2 = $dataRow['n2'];
        

    } 

    public function getByName(){

        $columns = implode(', ', $this->columns);

        $sqlQuery = "SELECT
            ".$columns."
        FROM
            ". $this->db_table ."
        WHERE 
            section_name = ?  
        LIMIT 0,1";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->section_name=htmlspecialchars(strip_tags($this->section_name));

        $stmt->bindParam(1, $this->section_name); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->reg_id = $dataRow['reg_id'];
        $this->section_name = $dataRow['section_name'];
        $this->section_code = $dataRow['section_code'];
        $this->status = $dataRow['status'];
        $this->teacher_reg_id = $dataRow['teacher_reg_id'];
        $this->admin_id = $dataRow['admin_id']; 

    } 

    public function getByCode(){

        $columns = implode(', ', $this->columns);

        $sqlQuery = "SELECT
            ".$columns."
        FROM
            ". $this->db_table ."
        WHERE 
            section_code = ?  
        LIMIT 0,1";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->section_code=htmlspecialchars(strip_tags($this->section_code));

        $stmt->bindParam(1, $this->section_code); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->reg_id = $dataRow['reg_id'];
        $this->section_name = $dataRow['section_name'];
        $this->section_code = $dataRow['section_code'];
        $this->status = $dataRow['status'];
        $this->teacher_reg_id = $dataRow['teacher_reg_id'];
        $this->admin_id = $dataRow['admin_id']; 

    } 



    public function create(){

        // section_code = :section_code, 

        $sqlQuery = "INSERT INTO
                    ". $this->db_table ."
                SET
                    section_name = :section_name, 
                    
                    status = :status,
                    q1 = :q1,
                    q2 = :q2,
                    q3 = :q3,
                    q4 = :q4,
                    n1 = :n1,
                    n2 = :n2,
                    teacher_reg_id = :teacher_reg_id";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->section_name=htmlspecialchars(strip_tags($this->section_name));
        // $this->section_code=htmlspecialchars(strip_tags($this->section_code));
        $this->status=htmlspecialchars(strip_tags($this->status));
        $this->q1=htmlspecialchars(strip_tags($this->q1));
        $this->q2=htmlspecialchars(strip_tags($this->q2));
        $this->q3=htmlspecialchars(strip_tags($this->q3));
        $this->q4=htmlspecialchars(strip_tags($this->q4));
        $this->n1=htmlspecialchars(strip_tags($this->n1));
        $this->n2=htmlspecialchars(strip_tags($this->n2));
        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        
        $stmt->bindParam(":section_name", $this->section_name);
        // $stmt->bindParam(":section_code", $this->section_code);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":q1", $this->q1);
        $stmt->bindParam(":q2", $this->q2);
        $stmt->bindParam(":q3", $this->q3);
        $stmt->bindParam(":q4", $this->q4);
        $stmt->bindParam(":n1", $this->n1);
        $stmt->bindParam(":n2", $this->n2);
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id);

        if($stmt->execute()){
            return true;
        }

       
//    echo '<pre>';
//    print_r($stmt->errorInfo());
//    echo '</pre>';
//    exit;

        return false;

    }

    public function update(){

        $sqlQuery = "UPDATE
                    ". $this->db_table ."
                SET
                    section_name = :section_name,
                    status = :status,
                    q1 = :q1,
                    q2 = :q2,
                    q3 = :q3,
                    q4 = :q4,
                    n1 = :n1,
                    n2 = :n2
                WHERE 
                    reg_id = :reg_id";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->section_name=htmlspecialchars(strip_tags($this->section_name));   
        $this->status=htmlspecialchars(strip_tags($this->status)); 
        $this->q1=htmlspecialchars(strip_tags($this->q1));
        $this->q2=htmlspecialchars(strip_tags($this->q2));
        $this->q3=htmlspecialchars(strip_tags($this->q3));
        $this->q4=htmlspecialchars(strip_tags($this->q4));  
        $this->n1=htmlspecialchars(strip_tags($this->n1));
        $this->n2=htmlspecialchars(strip_tags($this->n2));  
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));  
 
        $stmt->bindParam(":section_name", $this->section_name);   
        $stmt->bindParam(":status", $this->status);   
        $stmt->bindParam(":q1", $this->q1);
        $stmt->bindParam(":q2", $this->q2);
        $stmt->bindParam(":q3", $this->q3);
        $stmt->bindParam(":q4", $this->q4);
        $stmt->bindParam(":n1", $this->n1);
        $stmt->bindParam(":n2", $this->n2);
        $stmt->bindParam(":reg_id", $this->reg_id);

        if($stmt->execute()){
            return true;
        }
        return false;
    }

    function delete(){

        $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE reg_id = ?";
        $stmt = $this->conn->prepare($sqlQuery);
    
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt->bindParam(1, $this->reg_id);

        if($stmt->execute()){
            return true;
        }

        return false;
    }




    public function updateSectionCode(){

        $sqlQuery = "UPDATE
                    ". $this->db_table ."
                SET
                    section_code = :section_code
                     
                WHERE 
                    reg_id = :reg_id";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->section_code=htmlspecialchars(strip_tags($this->section_code));    
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));  
 
        $stmt->bindParam(":section_code", $this->section_code);   
        $stmt->bindParam(":reg_id", $this->reg_id);

        if($stmt->execute()){
            return true;
        }

        
        // echo '<pre>';
        //     print_r($stmt->errorInfo());
        //     echo '</pre>';
        //     exit;

        return false;

    }





}