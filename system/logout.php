<?php 
require_once '../global.php'; 

$account = $_GET['account']; 
 
switch ($account) {
  case 'admin':
    $path = ADMIN_LOGIN;
    break;
  case 'pupil':
    $path = PUPIL_LOGIN;
    break;
  case 'teacher':
    $path = TEACHER_LOGIN;
    break; 
  default:
    $path = BASEPATH;
}

if(isset($_SESSION['mathapp']['login'])){

 

  unset($_SESSION['mathapp']['login']);

 

}

header('location: '.$path);