<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                
                
            </div>

            <div class="row">
                <div class="col-12 col-xl-8">
                    <div class="card card-body border-0 shadow mb-4">
                        <h2 class="h5 mb-4"><?=$page['title'];?></h2>
                        <?php resultMessage(); ?>
                        <form action="<?=SUBMIT?>" method="post">
                        <input type="hidden" name="id" value="<?=!empty($_GET['id'])? $_GET['id']:'';?>">
                
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="school_name">School Name</label>
                                        <input class="form-control" name="school_name" id="school_name" type="text" placeholder="Enter your school_name" value="<?=!empty($data['school_name'])? $data['school_name']:'';?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="school_id">School ID</label>
                                        <input class="form-control" name="school_id" id="school_id" type="text" placeholder="Also your school_id" value="<?=!empty($data['school_id'])? $data['school_id']:'';?>" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="school_id">School Address</label>
                                        <input class="form-control" name="school_address" id="school_address" type="text" placeholder="Also your school_addresss" value="<?=!empty($data['school_address'])? $data['school_address']:'';?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="gender">Status</label>
                                    <select class="form-select mb-0" id="gender" name="status" aria-label="Gender select example">
                                        <option value="">Select Status</option>
                                        <option value="0" <?=(!empty($data['status']) && $data['status']=='0') ? 'selected':'';?>>Inactive</option>
                                        <option value="1" <?=(!empty($data['status']) && $data['status']=='1') ? 'selected':'';?>>Active</option> 
                                    </select>
                                </div>
                            </div>

                            <div class="row align-items-center">
                              
                                <div class="col-md-6 mb-3">
                                    <label for="gender">Language</label>
                                    <select class="form-select mb-0" id="gender" name="language" aria-label="Gender select example">
                                        <option value="">Select Language</option>
                                        <option value="english" <?=(!empty($data['language']) && $data['language']=='english') ? 'selected':'';?>>English</option>
                                        <option value="waray" <?=(!empty($data['language']) && $data['language']=='waray') ? 'selected':'';?>>Waray</option> 
                                        <option value="bisaya" <?=(!empty($data['language']) && $data['language']=='bisaya') ? 'selected':'';?>>Bisaya</option> 
                                    </select>
                                </div>
                            </div>


 
                              
                            <div class="mt-3">
                                <button class="btn btn-primary mt-2 animate-up-2" type="submit">Save</button>
                            </div>
                        </form>
                    </div>
                   
                </div>
              
            </div>