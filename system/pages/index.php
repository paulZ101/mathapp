 
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Primary Meta Tags -->
<title>MathApp 3</title>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 

<link rel="apple-touch-icon" sizes="120x120" href="<?=THEME2;?>assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=THEME2;?>assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=THEME2;?>assets/img/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

<!-- Fontawesome -->
<link type="text/css" href="<?=THEME1;?>vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">

<!-- Pixel CSS -->
<link type="text/css" href="<?=THEME1;?>css/pixel.css" rel="stylesheet">


</head>

<body>
    

<!-- <header class="header-global">
    <nav id="navbar-main" aria-label="Primary navigation" class="navbar navbar-main navbar-expand-lg navbar-theme-primary headroom navbar-dark navbar-theme-secondary">
        <div class="container position-relative">
            
            <div class="navbar-collapse collapse me-auto" id="navbar_global">
                
               
            </div>
            <div class="d-flex align-items-center">
                <a href="<?= PUPIL_LOGIN;?>" target="_blank" class="btn btn-outline-tertiary   me-md-3"><span class="fas fa-users me-2"></span> PUPIL LOGIN</a>
                <a href="<?= TEACHER_LOGIN;?>" target="_blank" class="btn btn-tertiary"><i class="fas fa-users me-2"></i> TEACHER LOGIN</a>
                <button class="navbar-toggler ms-2" type="button" data-bs-toggle="collapse" data-bs-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </nav>
</header> -->
    


<header class="header-global">
    <nav id="navbar-main" aria-label="Primary navigation" class="navbar navbar-main navbar-expand-lg navbar-theme-primary headroom navbar-dark navbar-theme-secondary">
        <div class="container position-relative">
            
            <div class="navbar-collapse collapse me-auto" id="navbar_global">
                <div class="navbar-collapse-header">
                    <div class="row">
                        <div class="col-6 collapse-brand">
                            <a href="../../index.html">
                                <img src="../../assets/img/brand/dark.svg" alt="Themesberg logo">
                            </a>
                        </div>
                        <div class="col-6 collapse-close">
                            <a href="#navbar_global" class="fas fa-times" data-bs-toggle="collapse" data-bs-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" title="close" aria-label="Toggle navigation"></a>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="d-flex align-items-center">
                <a href="<?= PUPIL_LOGIN;?>" class="btn btn-outline-tertiary   me-2"><span class="fas fa-users me-2"></span> PUPIL LOGIN</a>
                <a href="<?= TEACHER_LOGIN;?>" class="btn btn-tertiary"><i class="fas fa-users me-2"></i> TEACHER LOGIN</a>
            </div>
        </div>
    </nav>
</header>



    <main class="mt-5">
 

        <!-- Hero -->
    
        <!-- End of Hero section -->
        <!-- Section -->
        <section class="section section-md">
            <div class="container">
                <div class="row align-items-center justify-content-around">
                    <div class="col-md-6 col-xl-6 mb-5">
                        <img class="organic-radiusx img-fluid" src="<?=BASEPATH;?>assets/images/logo.png" alt="Office Desk">
                    </div>
                    <div class="col-md-6 col-xl-5 text-center text-md-left">
                        <h2 class="h1 mb-5">MathApp3.com</h2>
                        <p class="lead">MathApp is a web-based system that aims to make the role of teachers easier in making their lessons and quizzes. Perfect for grade 3 pupils because it makes their learning fun. Minimize the use of paper and pens.</p>
                       
                    </div>
                </div>
            </div>
        </section>
        <!-- End of section -->
        <!-- Section -->
        <section class="section section-lg pt-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-lg-4 text-center">
                        <!-- Visit Box -->
                        <div class="icon-box mb-4">
                            <div class="icon icon-primary mb-4">
                                <span class="fas fa-school"></span>
                            </div>
                            <h3 class="h5">Schools</h3>
                            <span class="counter display-3 text-gray d-block"><?= $schoolsCount;?></span>
                        </div>
                        <!-- End of Visit Box -->
                    </div>
                    <div class="col-md-4 col-lg-4 text-center">
                        <!-- Call Box -->
                        <div class="icon-box mb-4">
                            <div class="icon icon-primary mb-4">
                                <span class="fas fa-users"></span>
                            </div>
                            <h3 class="h5">Teachers</h3>
                            <span class="counter display-3 text-gray d-block"><?= $teachersCount;?></span>
                        </div>
                        <!-- End of Call Box -->
                    </div>
                    <div class="col-md-4 col-lg-4 text-center">
                        <!-- Email Box -->
                        <div class="icon-box mb-4">
                            <div class="icon icon-primary mb-4">
                                <span class="fas fa-users"></span>
                            </div>
                            <h3 class="h5">Pupils</h3>
                            <span class="counter display-3 text-gray d-block"><?=$pupilsCount?></span>
                        </div>
                        <!-- End of Email Box -->
                    </div>
                </div>
            </div>
        </section>
        <!-- End of section -->
        <!-- Section -->
       
        
        <section class="section section-lg">
            <div class="container">
                <div class="row mb-5 mb-lg-6">
                    <div class="col-12 col-md-9 col-lg-8 text-center mx-auto">
                        <h2 class="h1 mb-4">Team BSCS Batch 2023</h2>
                        <p class="lead">We have developed a MULTI -PLATFORM EDUCATIONAL APPLICATION FOR GRADE 3 MATHEMATICS SUBJECT
                        </p>
                    </div>
                </div>


                <div class="row mb-5 mb-lg-6">

                <?php
                $arrTeam[0]['fullname'] = 'Jhon Paul Bulan';
                $arrTeam[0]['position'] = 'Team Leader / Programmer';
                $arrTeam[0]['image'] = 'linkedin-profile-image-JhonPaulBulan.jpeg';

                $arrTeam[1]['fullname'] = 'Rose Estacion';
                $arrTeam[1]['position'] = 'Doc Assistant';
                $arrTeam[1]['image'] = 'linkedin-profile-image-RoseEstacion.jpeg';

                $arrTeam[2]['fullname'] = 'Roliegien Holanda';
                $arrTeam[2]['position'] = 'System Researcher';
                $arrTeam[2]['image'] = 'linkedin-profile-image-RoliegienHolanda.jpeg';

                $arrTeam[3]['fullname'] = 'Annabel Mascariñas';
                $arrTeam[3]['position'] = 'System Researcher Developer';
                $arrTeam[3]['image'] = 'linkedin-profile-image-AnnabelMascarinas2.jpeg';

                $arrTeam[4]['fullname'] = 'Eugene Wynnco Pigao';
                $arrTeam[4]['position'] = 'Researcher Assistant';
                $arrTeam[4]['image'] = 'linkedin-profile-image-EugeneWynncoPigao.jpeg';


                
                
                foreach($arrTeam as $team): ?>

           
                    <div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0 ">
                        <div class="card shadow border-gray-300 mb-5">
                            <img src="<?=BASEPATH;?><?= $team['image'];?>" class="card-img-top rounded-top" alt="<?= $team['fullname'];?>">
                            <div class="card-body">
                                <h3 class="h4 card-title mb-2"><?= $team['fullname'];?></h3>
                                <span class="card-subtitle text-gray fw-normal"><?= $team['position'];?></span>
                                <!-- <p class="card-text my-3">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
                                <ul class="list-unstyled d-flex mt-3 mb-0">
                                    <li>
                                        <a href="#" target="_blank" aria-label="facebook social link" class="icon-facebook me-3">
                                            <span class="fab fa-facebook-f"></span>
                                        </a>
                                    </li>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                    
               

                <?php endforeach; ?>

                </div>
          
            </div>
        </section>
        <!-- End of section -->
    </main>
    <section class="section py-0">
    <div class="container z-2">
       <div class="row position-relative justify-content-center align-items-cente">
          <div class="col-12">
             <!-- Card -->
             <div class="card border-light px-4 py-1">
                <div class="card-body text-center text-md-left">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="h1 mb-3">Become one of us</h2>
                            <p class="lead mb-4">
                                Join us for free
                            </p>
                            <a href="<?=TEACHER_SIGNUP?>" class="btn btn-primary">
                                <span class="me-1">
                                    <span class="fas fa-users"></span>
                                </span>
                                Teacher Signup
                            </a>
                        </div>
                        <div class="col-12 col-md-6 mt-5 mt-md-0 text-md-right">
                            <img src="<?=BASEPATH;?>assets/images/logo.png" alt="">
                      
                        </div>
                    </div>
                </div>
            </div>
          </div>
       </div>
    </div>
</section>

    <footer class="footer pb-6 bg-primary text-white pt-9 mt-n9">
    <div class="container">
      
        <div class="row">
            <div class="col mb-md-0 mt-5">
            
            <div class="d-flex text-center justify-content-center align-items-center" role="contentinfo">
                <p class="font-weight-normal font-small mb-0">Copyright © MathApp3.com
                    <span class="current-year">2023</span>. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>

    <!-- Core -->
<script src="<?=THEME1;?>vendor/@popperjs/core/dist/umd/popper.min.js"></script>
<script src="<?=THEME1;?>vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?=THEME1;?>vendor/headroom.js/dist/headroom.min.js"></script>

<!-- Vendor JS -->
<script src="<?=THEME1;?>vendor/onscreen/dist/on-screen.umd.min.js"></script>
<script src="<?=THEME1;?>vendor/jarallax/dist/jarallax.min.js"></script>
<script src="<?=THEME1;?>vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
<script src="<?=THEME1;?>vendor/vivus/dist/vivus.min.js"></script>
<script src="<?=THEME1;?>vendor/vanillajs-datepicker/dist/js/datepicker.min.js"></script>

<script async defer src="https://buttons.github.io/buttons.js"></script>

<!-- Pixel JS -->
<script src="<?=THEME1;?>assets/js/pixel.js"></script>

</body>

</html>