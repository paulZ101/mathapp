

    <footer class="footer pt-4 pb-5 bg-primaryX text-whiteX">
    <div class="container">
     
      <hr class="bg-secondaryX my-3X my-lg-5X">
      <div class="row">
        <div class="col mb-md-0">
          <a href="#"  class="d-flex justify-content-center mb-3">
         
            <!-- <img class="navbar-brand-dark" src="<?=BASEPATH;?>assets/images/logo.png" alt="Logo dark" width="300">  -->
            <p class="text-whiteX fw-bold footer-logo-text m-0">MathApp3.com</p>
          </a>

          <div class="d-flex text-center justify-content-center align-items-center" role="contentinfo">
            <p class="fw-normal font-small mb-0">Copyright © MathApp3 <span class="current-year">2023</span>. All rights reserved. </p>
          </div>
        </div>

      </div>
    </div>
  </footer>
  <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/@popperjs/core/dist/umd/popper.min.js"></script>
  <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/headroom.js/dist/headroom.min.js"></script>
  <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/onscreen/dist/on-screen.umd.min.js"></script>
  <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/nouislider/distribute/nouislider.min.js"></script>
  <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/jarallax/dist/jarallax.min.js"></script>
  <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
  <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/vivus/dist/vivus.min.js"></script>
  <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/chartist/dist/chartist.min.js"></script>
  <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
  <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/@glidejs/glide/dist/glide.min.js"></script>
  <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/countup.js/dist/countUp.umd.js"></script>
  <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/vanillajs-datepicker/dist/js/datepicker.min.js"></script>
  <script async defer="defer" src="https://buttons.github.io/buttons.js"></script>
  <script src="https://demo.themesberg.com/pixel-pro/v5/assets/js/pixel.js"></script>
  <script defer src="https://static.cloudflareinsights.com/beacon.min.js/vaafb692b2aea4879b33c060e79fe94621666317369993" integrity="sha512-0ahDYl866UMhKuYcW078ScMalXqtFJggm7TmlUtp0UlD4eQk0Ixfnm5ykXKvGJNFjLMoortdseTfsRT8oCfgGA==" data-cf-beacon='{"rayId":"78a074d3bb4cb4fd","version":"2022.11.3","r":1,"token":"3a2c60bab7654724a0f7e5946db4ea5a","si":100}' crossorigin="anonymous"></script>
</body>
</html>