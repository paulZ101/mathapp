<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MathApp3</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
 
    <link rel="apple-touch-icon" sizes="120x120" href="<?=THEME2;?>assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=THEME2;?>assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=THEME2;?>assets/img/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <link type="text/css" href="https://demo.themesberg.com/pixel-pro/v5/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://demo.themesberg.com/pixel-pro/v5/vendor/@glidejs/glide/dist/css/glide.core.min.css">
    <link rel="stylesheet" href="https://demo.themesberg.com/pixel-pro/v5/vendor/@glidejs/glide/dist/css/glide.theme.min.css"> -->
    <link type="text/css" href="<?=THEME1;?>css/pixel.css" rel="stylesheet">


    <link type="text/css" href="<?= THEME1;?>vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <style>
 

    </style>
  </head>
  <body>
   
    <header class="header-global">



      <nav id="navbar-main" aria-label="Primary navigation" class="navbar navbar-main navbar-expand-lg navbar-theme-primary headroom navbar-light navbar-transparent navbar-theme-secondary" style="display:none;">
        <div class="container position-relative">
          <a class="navbar-brand me-lg-5" href="">
            <!-- <img class="navbar-brand-dark" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/brand/light.svg" alt="Logo light">
            <img class="navbar-brand-light" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/brand/dark.svg" alt="Logo dark"> -->

            <!-- <img class="navbar-brand-light" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/brand/dark.svg" alt="Logo dark"> -->
            

          </a>
          <div class="navbar-collapse collapse me-auto" id="navbar_global">
            <div class="navbar-collapse-header">
              <div class="row">
                <div class="col-6 collapse-brand">
                  <a href="">
                    <img src="" alt="Themesberg logo">

                    
                  </a>
                </div>
                <div class="col-6 collapse-close">
                  <a href="#navbar_global" class="fas fa-times" data-bs-toggle="collapse" data-bs-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" title="close" aria-label="Toggle navigation"></a>
                </div>
              </div>
            </div>
        
          </div>
          <div class="d-flex align-items-center" >
            <a href="<?=BASEPATH;?>game/game1/"  class="btn btn-outline-primary d-noneX d-lg-inline me-md-3 me-1">
      
              QUIZ GAME </a>
            <a href="step1.php"  class="btn btn-outline-primary">
      
              TAKE QUIZ</a> 
       
          </div>
        </div>
      </nav>
    </header>
    
    
    