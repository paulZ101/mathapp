
  <?php $language = language(); ?>

    <main>
      
      <div class="section section-lg pt-5 pt-md-7 border-gray-300X">
        <div class="container">
          <div class="row pt-5 pt-md-0">
            <div class="col-12 col-lg-4 mb-3 mb-lg-0">
              <div class="card border-gray-300X p-2">
                <div class="card-header bg-white border-0 text-center d-flex flex-row flex-lg-column align-items-center justify-content-center px-1 px-lg-4">
               
                  <div class="profile-thumbnail dashboard-avatar mx-lg-auto me-3">
                    <img src="<?= avatar($_SESSION['mathapp']['prelogin']['avatar']);?>" class="card-img-top rounded-circle border-white" alt="avatar">
                    <!-- <?=BASEPATH;?>assets/images/logo.png -->
                  
                  </div>


                  <span class="h5 my-0 my-lg-3 me-3 me-lg-0">Hi, <?= isset($_SESSION['mathapp']['login']['account_name']) ? $_SESSION['mathapp']['login']['account_name'] : 'Pupil';?></span>
                  <a href="<?= LOGOUT;?>?account=pupil" class="btn btn-gray-300 btn-xs">
                    <span class="me-2">
                      <span class="fas fa-sign-out-alt"></span>
                    </span><?= $lang[$language]['SIGN_OUT'];?> </a>
                </div>


                <div class="card-body p-2 d-none d-lg-block">
                  <div class="list-group dashboard-menu list-group-sm">
                    <a href="<?= BASEPATH;?>p/index.php" class="d-flex list-group-item border-0 list-group-item-action active">
                    <?= $lang[$language]['OVERVIEW'];?> <span class="icon icon-xs ms-auto">
                        <span class="fas fa-chevron-right"></span>
                      </span>
                    </a>
                    <a href="<?= BASEPATH;?>p/step1.php" class="d-flex list-group-item border-0 list-group-item-action">
                    <?= $lang[$language]['TAKE_QUIZ'];?>  <span class="icon icon-xs ms-auto">
                        <span class="fas fa-chevron-right"></span>
                      </span>
                    </a>
                    <a href="<?= BASEPATH;?>game/game1/"   class="d-flex list-group-item border-0 list-group-item-action">
                    <?= $lang[$language]['QUIZ_GAME'];?>  <span class="icon icon-xs ms-auto">
                        <span class="fas fa-chevron-right"></span>
                      </span>
                    </a>
                    <!-- <a href="" class="d-flex list-group-item border-0 list-group-item-action">Quiz Results <span class="icon icon-xs ms-auto">
                        <span class="fas fa-chevron-right"></span>
                      </span>
                    </a> -->
                    <!-- <a href="./billing.html" class="d-flex list-group-item border-0 list-group-item-action">Billing <span class="icon icon-xs ms-auto">
                        <span class="fas fa-chevron-right"></span>
                      </span>
                    </a>
                    <a href="./messages.html" class="d-flex list-group-item border-0 list-group-item-action border-0">Messages <span class="icon icon-xs ms-auto">
                        <span class="fas fa-chevron-right"></span>
                      </span>
                    </a> -->
                  </div>
                </div>
              </div>
            </div>

               

 

            


            <div class="col-12 d-lg-none">
              <div class="card bg-white mb-lg-5">
                <div class="card-body">
                  <div class="row align-items-center">
                    <div class="col-10 d-flex">
                      <a href="index.php" class="list-group-item list-group-item-action border-0 text-center me-2 active">Overview</a>
                      <a href="step1.php" class="list-group-item list-group-item-action border-0 text-center me-2">Take Quiz</a>
                      <a href="" class="list-group-item list-group-item-action border-0 text-center me-2 d-none d-sm-block border-0">Quiz Game</a>
                      <!-- <a href="" class="list-group-item list-group-item-action border-0 text-center d-none d-md-block border-0">Quiz Results</a> -->
                    </div>
                    <div class="col-2 d-flex justify-content-center">
                      <div class="btn-group dropleft">
                        <button class="btn btn-link dropdown-toggle dropdown-toggle-split me-2 m-0 p-0" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <span class="icon icon-sm">
                            <span class="fas fa-ellipsis-h icon-secondary fa-lg"></span>
                          </span>
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu">
                          <a href="<?= BASEPATH;?>p/index.php" class="list-group-item list-group-item-action d-sm-none border-0">Overview</a>
                          <a href="<?= BASEPATH;?>p/step1.php" class="list-group-item list-group-item-action d-md-none border-0">Take Quiz</a>
                          <a href="<?= BASEPATH;?>game/game1/" target="_blank"  class="list-group-item list-group-item-action border-0">Quiz Game</a>
                          <!-- <a href="" class="list-group-item list-group-item-action border-0">Quiz Results</a> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-8">
              <div class="row">
              
                <div class="col-12 col-sm-6 mb-4">

                  <a href="<?= BASEPATH;?>p/step1.php">
                  <div class="card border-gray-300">
                    <div class="card-body d-block d-md-flex align-items-center">
                      <div class="icon icon-shape icon-md icon-shape-primary rounded-circle me-3 mb-4 mb-md-0">
                        <!-- <span class="fas fa-regular fa-pen-field"></span> -->
                        <span class="fas fa-check"></span>
                      </div>
                      <div>

                        <!-- <span class="d-block h6 fw-normal">Take Quiz</span> -->
                        <h5 class="h3 fw-bold mb-1"><?= $lang[$language]['TAKE_QUIZ'];?></h5>
                        
                      </div>
                    </div>
                  </div>
                  </a>


                  </div>

                  <div class="col-12 col-sm-6 mb-4">

                    <a href="<?= BASEPATH;?>game/game1/"  >
                    <div class="card border-gray-300">
                      <div class="card-body d-block d-md-flex align-items-center">
                        <div class="icon icon-shape icon-md icon-shape-primary rounded-circle me-3 mb-4 mb-md-0">
                          <!-- <span class="fas fa-wallet"></span> -->
                          <span class="	far fa-grin"></span>
                        </div>
                        <div>

                          <!-- <span class="d-block h6 fw-normal">Quiz Results</span> -->
                          <h5 class="h3 fw-bold mb-1"><?= $lang[$language]['QUIZ_GAME'];?></h5>
                          
                        </div>
                      </div>
                    </div>
                    </a>





                  </div>
                



                  <?php if(isset($numeracyResult1) AND $numeracyResult1 == true){  
                  
                  ?>
                  <div class="col-12 col-sm-6 mb-4">

                    <a href="<?= BASEPATH;?>p/numeracy_test.php?n=1" >
                    <div class="card border-gray-300">
                      <div class="card-body d-block d-md-flex align-items-center">
                        <div class="icon icon-shape icon-md icon-shape-primary rounded-circle me-3 mb-4 mb-md-0">
                          <!-- <span class="fas fa-wallet"></span> -->
                          <span class="fas fa-file-alt"></span>
                        </div>
                        <div>

                          <!-- <span class="d-block h6 fw-normal">Quiz Results</span> -->
                          <h5 class="h3 fw-bold mb-1">Numeracy Test</h5>
                          
                        </div>
                      </div>
                    </div>
                    </a> 

                  </div>
                
                  <?php } ?>

                  <?php if(isset($numeracyResult2) AND $numeracyResult2 == true){  
                  
                  ?>
                  <div class="col-12 col-sm-6 mb-4">

                    <a href="<?= BASEPATH;?>p/numeracy_test.php?n=1" >
                    <div class="card border-gray-300">
                      <div class="card-body d-block d-md-flex align-items-center">
                        <div class="icon icon-shape icon-md icon-shape-primary rounded-circle me-3 mb-4 mb-md-0">
                          <!-- <span class="fas fa-wallet"></span> -->
                          <span class="fas fa-file-alt"></span>
                        </div>
                        <div>

                          <!-- <span class="d-block h6 fw-normal">Quiz Results</span> -->
                          <h5 class="h3 fw-bold mb-1">Numeracy Test</h5>
                          
                        </div>
                      </div>
                    </div>
                    </a> 

                  </div>
                
                  <?php } ?>
               


              </div>
            </div>
          </div>
        </div>
      </div>
    </main>


    <style>
    @import url("https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap");

* {
  margin: 0;
  padding: 0;
}
 
 
 
body {
  /* background: #ecf2fe; */
  /* height: 100vh; */

  background-image: url(<?=THEME1;?>assets/img/pages/bg1.jpg);
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center;
  background-size:     cover;      

}
    

.plans {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
  -ms-flex-pack: justify;
  justify-content: space-between;

  max-width: 970px;
  padding: 85px 50px;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  /* background: #fff; */
  background: transparent;
  
  border-radius: 20px;
  /* -webkit-box-shadow: 0px 8px 10px 0px #d8dfeb; */
  /* box-shadow: 0px 8px 10px 0px #d8dfeb; */
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
}

.plans .plan input[type="radio"] {
  position: absolute;
  opacity: 0;
}

.plans .plan {
  cursor: pointer;
  width: 48.5%;
}

.plans .plan .plan-content {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  padding: 30px;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  border: 2px solid #e1e2e7;
  border-radius: 10px;
  -webkit-transition: -webkit-box-shadow 0.4s;
  transition: -webkit-box-shadow 0.4s;
  -o-transition: box-shadow 0.4s;
  transition: box-shadow 0.4s;
  transition: box-shadow 0.4s, -webkit-box-shadow 0.4s;
  position: relative;
}

.plans .plan .plan-content img {
  margin-right: 30px;
  height: 72px;
}

.plans .plan .plan-details span {
  margin-bottom: 10px;
  display: block;
  font-size: 20px;
  line-height: 24px;
  color: #252f42;
}

.container .title {
  font-size: 16px;
  font-weight: 500;
  -ms-flex-preferred-size: 100%;
  flex-basis: 100%;
  color: #252f42;
  margin-bottom: 20px;
}

.plans .plan .plan-details p {
  color: #646a79;
  font-size: 14px;
  line-height: 18px;
}

.plans .plan .plan-content:hover {
  -webkit-box-shadow: 0px 3px 5px 0px #e8e8e8;
  box-shadow: 0px 3px 5px 0px #e8e8e8;
}

.plans .plan input[type="radio"]:checked + .plan-content:after {
  content: "";
  position: absolute;
  height: 8px;
  width: 8px;
  background: #216fe0;
  right: 20px;
  top: 20px;
  border-radius: 100%;
  border: 3px solid #fff;
  -webkit-box-shadow: 0px 0px 0px 2px #0066ff;
  box-shadow: 0px 0px 0px 2px #0066ff;
}

.plans .plan input[type="radio"]:checked + .plan-content {
  border: 2px solid #216ee0;
  background: #eaf1fe;
  -webkit-transition: ease-in 0.3s;
  -o-transition: ease-in 0.3s;
  transition: ease-in 0.3s;
}

@media screen and (max-width: 991px) {
  .plans {
    margin: 0 20px;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: flex-start;
    padding: 40px;
  }

  .plans .plan {
    width: 100%;
  }

  .plan.complete-plan {
    margin-top: 20px;
  }

  .plans .plan .plan-content .plan-details {
    width: 70%;
    display: inline-block;
  }

  .plans .plan input[type="radio"]:checked + .plan-content:after {
    top: 45%;
    -webkit-transform: translate(-50%);
    -ms-transform: translate(-50%);
    transform: translate(-50%);
  }
}

@media screen and (max-width: 767px) {
  .plans .plan .plan-content .plan-details {
    width: 60%;
    display: inline-block;
  }
}

@media screen and (max-width: 540px) {
  .plans .plan .plan-content img {
    margin-bottom: 20px;
    height: 56px;
    -webkit-transition: height 0.4s;
    -o-transition: height 0.4s;
    transition: height 0.4s;
  }

  .plans .plan input[type="radio"]:checked + .plan-content:after {
    top: 20px;
    right: 10px;
  }

  .plans .plan .plan-content .plan-details {
    width: 100%;
  }

  .plans .plan .plan-content {
    padding: 20px;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: baseline;
    -ms-flex-align: baseline;
    align-items: baseline;
  }
}

/* inspiration */
.inspiration {
  font-size: 12px;
  margin-top: 50px;
  position: absolute;
  bottom: 10px;
  font-weight: 300;
}

.inspiration a {
  color: #666;
}
@media screen and (max-width: 767px) {
  /* inspiration */
  .inspiration {
    display: none;
  }
}

.mb-3{
    margin-bottom:30px;
}

    </style>