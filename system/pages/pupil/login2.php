<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Primary Meta Tags -->
<title>MathApp3.com</title>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="title" content="MathApp3">
 
 

<!-- Favicon -->
<link rel="apple-touch-icon" sizes="120x120" href="<?=THEME1;?>assets/img/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?=THEME1;?>assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?=THEME1;?>assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="<?=THEME1;?>assets/img/favicon/site.webmanifest">
<link rel="mask-icon" href="<?=THEME1;?>assets/img/favicon/safari-pinned-tab.svg" color="#ffffff">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">

<!-- Fontawesome -->
<link type="text/css" href="<?=THEME1;?>vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">

<!-- Pixel CSS -->
<link type="text/css" href="<?=THEME1;?>css/pixel.css" rel="stylesheet">

<style>
body {
  /* background-image: url(<?=THEME1;?>assets/img/pages/form-image3.jpeg);
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center;
  background-size:     cover;        */
}
</style>

</head>

<body>
    <main>
    <!-- data-background="<?=THEME1;?>assets/img/pages/form-image.jpg" -->
        <section class="min-vh-100 d-flex align-items-center section-image overlay-soft-dark" >
        
        <div class="container z-2XX mt-n9X mt-lg-n10X align-items-center ">

          <div class="row">
            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
              <a href="./support-topic.html" class="card shadow text-center border-gray-300 animate-up-3">
                <div class="card-body p-5">
                  <div class="icon icon-shape icon-shape-secondary rounded-circle mb-4">
                    <span class="fas fa-clipboard-list"></span>
                  </div>
                  <h6 class="text-gray mb-0">Requirements</h6>
                </div>
              </a>
            </div>
            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
              <a href="./support-topic.html" class="card shadow text-center border-gray-300 animate-up-3">
                <div class="card-body p-5">
                  <div class="icon icon-shape icon-shape-secondary rounded-circle mb-4">
                    <span class="fas fa-receipt"></span>
                  </div>
                  <h6 class="text-gray mb-0">Licensing</h6>
                </div>
              </a>


      
            </div>
             
            
          </div>
        </div>
    </section>


    </main>

    <!-- Core -->
<script src="<?=THEME1;?>vendor/@popperjs/core/dist/umd/popper.min.js"></script>
<script src="<?=THEME1;?>vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?=THEME1;?>vendor/headroom.js/dist/headroom.min.js"></script>

<!-- Vendor JS -->
<script src="<?=THEME1;?>vendor/onscreen/dist/on-screen.umd.min.js"></script>
<script src="<?=THEME1;?>vendor/jarallax/dist/jarallax.min.js"></script>
<script src="<?=THEME1;?>vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
<script src="<?=THEME1;?>vendor/vivus/dist/vivus.min.js"></script>
<script src="<?=THEME1;?>vendor/vanillajs-datepicker/dist/js/datepicker.min.js"></script>

<script async defer src="https://buttons.github.io/buttons.js"></script>

<!-- Pixel JS -->
<script src="<?=THEME1;?>assets/js/pixel.js"></script>

</body>

</html>