
<style>
@keyframes animatedBackground {
  from {
    background-position: 0 0;
  }
  to {
    background-position: 100% 0;
  }
}
#animate-area {
  /* width: 200px;
  height: 200px; */
  background-image: url(https://mathapp/assets/imageslessonbg.jpg);
  /* background-position: 0px 0px;
  background-repeat: repeat-x; */
  animation: animatedBackground 10s linear infinite alternate;
}
 
</style>


    <main>
     
      <section class="section section-header pb-6 bg-primary text-white overflow-hidden z-2 " id="animate-area">
        <div class="container z-2">
          <div class="row justify-content-center mb-2">
            <div class="col-lg-8 col-xl-8">
              <div class="text-center">
                <h1 class="display-2 mb-3">Welcome to MathApp3.com</h1>
                <!-- <p class="lead text-muted">Enter your section code number provided by your teacher -->
                </p>
              </div>
              <form class="row d-flex align-items-top justify-content-center mt-5" action="<?= SUBMIT;?>" method="post">
              <?php resultMessage(); ?>
                <div class="col-12 col-xl-10">
                  <div class="form-group">
                    <label class="h6" for="email">Section Code Number</label>
                    <div class="d-flex flex-row justify-content-center">
                      <input class="form-control form-control-lg border-0 rounded" id="code" name="code" placeholder="Enter your section code" type="number" required>
                      <button type="submit" class="btn btn-tertiary btn-lg ms-2">GO</button>
                    </div>
                  </div>
                  <p class="small text-left text-muted mt-2">Enter your section code number provided by your teacher</p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>


      <div class="section section-md">
        <div class="container">
          <!-- <ul class="nav justify-content-center mb-4">
            <li class="nav-item">
              <a class="nav-link text-instagram h3 px-4" aria-label="instagram social link" href="#">
                <span class="fab fa-instagram"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-facebook h3 px-4" aria-label="facebook social link" href="#">
                <span class="fab fa-facebook"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-github h3 px-4" aria-label="github social link" href="#">
                <span class="fab fa-github"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-dribbble h3 px-4" aria-label="dribbble social link" href="#">
                <span class="fab fa-dribbble"></span>
              </a>
            </li>
          </ul> -->
          <div class="row justify-content-center align-items-center">
            <div class="col-lg-8 text-center">

              <img class="navbar-brand-dark" src="<?=BASEPATH;?>assets/images/logo.png" alt="Logo dark" width="300"> 

              <p class="h3 fw-normal lh-160">MathApp3 is a unique and awesome math application for Grade 3 pupils .  </p>
              <!-- <div class="mt-5">
                <a href="https://themesberg.com/product/ui-kit/pixel-pro-premium-bootstrap-5-ui-kit" class="btn btn-secondary animate-up-1">Purchase now</a>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </main>


    