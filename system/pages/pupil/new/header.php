<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MathApp3</title>
 
   

    <link rel="apple-touch-icon" sizes="120x120" href="<?=THEME2;?>assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=THEME2;?>assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=THEME2;?>assets/img/favicon/favicon-16x16.png">


    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link type="text/css" href="https://demo.themesberg.com/pixel-pro/v5/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://demo.themesberg.com/pixel-pro/v5/vendor/@glidejs/glide/dist/css/glide.core.min.css">
    <link rel="stylesheet" href="https://demo.themesberg.com/pixel-pro/v5/vendor/@glidejs/glide/dist/css/glide.theme.min.css"> -->
    <link type="text/css" href="<?= THEME1;?>/css/pixel.css" rel="stylesheet">
 
    <link type="text/css" href="<?=THEME1;?>vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
     
  </head>
  <body>
  
 