<main>
<audio id="myAudio" autoplay controls hidden>
  <source src="<?= BASEPATH;?>assets/audio/quiz_result.mp3" type="audio/mp3">
</audio>     
      

<style>
.bg-primary{
  background-image: url(<?=THEME1;?>assets/img/pages/quiz.jpg);
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: top;
  background-size:     cover;   
} 
</style>
      
      <section class="section section-header pb-10 bg-primary text-whiteX">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-8 col-xl-8 text-center">
              <h1 class="display-2 fw-light mb-4">Numeracy <span class="fw-bold">Score</span>
              </h1>
              <p class="lead"></p>
            </div>
          </div>
        </div>
      </section>
      <div class="section section-lg pt-0">
        <div class="container">
          <div class="row mt-n9">
            <div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0">
               
            </div>


            <div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0">
              <div class="card shadow border-gray-300 text-center py-4">
                <div class="card-header border-0 bg-white p-3">
                  <h2 class="h3 text-primary mb-4">Score</h2>
                  <span class="d-block">
                    <span class="display-2 text-tertiary fw-bold">
                        <?= $results['right_answer'] ?> / <?=($results['right_answer']+$results['wrong_answer']+$results['unanswered']);?> </span>
                    <!-- <span class="d-block text-gray font-small">/ <?=($results['right_answer']+$results['wrong_answer']+$results['unanswered']);?></span> -->
                  </span>
                </div>

 

                <div class="card-body">
                  <ul class="list-unstyled mb-4">
                   
                  <?php if(isset($lesson->lesson_title)): ?>
                    <li class="list-item pb-3">
                        <strong><h4><?= $lesson->lesson_title;?></h4></strong>
                    </li>
                    <?php endif; ?>

                    <li class="list-item pb-3">
                      You got
                    </li>
                    <li class="list-item pb-3">
                      <strong><?= $results['right_answer'];?></strong> Right Answer
                    </li>
                    <li class="list-item pb-3">
                      <strong><?= $results['wrong_answer'];?></strong> Wrong Answer
                    </li>
                  

                    
                  </ul>
                  <div class="d-grid">
                    <a href="index.php" class="btn btn-tertiary">Back</a>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col-12 col-md-6 col-lg-4">
            </div>
          </div>
        </div>
      </div>


     
   
    </main>