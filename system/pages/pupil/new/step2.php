<main>
    
    <section class="section-header pb-9 pb-lg-10 text-black">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-12 col-md-10 text-center">
            <h1 class="mb-3">Select Module </h1>
            <p class="">Select Module</p>
             
          </div>
        </div>
      </div>
      <div class="pattern bottom"></div>
    </section>


 


    <section class="section section-md pb-0">
      <div class="container z-2 mt-n9 mt-lg-n10">
        <div class="row justify-content-center">
          
        <?php
      
        foreach($dataArray["body"] as $data){
            $mid = encrypt_decrypt('encrypt', $data['reg_id']);
            ?>
            <div class="col-lg-10 mb-3">
                <a href="step3.php?mid=<?= $mid;?>" class="card border-gray-300 animate-up-3 p-0 p-lg-4">
                <div class="card-body">
                    <h2><?=$data['module_title'];?></h2>
                    <!-- <p class="lead text-gray mb-4">How to edit account settings such as email addresss, user name, language and switch to dark mode</p> -->
                    
                </div>
                </a>
            </div>
            <?php }   ?>

  
        
        </div>
      </div>
    </section>
    <!-- <section class="section section-md bg-gray-200">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col">
            <div class="text-center">
              <h3 class="mb-4">Can't find what you are looking for? Let us know!</h3>
              <a class="text-secondary fw-normal h4" href="step1.php">
              
                <i class="fas fa-arrow-left"></i> Select Category <span class="icon icon-sm icon-primary ms-1">
                  
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section> -->
  </main>

  <style>


@import url("https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap");

* {
  margin: 0;
  padding: 0;
}
        body {
 
  background-image: url(<?=THEME1;?>assets/img/pages/bg11.jpg);
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center;
  background-size:     cover;      


}

.main-ui {
  max-width: 800px;
  margin: 0 auto;
}

.our-form {
  display: flex;
  justify-content: center;
}

.status {
  text-align: center;
  font-size: .85rem;
}

.boxes {
  display: flex;
  width: 100%;
}

.progress {
  border: 1px solid #c7c7c7;
  border-right: none;
  position: relative;
}

.progress-inner {
  position: absolute;
  top: 0;
  bottom: 0;
  width: 100%;
  background-color: #7ecc00;
  opacity: .57;
  transform: scaleX(0);
  transform-origin: center left;
  transition: transform .4s ease-out;
}

.box {
  height: 40px;
  border-right: 1px solid #c7c7c7;
  flex: 1;
}

.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: rgba(255, 255, 255, .82);
  display: flex;
  justify-content: center;
  align-items: center;
  opacity: 0;
  visibility: hidden;
  transition: all .33s ease-out;
  transform: scale(1.2);
}

.overlay-inner {
  text-align: center;
  max-width: 700px;
}

body.overlay-is-open .overlay,
.overlay--visible {
  opacity: 1;
  visibility: visible;
  transform: scale(1);
}

body.overlay-is-open .main-ui,
.blurred {
  filter: blur(4px);
}

@keyframes showError {
  50% {
    color: red;
    transform: scale(1.2);
  }

  100% {
    color: #333;
    transform: scale(1);
  }
}

.animate-wrong {
  animation: .45s showError;
}

.problem {
  font-size: 5rem;
  margin: 0;
  text-align: center;
}

.end-message {
  font-size: 1.5rem;
  margin-top: 0;
}

.reset-button {
  font-size: 1.2rem;
  background-color: #004094;
  border: none;
  color: #FFF;
  border-radius: 7px;
  padding: 12px 20px;
  display: inline-block;
  outline: none;
  cursor: pointer;
}

.reset-button:hover {
  background-color: #00367e;
}

  
  