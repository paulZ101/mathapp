<html>

<head>
<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
 

<script src="<?=BASEPATH;?>assets/js/jquery.min.js"></script>
<link href="<?=BASEPATH;?>assets/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>


<style>
    @import url("https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap");

* {
  margin: 0;
  padding: 0;
}

body {
  /* background: #ecf2fe; */
  /* height: 100vh; */

  background-image: url(<?=THEME1;?>assets/img/pages/quiz0.jpg);
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center;
  background-size:     cover;  



  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -ms-flex-direction: column;
  flex-direction: column;
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  justify-content: center;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  font-family: "Roboto", sans-serif;
}

.plans {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
  -ms-flex-pack: justify;
  justify-content: space-between;

  max-width: 970px;
  padding: 85px 50px;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  background: #fff;
  border-radius: 20px;
  /* -webkit-box-shadow: 0px 8px 10px 0px #d8dfeb;
  box-shadow: 0px 8px 10px 0px #d8dfeb; */
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
}

.plans .plan input[type="radio"] {
  position: absolute;
  opacity: 0;
}

.plans .plan {
  cursor: pointer;
  width: 48.5%;
}

.plans .plan .plan-content {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  padding: 30px;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  border: 2px solid #e1e2e7;
  border-radius: 10px;
  -webkit-transition: -webkit-box-shadow 0.4s;
  transition: -webkit-box-shadow 0.4s;
  -o-transition: box-shadow 0.4s;
  transition: box-shadow 0.4s;
  transition: box-shadow 0.4s, -webkit-box-shadow 0.4s;
  position: relative;
}

.plans .plan .plan-content img {
  margin-right: 30px;
  height: 72px;
}

.plans .plan .plan-details span {
  margin-bottom: 10px;
  display: block;
  font-size: 20px;
  line-height: 24px;
  color: #252f42;
}

.container .title {
  font-size: 16px;
  font-weight: 500;
  -ms-flex-preferred-size: 100%;
  flex-basis: 100%;
  color: #252f42;
  margin-bottom: 20px;
}

.plans .plan .plan-details p {
  color: #646a79;
  font-size: 14px;
  line-height: 18px;
}

.plans .plan .plan-content:hover {
  -webkit-box-shadow: 0px 3px 5px 0px #e8e8e8;
  box-shadow: 0px 3px 5px 0px #e8e8e8;
}

.plans .plan input[type="radio"]:checked + .plan-content:after {
  content: "";
  position: absolute;
  height: 8px;
  width: 8px;
  background: #216fe0;
  right: 20px;
  top: 20px;
  border-radius: 100%;
  border: 3px solid #fff;
  -webkit-box-shadow: 0px 0px 0px 2px #0066ff;
  box-shadow: 0px 0px 0px 2px #0066ff;
}

.plans .plan input[type="radio"]:checked + .plan-content {
  border: 2px solid #216ee0;
  background: #eaf1fe;
  -webkit-transition: ease-in 0.3s;
  -o-transition: ease-in 0.3s;
  transition: ease-in 0.3s;
}

@media screen and (max-width: 991px) {
  .plans {
    margin: 0 20px;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: flex-start;
    padding: 40px;
  }

  .plans .plan {
    width: 100%;
  }

  .plan.complete-plan {
    margin-top: 20px;
  }

  .plans .plan .plan-content .plan-details {
    width: 70%;
    display: inline-block;
  }

  .plans .plan input[type="radio"]:checked + .plan-content:after {
    top: 45%;
    -webkit-transform: translate(-50%);
    -ms-transform: translate(-50%);
    transform: translate(-50%);
  }
}

@media screen and (max-width: 767px) {
  .plans .plan .plan-content .plan-details {
    width: 60%;
    display: inline-block;
  }
}

@media screen and (max-width: 540px) {
  .plans .plan .plan-content img {
    margin-bottom: 20px;
    height: 56px;
    -webkit-transition: height 0.4s;
    -o-transition: height 0.4s;
    transition: height 0.4s;
  }

  .plans .plan input[type="radio"]:checked + .plan-content:after {
    top: 20px;
    right: 10px;
  }
h1
  .plans .plan .plan-content .plan-details {
    width: 100%;
  }

  .plans .plan .plan-content {
    padding: 20px;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: baseline;
    -ms-flex-align: baseline;
    align-items: baseline;
  }
}

/* inspiration */
.inspiration {
  font-size: 12px;
  margin-top: 50px;
  position: absolute;
  bottom: 10px;
  font-weight: 300;
}

.inspiration a {
  color: #666;
}
@media screen and (max-width: 767px) {
  /* inspiration */
  .inspiration {
    display: none;
  }
}

.mb-3{
    margin-bottom:30px;
}

    </style>


 
<audio id="myAudio" autoplay controls hidden>
        <source src="<?= BASEPATH;?>assets/audio/audio_quiz.mp3" type="audio/mp3">
    </audio>


<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 mt-5">
     	<div class="container">
     		<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
					<h1 class="text-center text_underline"> Timer : <span id='timer'></span> </h1>
	     			<form class="form-horizontal" role="form" id='quiz_form' method="post" action="numeracy-result.php" onkeydown="return event.key != 'Enter';">
                     <?php
						
						$remainder = $results['remainder'];
						$number_question =  $results['number_question'];
						$rowcount =  $results['rowcount'];
						$i = 0;
						$j = 1; $k = 1;



                  
						?>
						<?php foreach ($results['questions'] as $result) {

                            // echo '<pre>';
                            // print_r($result);
                            // echo '</pre>';

							if ( $i == 0) echo "<div class='cont' id='question_splitter_$j'>";?>
					 


                                <div class="plans" id='question<?php echo $k;?>'>
                                <!-- <?php echo $k?>.  -->
                                    <div class="title" id="qname<?php echo $j;?>">
                                    <center>
                                        <h1><?php echo $result['number1'];?> <?php echo operatorSign($result['operator']);?> <?php echo $result['number2'];?> </h1>

                  <div>

                    <input   placeholder="Input your asnwer" name="<?php echo $result['reg_id'];?>" class="form-control form-control-lg" id="formFileLg" type="number" nput your answer>
                  </div>
 
            </center>
                                  </div> 	
                                      

                                </div>
                           
                                <!-- <input type="radio" checked='checked' style='display:none' value="smart_quiz" id='radio1_<?php echo $result['reg_id'];?>' name='<?php echo $result['reg_id'];?>'/>                                                                       -->
               






<br>
                          <center>  
          

                         
							<?php
                               
								 $i++; 
								 if ( ( $remainder < 1 ) || ( $i == $number_question && $remainder == 1 ) ) {
								 	echo "<button id='".$j."' class='next btn btn-success btn-lg px-4' type='submit'>Finish</button>";
								 	echo "</div>";
								 }  elseif ( $rowcount > $number_question  ) {
								 	if ( $j == 1 && $i == $number_question ) {
										echo "<button id='".$j."' class='next btn btn-success btn-lg px-4' type='button'>Next</button>";
										echo "</div>";
										$i = 0;
										$j++;           
									} elseif ( $k == $rowcount ) { 
										echo " <button id='".$j."' class='previous btn btn-success btn-lg px-4' type='button'>Previous</button>
													<button id='".$j."' class='next btn btn-success btn-lg px-4' type='submit'>Finish</button>";
										echo "</div>";
										$i = 0;
										$j++;
									} elseif ( $j > 1 && $i == $number_question ) {
										echo "<button id='".$j."' class='previous btn btn-success btn-lg px-4' type='button'>Previous</button>
								                    <button id='".$j."' class='next btn btn-success btn-lg px-4' type='button' >Next</button>";
										echo "</div>";
										$i = 0;
										$j++;
									}
									
								 }
								  $k++;
						} 
                        
                        ?>	

          </center>  
      

						</form>
	     		</div>
	     		
			</div>
		</div>	
</main>

<script>

    $('.cont').hide();
		$('#question_splitter_1').show();
		$(document).on('click','.next',function(){


		    last=parseInt($(this).attr('id'));  
        
		    nex=last+1;
		    $('#question_splitter_'+last).hide();
		    
		    $('#question_splitter_'+nex).show();
		});
		
		$(document).on('click','.previous',function(){
		    last=parseInt($(this).attr('id'));     
		    pre=last-1;
		    $('#question_splitter_'+last).hide();
		    
		    $('#question_splitter_'+pre).show();
		});


		
        var c = 180; // 3minutes
        var t;
        timedCount();

        function timedCount() {

        	var hours = parseInt( c / 3600 ) % 24;
        	var minutes = parseInt( c / 60 ) % 60;
        	var seconds = c % 60;

        	var result = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);

            
        	$('#timer').html(result);
            if(c == 0 ){
            	setConfirmUnload(false);
                $("#quiz_form").submit();
            }
            c = c - 1;
            t = setTimeout(function(){ timedCount() }, 1000);
        }
	</script>

<script type="text/javascript">
    // Prevent accidental navigation away
    setConfirmUnload(true);
    function setConfirmUnload(on)
    {
        window.onbeforeunload = on ? unloadMessage : null;
    }
    function unloadMessage()
    {
        return 'Your Answered Questions are resetted zero, Please select stay on page to continue your Quiz';
    }

    $(document).on('click', 'button:submit',function(){
        setConfirmUnload(false);
    });
</script>


</body>
</html>