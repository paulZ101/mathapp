<html>

<head>
<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">

<!-- Favicon -->
<link rel="apple-touch-icon" sizes="120x120" href="<?=THEME2;?>assets/img/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?=THEME2;?>assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?=THEME2;?>assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="<?=THEME2;?>assets/img/favicon/site.webmanifest">
<link rel="mask-icon" href="<?=THEME2;?>assets/img/favicon/safari-pinned-tab.svg" color="#ffffff">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">
 

<script src="<?=BASEPATH;?>assets/js/jquery.min.js"></script>
<link href="<?=BASEPATH;?>assets/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
 

<style>
    @import url("https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap");

* {
  margin: 0;
  padding: 0;
}
 
.cont{
  /* background: #222; */
}
 
body {
  /* background: #ecf2fe; */
  /* height: 100vh; */

  font-family: 'Luckiest Guy', cursive;

  background-image: url(<?=THEME1;?>assets/img/pages/quiz1.jpg);
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center;
  background-size:     cover;     
   

}

.plans {


  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
  -ms-flex-pack: justify;
  justify-content: space-between;

  /* max-width: 970px; */
  padding: 85px 50px;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  /* background: #fff; */
  background: transparent;
  
  border-radius: 20px;
  /* -webkit-box-shadow: 0px 8px 10px 0px #d8dfeb; */
  /* box-shadow: 0px 8px 10px 0px #d8dfeb; */
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
}

.plans .plan input[type="radio"] {
  position: absolute;
  opacity: 0;
  
}

.plans .plan {
  cursor: pointer;
  width: 48.5%;
 
}

.plans .plan .plan-content {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  padding: 30px;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  border: 2px solid #e1e2e7;
  border-radius: 10px;
  -webkit-transition: -webkit-box-shadow 0.4s;
  transition: -webkit-box-shadow 0.4s;
  -o-transition: box-shadow 0.4s;
  transition: box-shadow 0.4s;
  transition: box-shadow 0.4s, -webkit-box-shadow 0.4s;
  position: relative;

  background: #fff;
  
}

.plans .plan .plan-content img {
  margin-right: 30px;
  height: 72px;
}

.plans .plan .plan-details span {
  margin-bottom: 10px;
  display: block;
  font-size: 20px;
  line-height: 24px;
  color: #252f42;
}

.container .title {
  font-size: 16px;
  font-weight: 500;
  -ms-flex-preferred-size: 100%;
  flex-basis: 100%;
  color: #252f42;
  margin-bottom: 20px;
}

.plans .plan .plan-details p {
  color: #646a79;
  font-size: 14px;
  line-height: 18px;
  
}

.plans .plan .plan-content:hover {
  -webkit-box-shadow: 0px 3px 5px 0px #e8e8e8;
  box-shadow: 0px 3px 5px 0px #e8e8e8;
}

.plans .plan input[type="radio"]:checked + .plan-content:after {
  content: "";
  position: absolute;
  height: 8px;
  width: 8px;
  background: #216fe0;
  right: 20px;
  top: 20px;
  border-radius: 100%;
  border: 3px solid #fff;
  -webkit-box-shadow: 0px 0px 0px 2px #0066ff;
  box-shadow: 0px 0px 0px 2px #0066ff;
}

.plans .plan input[type="radio"]:checked + .plan-content {
  border: 2px solid #216ee0;
  background: #eaf1fe;
  -webkit-transition: ease-in 0.3s;
  -o-transition: ease-in 0.3s;
  transition: ease-in 0.3s;
}

@media screen and (max-width: 991px) {
  .plans {
    margin: 0 20px;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: flex-start;
    padding: 40px;
  }

  .plans .plan {
    width: 100%;
  }

  .plan.complete-plan {
    margin-top: 20px;
  }

  .plans .plan .plan-content .plan-details {
    width: 70%;
    display: inline-block;
  }

  .plans .plan input[type="radio"]:checked + .plan-content:after {
    top: 45%;
    -webkit-transform: translate(-50%);
    -ms-transform: translate(-50%);
    transform: translate(-50%);
  }
}

@media screen and (max-width: 767px) {
  .plans .plan .plan-content .plan-details {
    width: 60%;
    display: inline-block;
  }
}

@media screen and (max-width: 540px) {
  .plans .plan .plan-content img {
    margin-bottom: 20px;
    height: 56px;
    -webkit-transition: height 0.4s;
    -o-transition: height 0.4s;
    transition: height 0.4s;
  }

  .plans .plan input[type="radio"]:checked + .plan-content:after {
    top: 20px;
    right: 10px;
  }

  .plans .plan .plan-content .plan-details {
    width: 100%;
  }

  .plans .plan .plan-content {
    padding: 20px;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: baseline;
    -ms-flex-align: baseline;
    align-items: baseline;
  }
}

/* inspiration */
.inspiration {
  font-size: 12px;
  margin-top: 50px;
  position: absolute;
  bottom: 10px;
  font-weight: 300;
}

.inspiration a {
  color: #666;
}
@media screen and (max-width: 767px) {
  /* inspiration */
  .inspiration {
    display: none;
  }
}

.mb-3{
    margin-bottom:30px;
}

    </style>

<!-- hidden -->
 
<!-- <audio id="myAudio" controls autoplay > -->
<!-- <audio controls autoplay>

 <source src="<?= BASEPATH;?>assets/audio/audio_quiz.mp3" type="audio/mpeg">
 Your browser does not support the audio element.
</audio> -->









 
    <!-- <iframe src="silence.mp3" allow="autoplay" id="audio" style="display: none;"></iframe> -->
    <audio id="myAudio" autoplay controls hidden>
        <source src="<?= BASEPATH;?>assets/audio/audio_quiz.mp3" type="audio/mp3">
    </audio>







    <!-- class="col-md-8 ms-sm-auto col-lg-10 px-md-4 mt-5" -->

<main >



     	<div class="container mt-5">
     		<div class="row"  >

         <!-- col-xs-12 col-sm-8 col-md-8 col-lg-8 mt-5 -->
				<div class="" style="background:rgba(255, 255, 255, 0.8);">



					<h1 class="text-center text_underline mt-5"> Timer : <span id='timer'></span> </h1>
	     			<form class="form-horizontal" role="form" id='quiz_form' method="post" action="quiz-result.php" > 
                     <?php
						
						$remainder = $results['remainder'];
						$number_question =  $results['number_question'];
						$rowcount =  $results['rowcount'];
						$i = 0;
						$j = 1; $k = 1;



                  
						?>
						<?php foreach ($results['questions'] as $result) {

                            // echo '<pre>';
                            // print_r($result);
                            // echo '</pre>';

							if ( $i == 0) echo "<div class='cont'   id='question_splitter_$j'>";?>
						 


                                <div class="plans" id='question<?php echo $k;?>'>
                                    
                                    <div class="title " id="qname<?php echo $j;?>">
                                      <h3><?php echo $k?>. <?php echo $result['question'];?></h3>

                                      <?php if(file_exists(DOCUMENT_ROOT . 'uploads/' . $result['imagefile'])) {?>
                                        <img  src="<?= BASEPATH; ?>uploads/<?= $result['imagefile']; ?>" alt="" class="img-fluid"/>
                                        <br><br>
                                      <?php } ?>
                                  
                                  </div>


                                   

                                    <label class="plan radio1_<?php echo $result['reg_id'];?>-plan mb-3" for="radio1_<?php echo $result['reg_id'];?>">

                                    <!-- <img src="<?=BASEPATH;?>assets/images/MathTime.png" alt="MathTime"> -->

                                      <input type="radio" id="radio1_<?php echo $result['reg_id'];?>" name="<?php echo $result['reg_id'];?>" value="1" />
                                      <div class="plan-content">
                                          <!-- <img loading="lazy" src="https://raw.githubusercontent.com/ismailvtl/ismailvtl.github.io/master/images/potted-plant-img.svg" alt="" /> -->
                                          <div class="plan-details">
                                          <!-- <span>Option 1.</span> -->
                                          <h4><?php echo $result['option1'];?></h4>
                                          </div>
                                      </div>
                                    </label>

 
                                    <label class="plan radio2_<?php echo $result['reg_id'];?>-plan mb-3" for="radio2_<?php echo $result['reg_id'];?>">
                                      <input type="radio" id="radio2_<?php echo $result['reg_id'];?>" name="<?php echo $result['reg_id'];?>" value="2" />
                                      <div class="plan-content">
                                          <!-- <img loading="lazy" src="https://raw.githubusercontent.com/ismailvtl/ismailvtl.github.io/master/images/potted-plant-img.svg" alt="" /> -->
                                          <div class="plan-details">
                                          <!-- <span>Option 2.</span> -->
                                          <h4><?php echo $result['option2'];?></h4>
                                          </div>
                                      </div>
                                    </label>

 
                                    <?php if(isset( $result['option3'] ) && !empty( $result['option3'] )){ ?>
                                    <label class="plan radio3_<?php echo $result['reg_id'];?>-plan mb-3" for="radio3_<?php echo $result['reg_id'];?>">
                                      <input type="radio" id="radio3_<?php echo $result['reg_id'];?>" name="<?php echo $result['reg_id'];?>" value="3" />
                                      <div class="plan-content">
                                          <!-- <img loading="lazy" src="https://raw.githubusercontent.com/ismailvtl/ismailvtl.github.io/master/images/potted-plant-img.svg" alt="" /> -->
                                          <div class="plan-details">
                                          <!-- <span>Option 3.</span> -->
                                          <h3><?php echo $result['option3'];?></h4>
                                          </div>
                                      </div>
                                    </label>
                                    <?php } ?>				
 
                                    <?php if(isset( $result['option3'] ) && !empty( $result['option3'] )){ ?>
                                    <label class="plan radio4_<?php echo $result['reg_id'];?>-plan mb-3" for="radio4_<?php echo $result['reg_id'];?>">
                                      <input type="radio" id="radio4_<?php echo $result['reg_id'];?>" name="<?php echo $result['reg_id'];?>" value="4" />
                                      <div class="plan-content">
                                          <!-- <img loading="lazy" src="https://raw.githubusercontent.com/ismailvtl/ismailvtl.github.io/master/images/potted-plant-img.svg" alt="" /> -->
                                          <div class="plan-details">
                                          <!-- <span>Option 4.</span> -->
                                          <h4><?php echo $result['option4'];?></h4>
                                          </div>
                                      </div>
                                    </label>
                                    <?php } ?>				
                                      

                                </div>
                           
                                <input type="radio" checked='checked' style='display:none' value="smart_quiz" id='radio1_<?php echo $result['reg_id'];?>' name='<?php echo $result['reg_id'];?>'/>                                                                      
               






 
                          <center>  
          

                         
							<?php
                               
								 $i++; 
								 if ( ( $remainder < 1 ) || ( $i == $number_question && $remainder == 1 ) ) {
								 	echo "<button id='".$j."' class='next btn btn-success btn-lg px-4' type='submit'>Finish</button>";
								 	echo "</div>";
								 }  elseif ( $rowcount > $number_question  ) {
								 	if ( $j == 1 && $i == $number_question ) {
										echo "<button id='".$j."' class='next btn btn-success btn-lg px-4' type='button'>Next</button>";
										echo "</div>";
										$i = 0;
										$j++;           
									} elseif ( $k == $rowcount ) { 
										echo " <button id='".$j."' class='previous btn btn-success btn-lg px-4' type='button'>Previous</button>
													<button id='".$j."' class='next btn btn-success btn-lg px-4' type='submit'>Finish</button>";
										echo "</div>";
										$i = 0;
										$j++;
									} elseif ( $j > 1 && $i == $number_question ) {
										echo "<button id='".$j."' class='previous btn btn-success btn-lg px-4' type='button'>Previous</button>
								                    <button id='".$j."' class='next btn btn-success btn-lg px-4' type='button' >Next</button>";
										echo "</div>";
										$i = 0;
										$j++;
									}
									
								 }
								  $k++;
						} 
                        
                        ?>	

                        <br>

          </center>  
      

						</form>
	     		</div>
	     		
			</div>
		</div>	
</main>


 


<script>




    $('.cont').hide();
		$('#question_splitter_1').show();
		$(document).on('click','.next',function(){


		    last=parseInt($(this).attr('id'));  
        
		    nex=last+1;
		    $('#question_splitter_'+last).hide();
		    
		    $('#question_splitter_'+nex).show();
		});
		
		$(document).on('click','.previous',function(){
		    last=parseInt($(this).attr('id'));     
		    pre=last-1;
		    $('#question_splitter_'+last).hide();
		    
		    $('#question_splitter_'+pre).show();
		});


		
        var c = 180; // 3minutes
        var t;
        timedCount();

        function timedCount() {

        	var hours = parseInt( c / 3600 ) % 24;
        	var minutes = parseInt( c / 60 ) % 60;
        	var seconds = c % 60;

        	var result = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);

            
        	$('#timer').html(result);
            if(c == 0 ){
            	setConfirmUnload(false);
                $("#quiz_form").submit();
            }
            c = c - 1;
            t = setTimeout(function(){ timedCount() }, 1000);
        }
	</script>

<script type="text/javascript">
    // Prevent accidental navigation away
    setConfirmUnload(true);
    function setConfirmUnload(on)
    {
        window.onbeforeunload = on ? unloadMessage : null;
    }
    function unloadMessage()
    {
        return 'Your Answered Questions are resetted zero, Please select stay on page to continue your Quiz';
    }

    $(document).on('click', 'button:submit',function(){
        setConfirmUnload(false);
    });
</script>


</body>
</html>