<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MathApp3</title>
 
   

    <link rel="apple-touch-icon" sizes="120x120" href="<?=THEME2;?>assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=THEME2;?>assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=THEME2;?>assets/img/favicon/favicon-16x16.png">


    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link type="text/css" href="https://demo.themesberg.com/pixel-pro/v5/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://demo.themesberg.com/pixel-pro/v5/vendor/@glidejs/glide/dist/css/glide.core.min.css">
    <link rel="stylesheet" href="https://demo.themesberg.com/pixel-pro/v5/vendor/@glidejs/glide/dist/css/glide.theme.min.css"> -->
    <link type="text/css" href="<?= THEME1;?>/css/pixel.css" rel="stylesheet">
 
    <link type="text/css" href="<?=THEME1;?>vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">


  
     
  </head>
  <body>
  
    <header class="header-global" style="display:none;">
      <nav id="navbar-main" aria-label="Primary navigation" class="navbar navbar-main navbar-expand-lg navbar-theme-primary headroom navbar-dark navbar-theme-primary">
        <div class="container position-relative">
          <a class="navbar-brand me-lg-5" href="">

<!--           
            <img class="navbar-brand-dark" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/brand/light.svg" alt="Logo light">
            <img class="navbar-brand-light" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/brand/dark.svg" alt="Logo dark"> -->
            <!-- <img class="navbar-brand-dark" src="<?=BASEPATH;?>assets/images/logo.png" alt="Logo dark"> -->

          </a>
      
          <!-- <div class="d-flex align-items-center">
            <a href="index.php"   class="btn btn-outline-gray-100 d-none d-lg-inline me-md-3">
              <span class="fas fa-book me-2"></span> Dashboard </a>
          
          </div> -->
        </div>
      </nav>
    </header>