<?php $language = language(); ?>


<main>

 
     <section class="section-header pb-5X pb-sm-7X bg-primaryX text-whiteX">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-12 col-lg-10 text-center">
              <div class="mb-4">
                <a href="" class="badge bg-success text-uppercase me-2 px-3"><?= $_SESSION['mathapp']['prelogin']['school_name'];?></a>
                <!-- <a href="#" class="badge bg-warning text-uppercase px-3">Marketing</a> -->
              </div>
              <h1 class="display-3 mb-4 px-lg-5"><?= $lang[$language]['WELCOME_TO_SECTION'];?> <b><?= ucwords($section->section_name);?></b></h1>
              <div class="post-meta">
                <span class="fw-bold me-3"><?= $lang[$language]['SELECT_YOUR_ACCOUNT_NAME'];?></span>
              </div>
            </div>
          </div>


          <div class="row justify-content-center mb-4 mb-lg-5">
            <div class="col">
           
              <!-- <h2 class="h2">Login to your account name</h2> -->
            </div>
          </div>
          <div class="row">

          <?php foreach ($dataArray["body"] as $data) { ?>
            <div class="col-12 col-xl-3 mb-4">
              <div class="card bg-white border-gray-300 p-3"> 

                <div class="card-header bg-white border-0 p-3 p-md-4"> 

                

                  <div class="avatarX avatar-lgX mx-auto mb-3">
                                    <!-- <img class="rounded-circle" alt="Image placeholder" src="../../assets/img/team/profile-picture-3.jpg"> -->

                                    <img src=" <?= avatar($data['avatar']);?>" class="mb-2 rounded-circleX img-thumbnailX image-lgX border-secondaryX p-2X" alt="Joseph Avatar">
                                </div>
                                
                  <h3 class="mb-3 h5 text-center"><?= $data['first_name'] . ' ' . $data['last_name']; ?></h3>  
                </div>

                <div class="card-body px-2X px-md-4X pt-0 "> 
              

                  <div class="mt-0 text-center">

                    <a href="auth.php?pid=<?= encrypt_decrypt('encrypt', $data['reg_id']); ?>" class="btn btn-sm btn-tertiary me-3 animate-up-2 px-3"> <span class="fas fa-paper-plane"></span> Login</a> 
 
                  </div>



                </div>
              </div>
            </div>

            <?php } ?>


           
          
            </div>





        </div>
      </section>




 



</main>