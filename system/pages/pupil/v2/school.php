
<style>
@keyframes animatedBackground {
  from {
    background-position: 0 0;
  }
  to {
    background-position: 100% 0;
  }
}
#animate-area {
  /* width: 200px;
  height: 200px; */
  background-image: url(https://mathapp/assets/imageslessonbg.jpg);
  /* background-position: 0px 0px;
  background-repeat: repeat-x; */
  animation: animatedBackground 10s linear infinite alternate;
}
 
</style>

<script src="https://code.jquery.com/jquery-3.6.3.slim.js" integrity="sha256-DKU1CmJ8kBuEwumaLuh9Tl/6ZB6jzGOBV/5YpNE2BWc=" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />



    <main>
     
      <section class="section section-header pb-6 bg-primary text-white overflow-hidden z-2 " id="animate-area">
        <div class="container z-2">
          <div class="row justify-content-center mb-2">
            <div class="col-lg-8 col-xl-8">
              <div class="text-center">
                <h1 class="display-2 mb-3">Welcome to MathApp3.com</h1>
                <!-- <p class="lead text-muted">Enter your section code number provided by your teacher -->
                </p>
              </div>
              <form class="row d-flex align-items-top justify-content-center mt-5" action="<?= SUBMIT;?>" method="post">
              <?php // resultMessage(); ?>
                <div class="col-12 col-xl-10">
                  <div class="form-group">
                    <label class="h6" for="email">Select your school</label>
                    <div class="d-flex flex-row justify-content-center">
                      <!-- <input class="form-control form-control-lg border-0 rounded" id="code" name="code" placeholder="Enter your section code" type="number" required> -->

                                        <select class="js-example-basic-single2 js-states form-control" id=" " name="sid" required>
                                            <option value="" selected>Select Your School</option>
                                            <?php foreach ($dataArray['body'] as $data): ?>
                                            <option value="<?= encrypt_decrypt('encrypt', $data['reg_id']);?>"><?= $data['school_name'];?></option> 
                                            <?php endforeach; ?>
                                         
                                        </select>



                      <button type="submit" class="btn btn-tertiary btn-lg ms-2">GO</button>
                    </div>
                  </div>
                  <!-- <p class="small text-left text-muted mt-2">Enter your section code number provided by your teacher</p> -->
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>


      <div class="section section-md">
        <div class="container">
          <!-- <ul class="nav justify-content-center mb-4">
            <li class="nav-item">
              <a class="nav-link text-instagram h3 px-4" aria-label="instagram social link" href="#">
                <span class="fab fa-instagram"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-facebook h3 px-4" aria-label="facebook social link" href="#">
                <span class="fab fa-facebook"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-github h3 px-4" aria-label="github social link" href="#">
                <span class="fab fa-github"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-dribbble h3 px-4" aria-label="dribbble social link" href="#">
                <span class="fab fa-dribbble"></span>
              </a>
            </li>
          </ul> -->
          <div class="row justify-content-center align-items-center">
            <div class="col-lg-8 text-center">

              <img class="navbar-brand-dark" src="<?=BASEPATH;?>assets/images/logo.png" alt="Logo dark" width="300"> 

              <p class="h3 fw-normal lh-160">MathApp3 is a unique and awesome math application for Grade 3 pupils .  </p>
              <!-- <div class="mt-5">
                <a href="https://themesberg.com/product/ui-kit/pixel-pro-premium-bootstrap-5-ui-kit" class="btn btn-secondary animate-up-1">Purchase now</a>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </main>


    <script>
        $(document).ready(function() {
            $('.js-example-basic-single').select2({
                // theme: "classic"
                theme: 'bootstrap-5'
            });

            $('.js-example-basic-single2').select2({
                // theme: "classic"
                theme: 'bootstrap-5'
            });
        });

    </script>