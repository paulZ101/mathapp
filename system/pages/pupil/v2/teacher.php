<?php $language = language(); ?>



<main>


    <div class="section-header section-image bg-white pb-0">
        <div class="container z-2">
          <div class="row my-5 justify-content-center text-center">
            <div class="col-lg-8">
              <img src="<?= THEME1;?>assets/img/team/icon1.png" class="rounded-circle img-thumbnail image-lg border-secondary p-2" alt="Joseph Avatar">
              <h1 class="my-4"><?= $lang[$language]['GOOD_DAY'];?></h1>
              <h2 class="h5 fw-normal text-gray mb-4">
                <!-- <span class="fas fa-map-marker-alt me-2"></span>   -->
                <?= $lang[$language]['I_AM_YOUR_TEACHER'];?> <?= strtoupper($data2['first_name'].' '.$data2['last_name']);?>            
              </h2>
 

              
              <a href="section.php?sid=<?=encrypt_decrypt('encrypt',$data2['school_reg_id']);?>" type="button" class="btn btn-outline-primary mb-2 mx-1 animate-up-2 text-whiteX">
                                <span class="me-2">
                                <!-- <i class="far fa-thumbs-up"></i> -->
                                </span>BACK </a>

                            <a href="login.php" type="button" class="btn btn-primary mb-2 mx-1 animate-down-2">
                                <span class="me-2">
                                <!-- <i class="fas fa-times"></i> -->
                                </span>NEXT </a>


            </div>
          </div>
        </div>
    </div>



</main>