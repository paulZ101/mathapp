<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                
                
            </div>

            <div class="row">
                <div class="col-12 col-xl-8">
                    <div class="card card-body border-0 shadow mb-4">
                        <h2 class="h5 mb-4"><?=$page['title'];?></h2>
                        <?php resultMessage(); ?>
                        <form action="<?=SUBMIT?>" method="post">
                        <input type="hidden" name="id" value="<?=!empty($_GET['id'])? $_GET['id']:'';?>">
                
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="first_name">First Name</label>
                                        <input class="form-control" name="first_name" id="first_name" type="text" placeholder="Enter first_name" value="<?=!empty($data['first_name'])? $data['first_name']:'';?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="last_name">Last Name</label>
                                        <input class="form-control" name="last_name" id="last_name" type="text" placeholder="Enter last_name" value="<?=!empty($data['last_name'])? $data['last_name']:'';?>" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="username">Username</label>
                                        <input class="form-control" name="username" id="username" type="text" placeholder="Enter username" value="<?=!empty($data['username'])? $data['username']:'';?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="password">Password</label>
                                        <input class="form-control" name="password" id="password" type="text" placeholder="Enter password" value="<?=!empty($data['password'])? $data['password']:'';?>"  >
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="student_id">Studen ID</label>
                                        <input class="form-control" name="student_id" id="student_id" type="text" placeholder="Enter student_id" value="<?=!empty($data['student_id'])? $data['student_id']:'';?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="gender">Section</label>
                                    <select class="form-select mb-0" id="section_id" name="section_id" aria-label="" required>
                                        <option value="">Select Section</option>
                                        <?php foreach($sectionArr['body'] as $section): ?>  
                                            <option value="<?=$section['reg_id'];?>" <?=(!empty($data['section_id']) && $data['section_id']==$section['reg_id']) ? 'selected':'';?>><?=$section['section_name'];?></option> 
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="gender">Status</label>
                                    <select class="form-select mb-0" name="status" id="status" name="status" aria-label="">
                                        <option value="">Select Status</option>
                                        <option value="0" <?=(!empty($data['status']) && $data['status']=='0') ? 'selected':'';?>>Inactive</option>
                                        <option value="1" <?=(!empty($data['status']) && $data['status']=='1') ? 'selected':'';?>>Active</option> 
                                    </select>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="gender">Gender</label>
                                    <select class="form-select mb-0" id="section_id" name="gender" aria-label="" required>
                                        <option value="">Select Gender</option>
                                        <option value="M" <?=(!empty($data['gender']) && $data['gender']=='M') ? 'selected':'';?>>Male</option>
                                        <option value="F" <?=(!empty($data['gender']) && $data['gender']=='F') ? 'selected':'';?>>Female</option> 
                                    </select>
                                </div>


                                
                            </div>

                            

                            <div class="mt-3">
                                <button class="btn btn-primary mt-2 animate-up-2" type="submit">Save</button>
                            </div>
                        </form>
                    </div>
                   
                </div>
              
            </div>