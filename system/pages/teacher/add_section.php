<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                
                
            </div>

            <div class="row">
                <div class="col-12 col-xl-8">
                    <div class="card card-body border-0 shadow mb-4">
                        <h2 class="h5 mb-4"><?=$page['title'];?></h2>
                        <?php resultMessage(); ?>
                        <form action="<?=SUBMIT?>" method="post">
                        <input type="hidden" name="id" value="<?=!empty($_GET['id'])? $_GET['id']:'';?>">
                
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="section_name">Section Name</label>
                                        <input class="form-control" name="section_name" id="section_name" type="text" placeholder="Enter section_name" value="<?=!empty($data['section_name'])? $data['section_name']:'';?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    
                                        <label for="status">Status</label>
                                        <select class="form-select mb-0" name="status" id="status"  aria-label="">
                                            <option value="">Select Status</option>
                                            <option value="0" <?=(!empty($data['status']) && $data['status']=='0') ? 'selected':'';?>>Inactive</option>
                                            <option value="1" <?=(!empty($data['status']) && $data['status']=='1') ? 'selected':'';?>>Active</option> 
                                        </select>
                                    
                                </div>
                            </div>

                            <div class="row align-items-center">
                               <div class="col-md-6 mb-3">
                                   <label for="q1">Quarter 1</label>
                                   <select class="form-select mb-0" name="q1" id="q1"  aria-label="">
                                       <option value="0" <?=(!empty($data['q1']) && $data['q1']=='0') ? 'selected':'';?>>Disabled</option>
                                       <option value="1" <?=(!empty($data['q1']) && $data['q1']=='1') ? 'selected':'';?>>Enabled</option> 
                                   </select>
                               </div>
                               <div class="col-md-6 mb-3">
                                   <label for="q2">Quarter 2</label>
                                   <select class="form-select mb-0" name="q2" id="q2"aria-label="">
                                       <option value="0" <?=(!empty($data['q2']) && $data['q2']=='0') ? 'selected':'';?>>Disabled</option>
                                       <option value="1" <?=(!empty($data['q2']) && $data['q2']=='1') ? 'selected':'';?>>Enabled</option> 
                                   </select>
                               </div>
                           </div> 

                           <div class="row align-items-center">
                               <div class="col-md-6 mb-3">
                                   <label for="q3">Quarter 3</label>
                                   <select class="form-select mb-0" name="q3" id="q3" aria-label="">
                                       <option value="0" <?=(!empty($data['q3']) && $data['q3']=='0') ? 'selected':'';?>>Disabled</option>
                                       <option value="1" <?=(!empty($data['q3']) && $data['q3']=='1') ? 'selected':'';?>>Enabled</option> 
                                   </select>
                               </div>
                               <div class="col-md-6 mb-3">
                                   <label for="q4">Quarter 4</label>
                                   <select class="form-select mb-0" name="q4" id="q4" aria-label="">
                                       <option value="0" <?=(!empty($data['q4']) && $data['q4']=='0') ? 'selected':'';?>>Disabled</option>
                                       <option value="1" <?=(!empty($data['q4']) && $data['q4']=='1') ? 'selected':'';?>>Enabled</option> 
                                   </select>
                               </div>
                           </div> 

 
                           
                           <div class="row align-items-center">
                               <div class="col-md-6 mb-3">
                                   <label for="n1">Numeracy 1</label>
                                   <select class="form-select mb-0" name="n1" id="n1" aria-label="">
                                       <option value="0" <?=(!empty($data['n1']) && $data['n1']=='0') ? 'selected':'';?>>Disabled</option>
                                       <option value="1" <?=(!empty($data['n1']) && $data['n1']=='1') ? 'selected':'';?>>Enabled</option> 
                                   </select>
                               </div>
                               <div class="col-md-6 mb-3">
                                   <label for="n2">Numeracy 2</label>
                                   <select class="form-select mb-0" name="n2" id="n2"  aria-label="">
                                       <option value="0" <?=(!empty($data['n2']) && $data['n2']=='0') ? 'selected':'';?>>Disabled</option>
                                       <option value="1" <?=(!empty($data['n2']) && $data['n2']=='1') ? 'selected':'';?>>Enabled</option> 
                                   </select>
                               </div>
                           </div> 


  

                            <div class="mt-3">
                                <button class="btn btn-primary mt-2 animate-up-2" type="submit">Save</button>
                            </div>
                        </form>
                    </div>
                   
                </div>
              
            </div>