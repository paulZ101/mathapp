<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                
                
            </div>

            <div class="row">
                <div class="col-12 col-xl-8">
                    <div class="card card-body border-0 shadow mb-4">
                        <h2 class="h5 mb-4"><?=$page['title'];?></h2>
                        <?php resultMessage(); ?>
                        <form action="<?=SUBMIT?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="<?=!empty($_GET['id'])? $_GET['id']:'';?>">
                
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="formFile" class="form-label">Upload CSV File</label>
                                    <input class="form-control" type="file" id="UploadFile" name="UploadFile" required>
                                </div>
                            </div>
 

                            <div class="row align-items-center">
                                

                                <div class="col-md-6 mb-3">
                                    <label for="gender">Section</label>
                                    <select class="form-select mb-0" id="section_id" name="section_id" aria-label="" required>
                                        <option value="">Select Section</option>
                                        <?php foreach($sectionArr['body'] as $section): ?>  
                                            <option value="<?=$section['reg_id'];?>" <?=(!empty($data['section_id']) && $data['section_id']==$section['reg_id']) ? 'selected':'';?>><?=$section['section_name'];?></option> 
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="gender">Status</label>
                                    <select class="form-select mb-0" name="status" id="status" name="status" aria-label="" required>
                                        <option value="0" <?=(!empty($data['status']) && $data['status']=='0') ? 'selected':'';?>>Inactive</option>
                                        <option value="1" <?=(!empty($data['status']) && $data['status']=='1') ? 'selected':'';?>>Active</option> 
                                    </select>
                                </div>
                            </div> 

                            

                            <div class="mt-3">
                                <button class="btn btn-primary mt-2 animate-up-2" type="submit">Save</button>
                            </div>
                        </form>
                    </div>
                   
                </div>
              
            </div>