<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                <div class="d-block mb-4 mb-md-0">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                        <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">
                            <li class="breadcrumb-item">
                                <a href="#">
                                    <svg class="icon icon-xxs" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path></svg>
                                </a>
                            </li>
                            <!-- <li class="breadcrumb-item"><a href="#"></a></li> -->
                            <li class="breadcrumb-item active" aria-current="page"><?=$page['title'];?></li>
                        </ol>
                    </nav>
                    <h2 class="h4"><?=$page['title'];?></h2>
                    <!-- <p class="mb-0">Your web analytics dashboard template.</p> -->
                </div>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <!-- <a href="<?= addLink(); ?>" class="btn btn-sm btn-gray-800 d-inline-flex align-items-center">
                        <svg class="icon icon-xs me-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg>
                        Add New
                    </a> -->
                   
                </div>
            </div>
            
            <?php resultMessage(); ?>
           
            <div class="card card-body border-0 shadow table-wrapper table-responsive">
                <table class="table table-hover" id="tbl">
                    <thead>
                        <tr>
                            <th class="border-gray-200">Names</th>
                            <th class="border-gray-200">Addition</th>
                            <th class="border-gray-200">Subtraction </th>
                            <th class="border-gray-200">Multiplication </th>
                            <th class="border-gray-200">Division </th>
                           
                
                            <th class="border-gray-200">Interpretation</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Item -->
                        <?php foreach($dataArray['body'] as $data): ?>

                        <tr>
                            <td>
                                <a href="#" class="fw-bold"><?= $data['last_name'].' '.$data['first_name']; ?></a><br><small><?= $data['section_name']; ?></small>
                            </td>

                            <td><a href="#" class="fw-bold"><?= $data['addition']; ?></a></td>
                            <td><a href="#" class="fw-bold"><?= $data['subtraction']; ?></a></td>
                            <td><a href="#" class="fw-bold"><?= $data['multiplication']; ?></a></td>
                          
                            <td><a href="#" class="fw-bold"><?= $data['division']; ?></a></td>
                          
                       

                            <td>
                                <?php

                                // HN
                                // - Kung perfect nimo tanan
                                // - Kun example 3 imong score sa addition nya the rest is 2 na imong score HN gihapon ka.
                                // - Atleast mayda ka perfect score and the rest dili below 2 na score belong ka sa HN

                                // MN
                                // - If myda ka score na zero automatic MN bisag perfect ka sa uban pero mayda ka zero score MN ka belong.
                                // - Then kung an score nimo tanan is puro below 3

                                // Ang NN is zero gyud tanan





                                if( $data['addition']==0 AND  $data['subtraction']==0 AND $data['multiplication']==0 AND $data['division']==0  ){
                                    echo '<span style="color:red;"><b>NN</b></span>';
                                }else if( $data['addition']==0 OR  $data['subtraction']==0 OR $data['multiplication']==0 OR $data['division']==0  ){
                                    echo '<span style="color:orange;"><b>MN</b></span>';
                                }else{
                                    echo '<span style="color:dark blue;"><b>HN</b></span>';
                                }
                                
                                

                                ?>


                            </td>

                           
                        </tr>

                        <?php endforeach; ?>
                                                   
                    </tbody>
                </table>
                
            </div>