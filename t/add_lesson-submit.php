<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/lessons.php';

authorize('teacher');

$label = 'lesson';
$main_page = $label.'s.php';

if (empty($_POST['lesson_title'])) {
    $_SESSION['mathapp']['error'] = 'Please enter a lesson_title'; 
    header('location: '.FILE_BASENAME);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$$label = new Lessons($db);
$$label->lesson_title = $_POST['lesson_title'];
$$label->module_id = strtoupper($_POST['module_id']);
$$label->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];

// $$label->getByTitle();
$$label->getByTitleModuleIDTeacherRegID(); 
 
if ($$label->lesson_title != null) { 
 
    $_SESSION['mathapp']['error'] = 'Lesson Title Already Exists ';
    header('location: ' . FILE_BASENAME);
    exit; 

}

/*
first_name
last_name
lesson_title
password
student_id
section_id
status
*/

$$label->lesson_title = $_POST['lesson_title']; 
$$label->status = $_POST['status'];   
$$label->module_id = $_POST['module_id']; 
 
$$label->teacher_reg_id = $_SESSION['mathapp']['login']['teacher']; 
 
if($$label->create()){
    $_SESSION['mathapp']['success'] = $label . ' created successfully.';
} else {
    $_SESSION['mathapp']['error'] = $label . ' could not be created.';
}

header('location: '.FILE_BASENAME);


