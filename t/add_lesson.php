<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/modules.php'; 

$page['title'] = 'Add Lesson';

authorize('teacher');

$database = new Database();
$db = $database->getConnection(); 

$module = new Modules($db); 
$module->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];
$stmt = $module->getActive();  
$moduleCount = $stmt->rowCount();

$moduleArr = array();
$moduleArr["body"] = array();
$moduleArr["itemCount"] = $moduleCount;

if ($moduleCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "module_title" => $module_title,
            "category_name" => $category_name
        );
        
        array_push($moduleArr["body"], $e);
    } 
}

require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/add_lesson.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';