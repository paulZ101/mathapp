<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/modules.php';

authorize('teacher');

$label = 'module';
$main_page = $label.'s.php';

if (empty($_POST['module_title'])) {
    $_SESSION['mathapp']['error'] = 'Please enter a module_title'; 
    header('location: '.FILE_BASENAME);
    exit;
}

if (empty($_POST['category_id'])) {
    $_SESSION['mathapp']['error'] = 'category is required'; 
    header('location: '.FILE_BASENAME);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$$label = new modules($db);

$$label->category_id = $_POST['category_id'];
$$label->module_title = strtoupper($_POST['module_title']);
$$label->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];

$$label->getByTitleCategoryIDTeacherRegID(); 

if ($$label->module_title != null) { 
   
    // if ($$label->teacher_reg_id == $_SESSION['mathapp']['login']['teacher']) {

        $_SESSION['mathapp']['error'] = 'Module Title Already Exists ';
        header('location: ' . FILE_BASENAME);
        exit;

    // } 

}
 
 
$$label->module_title = strtoupper($_POST['module_title']);
$$label->category_id = $_POST['category_id'];
$$label->status = $_POST['status'];   
$$label->teacher_reg_id = $_SESSION['mathapp']['login']['teacher']; 
 
if($$label->create()){
    $_SESSION['mathapp']['success'] = 'module created successfully.';
} else {
    $_SESSION['mathapp']['error'] = 'module could not be created.';
}

$logs = new ActivityLogs($db);
$logs->account_type = 'teacher'; 
$logs->account_id = $_SESSION['mathapp']['login']['teacher'];   
$logs->activity = 'teacher add module'; 
$logs->description = REQUEST_URI;
$logs->ip_address = '';
$logs->create(); 

header('location: '.FILE_BASENAME);


