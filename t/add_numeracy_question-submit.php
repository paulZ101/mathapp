<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/numeracyQuestions.php';

authorize('teacher');

$label = 'numeracy_question';
$main_page = $label.'s.php';

if (empty($_POST['number1']) || empty($_POST['number2'])) {
    $_SESSION['mathapp']['error'] = 'Please enter a question'; 
    header('location: '.FILE_BASENAME);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$$label = new numeracyQuestions($db);

$$label->number1 = $_POST['number1']; 
$$label->number2 = $_POST['number2']; 
$$label->answer = $_POST['answer']; 
$$label->operator = $_POST['operator']; 
$$label->numeracy = $_POST['numeracy']; 
$$label->status = $_POST['status']; 
$$label->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];
 
if($$label->create()){
    $_SESSION['mathapp']['success'] = 'question created successfully.';
} else {
    $_SESSION['mathapp']['error'] = 'question could not be created.';
}

header('location: '.FILE_BASENAME);