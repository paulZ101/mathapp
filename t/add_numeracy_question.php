<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/lessons.php'; 

$page['title'] = 'Add Numeracy Question';

authorize('teacher');

$database = new Database();
$db = $database->getConnection(); 

$lesson = new Lessons($db); 

$lesson->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];
$stmt = $lesson->get();  
$lessonCount = $stmt->rowCount();

$lessonArr = array();
$lessonArr["body"] = array();
$lessonArr["itemCount"] = $lessonCount;

if ($lessonCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "lesson_title" => $lesson_title,
            "category_name" => $category_name,
            "module_title" => $module_title
            
        );
        
        array_push($lessonArr["body"], $e);
    } 
} 

require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/add_numeracy_question.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';