<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php';

authorize('teacher');

$label = 'pupil';
$main_page = $label.'s.php';

if (empty($_POST['username'])) {
    $_SESSION['mathapp']['error'] = 'Please enter a username'; 
    header('location: '.FILE_BASENAME);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$$label = new pupils($db);
$$label->username = $_POST['username'];

$$label->getByUsername();
 
if ($$label->username != null) {
    if ($$label->teacher_reg_id == $_SESSION['mathapp']['login']['teacher']) {
        $_SESSION['mathapp']['error'] = 'username already exists';
        header('location: ' . FILE_BASENAME);
        exit;
    }
}

/*
first_name
last_name
username
password
student_id
section_id
status
*/

 
$gender = $_POST['gender'];
 

if($gender=='M'){
    $avatar = rand(1,3);
}else if($gender=='F'){
    $avatar = rand(4,6);
}else{
    $avatar = '0';
}




$$label->gender = $gender; 
$$label->avatar = $avatar; 

$$label->first_name = $_POST['first_name']; 
$$label->last_name = $_POST['last_name']; 
$$label->student_id = $_POST['student_id']; 

$$label->username = $_POST['username']; 
$$label->password = $_POST['password']; 
$$label->status = $_POST['status'];   
$$label->section_id = $_POST['section_id'];   
 
$$label->gender = $_POST['gender'];   
$$label->reg_type = 'teacher';  
$$label->teacher_reg_id = $_SESSION['mathapp']['login']['teacher']; 
 
if($$label->create()){
    $_SESSION['mathapp']['success'] = $label . ' created successfully.';
} else {
    $_SESSION['mathapp']['error'] = $label . ' could not be created.';
}

header('location: '.FILE_BASENAME);


