<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/sections.php';

authorize('teacher');

$label = 'section';
$main_page = $label.'s.php';

if (empty($_POST['section_name'])) {
    $_SESSION['mathapp']['error'] = 'Please enter a section_name'; 
    header('location: '.FILE_BASENAME);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$$label = new Sections($db);
$$label->section_name = $_POST['section_name'];

$$label->getByName();
 

if ($$label->section_name != null) {

    if ($$label->teacher_reg_id == $_SESSION['mathapp']['login']['teacher']) {
        $_SESSION['mathapp']['error'] = 'section_name already exists';
        header('location: ' . FILE_BASENAME);
        exit;
    }

}

// if($get_new_insert_id > 0){
//     $set_account_number = 'BC'.str_pad($get_new_insert_id,7,0,STR_PAD_LEFT);
//     $sql2="UPDATE ".$db.".users SET account_number='".$set_account_number."' WHERE reg_id='".$get_new_insert_id."' ";
//     if(!$res = $conn->query($sql2)){die('Database error: [' . $conn->error . ']');} 
// }
    


$$label->section_name = $_POST['section_name'];
$$label->section_code = ''; //date("ymdHis");
$$label->status = $_POST['status'];   

$$label->q1 = $_POST['q1'];   
$$label->q2 = $_POST['q2'];   
$$label->q3 = $_POST['q3'];   
$$label->q4 = $_POST['q4'];   
$$label->n1 = $_POST['n1'];   
$$label->n2 = $_POST['n2'];   

$$label->teacher_reg_id = $_SESSION['mathapp']['login']['teacher']; 
 
if($$label->create()){
    $_SESSION['mathapp']['success'] = 'section created successfully.';

    $get_new_insert_id = $db->lastInsertId();
    $set_section_code = ''.str_pad($get_new_insert_id,6,0,STR_PAD_LEFT); 

    $$label->section_code = ($set_section_code);
    $$label->reg_id = $get_new_insert_id;

    echo $$label->section_code;
    echo '<br>';
    echo $$label->reg_id;

    $$label->updateSectionCode();

    
} else {
    $_SESSION['mathapp']['error'] = 'section could not be created.';
}

header('location: '.FILE_BASENAME);


