<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/lessons.php'; 

authorize('teacher');

$label = 'lesson';
$main_page = $label.'s.php';

if(empty($_POST['id'])){
    header('location: '.$main_page);
    exit;
} 

$decryptedID = encrypt_decrypt('decrypt', $_POST['id']);

if (empty($_POST['lesson_title'])) {
    $_SESSION['mathapp']['error'] = 'Please enter a lesson_title'; 
    header('location: '.FILE_BASENAME.'?id='.$decryptedID);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$$label = new Lessons($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

if($$label->teacher_reg_id!=$_SESSION['mathapp']['login']['teacher']){
    header('location:'.$main_page);
    exit;
} 


if(
    $$label->module_id == $_POST['module_id'] AND
    $$label->lesson_title == $_POST['lesson_title']  AND
    $$label->status == $_POST['status']  
    ){
        
        $_SESSION['mathapp']['error'] = 'No changes have been made';
        header('location: '.FILE_BASENAME.'?id='.$_POST['id']);
        exit;
    }

$check = new Lessons($db);

$check->lesson_title = $_POST['lesson_title'];
$check->module_id = strtoupper($_POST['module_id']);
$check->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];

// $$label->getByTitle();
$check->getByTitleModuleIDTeacherRegID(); 
 
if ($check->lesson_title != null) { 
 
    if( $$label->status == $_POST['status'] ){
        $_SESSION['mathapp']['error'] = 'Lesson Title Already Exists ';
        header('location: ' . FILE_BASENAME);
        exit; 
    }
    
}

$$label->lesson_title = $_POST['lesson_title']; 
$$label->status = $_POST['status'];
$$label->module_id = $_POST['module_id'];
$$label->reg_id = $decryptedID;  

if($$label->update()){
    $_SESSION['mathapp']['success'] = $label.' updated successfully.';
} else {
    $_SESSION['mathapp']['error'] = $label.' could not be updated.';
}

header('location: '.FILE_BASENAME.'?id='.$_POST['id']);