<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/modules.php'; 
require_once DOCUMENT_ROOT.'system/classes/category.php'; 

$page['title'] = 'Edit Module';

authorize('teacher');

$label = 'module';
$main_page = $label.'s.php';

if(empty($_GET['id'])){
    header('location:'.$main_page);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_GET['id']);

$database = new Database();
$db = $database->getConnection();

$$label = new modules($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

if($$label->teacher_reg_id!=$_SESSION['mathapp']['login']['teacher']){
    header('location:'.$main_page);
    exit;
} 

$data = array(
    "reg_id" =>  $$label->reg_id,
    "module_title" => $$label->module_title,
    "category_id" => $$label->category_id,
    "status" => $$label->status,
    "teacher_reg_id" => $$label->teacher_reg_id
 
);


 

$category = new Category($db); 
 
$stmt = $category->get();  
$categoryCount = $stmt->rowCount();

$categoryArr = array();
$categoryArr["body"] = array();
$categoryArr["itemCount"] = $categoryCount;

if ($categoryCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "id" => $id,
            "category_name" => $category_name
        );
        
        array_push($categoryArr["body"], $e);
    } 
} 


require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/add_module.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';