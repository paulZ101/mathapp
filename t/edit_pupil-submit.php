<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 

authorize('teacher');

$label = 'pupil';
$main_page = $label.'s.php';

if(empty($_POST['id'])){
    header('location: '.$main_page);
    exit;
} 

$decryptedID = encrypt_decrypt('decrypt', $_POST['id']);

if (empty($_POST['username'])) {
    $_SESSION['mathapp']['error'] = 'Please enter a username'; 
    header('location: '.FILE_BASENAME.'?id='.$decryptedID);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$$label = new Pupils($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

if($$label->teacher_reg_id!=$_SESSION['mathapp']['login']['teacher']){
    header('location:'.$main_page);
    exit;
} 

$$label->username = $_POST['username'];
$$label->getByUsername();

if($$label->teacher_reg_id==$_SESSION['mathapp']['login']['teacher']){
    if($$label->username != null AND $$label->username != $_POST['username']){
        $_SESSION['mathapp']['error'] = 'username already exists';
        header('location: '.FILE_BASENAME.'?id='.$_POST['id']);
        exit;
    }
}

$$label->first_name = $_POST['first_name']; 
$$label->last_name = $_POST['last_name']; 
$$label->student_id = $_POST['student_id']; 

$$label->username = $_POST['username']; 
$$label->password = $_POST['password']; 
$$label->status = $_POST['status'];
$$label->section_id = $_POST['section_id'];
$$label->gender = $_POST['gender'];
$$label->reg_id = $decryptedID;  

if($$label->update()){
    $_SESSION['mathapp']['success'] = $label.' updated successfully.';
} else {
    $_SESSION['mathapp']['error'] = $label.' could not be updated.';
}

header('location: '.FILE_BASENAME.'?id='.$_POST['id']);