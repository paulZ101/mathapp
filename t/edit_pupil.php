<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 
require_once DOCUMENT_ROOT.'system/classes/sections.php';

$page['title'] = 'Edit pupil';

authorize('teacher');

$label = 'pupil';
$main_page = $label.'s.php';

if(empty($_GET['id'])){
    header('location:'.$main_page);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_GET['id']);

$database = new Database();
$db = $database->getConnection();

$$label = new Pupils($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

if($$label->teacher_reg_id!=$_SESSION['mathapp']['login']['teacher']){
    header('location:'.$main_page);
    exit;
} 
 
$data = array(
    "reg_id" =>  $$label->reg_id,
    "username" => $$label->username,
    "first_name" => $$label->first_name,
    "last_name" => $$label->last_name,
    "student_id" => $$label->student_id,
    "section_id" => $$label->section_id,
    "password" => $$label->password,
    "status" => $$label->status,
    "gender" => $$label->gender
);
 
 
$section = new Sections($db);
$section->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];
$stmt = $section->getActive();  
$sectionCount = $stmt->rowCount();

$sectionArr = array();
$sectionArr["body"] = array();
$sectionArr["itemCount"] = $sectionCount;

if ($sectionCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "section_name" => $section_name 
        );
        
        array_push($sectionArr["body"], $e);
    } 
}
 

require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/add_pupil.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';