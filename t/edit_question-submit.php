<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/questions.php'; 

authorize('teacher');

$label = 'question';
$main_page = $label.'s.php';

if(empty($_POST['id'])){
    header('location: '.$main_page);
    exit;
} 

$decryptedID = encrypt_decrypt('decrypt', $_POST['id']);

if (empty($_POST['question'])) {
    $_SESSION['mathapp']['error'] = 'Please enter a question'; 
    header('location: '.FILE_BASENAME.'?id='.$_POST['id']);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$$label = new Questions($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

if($$label->teacher_reg_id!=$_SESSION['mathapp']['login']['teacher']){
    header('location:'.$main_page);
    exit;
}


if (isset($_FILES["UploadFile"]["tmp_name"]) AND !empty($_FILES["UploadFile"]["tmp_name"])) {

    

    $target_dir = DOCUMENT_ROOT . 'uploads/';
    $htmlfield = 'UploadFile';
    $whitelist_extensions = ['gif', 'png', 'jpg', 'jpeg'];
    $whitelist_content_types = ['image/gif', 'image/png', 'image/jpeg'];
    $max_file_size = 500000;
    $min_file_size = 50;
    $max_filename_length = 63;
    $min_filename_length = 3; // including extension

    if (file_exists($target_dir) !== true) {
        $_SESSION['mathapp']['error'] = 'cannot upload file '.$target_dir;
        header('location: ' . FILE_BASENAME . '?id=' . $_POST['id']);
        exit;
    }

    $uname = $_FILES[$htmlfield]['name']; 

    if (!isset($_FILES) || !array_key_exists($htmlfield, $_FILES) || !array_key_exists('name', $_FILES[$htmlfield])) {
        $_SESSION['mathapp']['error'] = 'Please spefify a file name to upload';
        header('location: ' . FILE_BASENAME . '?id=' . $_POST['id']);
        exit;
    }

    // if (strlen($uname) > $max_filename_length) {
    //     $_SESSION['mathapp']['error'] = 'Sorry, the file name is too long.';
    //     header('location: ' . FILE_BASENAME . '?id=' . $_POST['id']);
    //     exit; 
    // }

    // if (strlen($uname) < $min_filename_length) {
    //     $_SESSION['mathapp']['error'] = 'Sorry, the file name is too short.';
    //     header('location: ' . FILE_BASENAME . '?id=' . $_POST['id']);
    //     exit;  
    // }

    $tmpname = $_FILES[$htmlfield]['tmp_name'];
    if (is_uploaded_file($tmpname) !== true) { 
        $_SESSION['mathapp']['error'] = 'Sorry, the file is not the uploaded one!';
        header('location: ' . FILE_BASENAME . '?id=' . $_POST['id']);
        exit;
    }

    // $unamen = Normalizer::normalize($uname);
    $unamen = ($uname);

    $lc = strtolower($unamen);

    $extlc = pathinfo($lc, PATHINFO_EXTENSION);
    if (in_array($extlc, $whitelist_extensions, true) !== true) {
        $_SESSION['mathapp']['error'] = "Sorry, the file extension '".htmlspecialchars($extlc)."' is not allowed";
        header('location: ' . FILE_BASENAME . '?id=' . $_POST['id']);
        exit; 
    }

    if ((in_array(mime_content_type($tmpname), $whitelist_content_types, true) !== true) ||
        (in_array($_FILES[$htmlfield]["type"], $whitelist_content_types, true) !== true)) {
            $_SESSION['mathapp']['error'] = "Sorry, this content type is not allowed.";
            header('location: ' . FILE_BASENAME . '?id=' . $_POST['id']);
            exit; 
    }

    $uname = newguid().'.'.$extlc; //NEW NAME

    $target_file = $target_dir.basename($uname);

    $target_rp = realpath(pathinfo($target_file, PATHINFO_DIRNAME));
    $targetdir_rp = realpath($target_dir);
    if ($target_rp === false || strpos($target_rp, $targetdir_rp) !== 0) { 
        $_SESSION['mathapp']['error'] = "Sorry, directory traversals are not allowed";
        header('location: ' . FILE_BASENAME . '?id=' . $_POST['id']);
        exit; 
    }

    if (file_exists($target_file) !== false) { 
        $_SESSION['mathapp']['error'] = "Sorry, file already exists.";
        header('location: ' . FILE_BASENAME . '?id=' . $_POST['id']);
        exit; 
    }

    $filesize = $_FILES[$htmlfield]['size'];

    if ($filesize > $max_file_size) { 
        $_SESSION['mathapp']['error'] = "Sorry, your file is too large.";
        header('location: ' . FILE_BASENAME . '?id=' . $_POST['id']);
        exit; 
    }

    if ($filesize < $min_file_size) { 
        $_SESSION['mathapp']['error'] = "Sorry, your file is too small.";
        header('location: ' . FILE_BASENAME . '?id=' . $_POST['id']);
        exit; 
    }

    $currentFile = $target_dir . $$label->imagefile;
    if (file_exists($currentFile) !== false) {
        unlink($currentFile);
    }

    if (move_uploaded_file($tmpname, $target_file)!==true) { 
        $_SESSION['mathapp']['error'] = "Sorry, there was an error uploading your file.";
        header('location: ' . FILE_BASENAME . '?id=' . $_POST['id']);
        exit; 
    }


    $imageName = $uname;
    
    
}

 


/*
$$label->question = $_POST['question'];
$$label->getByName();

if($$label->teacher_reg_id==$_SESSION['mathapp']['login']['teacher']){
    if($$label->question != null AND $$label->question != $_POST['question']){
        $_SESSION['mathapp']['error'] = 'question already exists';
        header('location: '.FILE_BASENAME.'?id='.$_POST['id']);
        exit;
    }
}*/




$$label->question = $_POST['question']; 
$$label->option1 = $_POST['option1']; 
$$label->option2 = $_POST['option2']; 
$$label->option3 = $_POST['option3']; 
$$label->option4 = $_POST['option4']; 
$$label->answer = $_POST['answer']; 
$$label->lesson_id = $_POST['lesson_id']; 
$$label->status = $_POST['status'];
$$label->reg_id = $decryptedID;  

if(isset($imageName)){
    $$label->imagefile = $imageName;  
}

if($$label->update()){
    $_SESSION['mathapp']['success'] = $label.' updated successfully.';
} else {
    $_SESSION['mathapp']['error'] = $label.' could not be updated.';
}

header('location: '.FILE_BASENAME.'?id='.$_POST['id']);