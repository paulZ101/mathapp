<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/questions.php'; 
require_once DOCUMENT_ROOT.'system/classes/lessons.php'; 

$page['title'] = 'Edit question';

authorize('teacher');

$label = 'question';
$main_page = $label.'s.php';

if(empty($_GET['id'])){
    header('location:'.$main_page);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_GET['id']);

$database = new Database();
$db = $database->getConnection();

$$label = new Questions($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

if($$label->teacher_reg_id!=$_SESSION['mathapp']['login']['teacher']){
    header('location:'.$main_page);
    exit;
} 

$data = array(
    "reg_id" =>  $$label->reg_id,
    "question" => $$label->question,
    "status" => $$label->status,
    "answer" => $$label->answer,
    "option1" => $$label->option1,
    "option2" => $$label->option2,
    "option3" => $$label->option3,
    "option4" => $$label->option4,
    "lesson_id" => $$label->lesson_id,
    "imagefile" => $$label->imagefile,
    "teacher_reg_id" => $$label->teacher_reg_id
 
);
 
$lesson = new Lessons($db); 
$lesson->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];
$stmt = $lesson->get();  
$lessonCount = $stmt->rowCount();

$lessonArr = array();
$lessonArr["body"] = array();
$lessonArr["itemCount"] = $lessonCount;

if ($lessonCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "lesson_title" => $lesson_title,
            "category_name" => $category_name,
            "module_title" => $module_title
        );
        
        array_push($lessonArr["body"], $e);
    } 
}
 

require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/add_question.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';