<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/sections.php'; 

authorize('teacher');

$label = 'section';
$main_page = $label.'s.php';

if(empty($_POST['id'])){
    header('location: '.$main_page);
    exit;
} 

$decryptedID = encrypt_decrypt('decrypt', $_POST['id']);

if (empty($_POST['section_name'])) {
    $_SESSION['mathapp']['error'] = 'Please enter a section_name'; 
    header('location: '.FILE_BASENAME.'?id='.$decryptedID);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$$label = new Sections($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

if($$label->teacher_reg_id!=$_SESSION['mathapp']['login']['teacher']){
    header('location:'.$main_page);
    exit;
} 

$$label->section_name = $_POST['section_name'];
$$label->getByName();
 

    if($$label->section_name != null AND $$label->section_name != $_POST['section_name']){
        if($$label->teacher_reg_id==$_SESSION['mathapp']['login']['teacher']){
            $_SESSION['mathapp']['error'] = 'section_name already exists';
            header('location: '.FILE_BASENAME.'?id='.$_POST['id']);
            exit;
        }
    }

$$label->section_name = $_POST['section_name']; 
$$label->status = $_POST['status'];

$$label->q1 = $_POST['q1'];   
$$label->q2 = $_POST['q2'];   
$$label->q3 = $_POST['q3'];   
$$label->q4 = $_POST['q4'];   
$$label->n1 = $_POST['n1'];   
$$label->n2 = $_POST['n2'];   

$$label->reg_id = $decryptedID;  

if($$label->update()){
    $_SESSION['mathapp']['success'] = $label.' updated successfully.';
} else {
    $_SESSION['mathapp']['error'] = $label.' could not be updated.';
}

header('location: '.FILE_BASENAME.'?id='.$_POST['id']);