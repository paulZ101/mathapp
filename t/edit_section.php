<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/sections.php'; 

$page['title'] = 'Edit Section';

authorize('teacher');

$label = 'section';
$main_page = $label.'s.php';

if(empty($_GET['id'])){
    header('location:'.$main_page);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_GET['id']);

$database = new Database();
$db = $database->getConnection();

$$label = new Sections($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

if($$label->teacher_reg_id!=$_SESSION['mathapp']['login']['teacher']){
    header('location:'.$main_page);
    exit;
} 

$data = array(
    "reg_id" =>  $$label->reg_id,
    "section_name" => $$label->section_name,
    "section_code" => $$label->section_code,
    "status" => $$label->status,
    "q1" => $$label->q1,
    "q2" => $$label->q2,
    "q3" => $$label->q3,
    "q4" => $$label->q4,
    "n1" => $$label->n1,
    "n2" => $$label->n2,
    "teacher_reg_id" => $$label->teacher_reg_id
 
); 

require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/add_section.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';