<?php
ob_start();
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php';

authorize('teacher');

if(isset($_SESSION['mathapp']['import-csv']['data'])){
    unset($_SESSION['mathapp']['import-csv']['data']);
}

if (!isset($_POST['section_id'])) {
    $_SESSION['mathapp']['error'] = 'section_id is required'; 
    header('location: '.FILE_BASENAME);
    exit;
}

if (!isset($_POST['status'])) {
    $_SESSION['mathapp']['error'] = 'status is required'; 
    header('location: '.FILE_BASENAME);
    exit;
}

$_SESSION['mathapp']['import-csv']['section_id'] = $_POST['section_id'];
$_SESSION['mathapp']['import-csv']['status'] = $_POST['status'];

if (isset($_FILES["UploadFile"]["tmp_name"])) {
    $filename=$_FILES["UploadFile"]["tmp_name"];    
    if($_FILES["UploadFile"]["size"] > 0)
    {
        $file = fopen($filename, "r");     
        $i=0;        
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE){
            if($i>0){
                if(!empty($getData[0])){
                    $_SESSION['mathapp']['import-csv']['data'][] = $getData;
                }
            } 
            $i++;
        }

        fclose($file);  
    }

    header('location: import_review.php');
    exit;

   
} 


header('location: '.FILE_BASENAME);
exit;