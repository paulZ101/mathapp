<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/lessons.php'; 

$page['title'] = 'Lessons';
$page['label'] = 'lesson';
 
authorize('teacher');

$database = new Database();
$db = $database->getConnection(); 

$lesson = new lessons($db);

$lesson->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];

$stmt = $lesson->get();  
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "lesson_title" => $lesson_title,  
            "lesson_file" => $lesson_file,
            "lesson_description" => $lesson_description,
            "module_id" => $module_id,
            "module_title"=>$module_title,
            "status" => $status  ,
            "category_name" => $category_name
        );
        
        array_push($dataArray["body"], $e);
    } 
}

$page['datatable'] = true;
 
require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/lessons.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';