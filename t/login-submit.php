<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/teachers.php'; 
require_once DOCUMENT_ROOT.'system/classes/activityLogs.php'; 

if (empty($_POST['email']) || empty($_POST['password'])) {
    $_SESSION['mathapp']['error'] = 'Please enter your email and password'; 
    header('location: '.FILE_BASENAME);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$teacher = new Teachers($db);
$teacher->email = $_POST['email'];  
$teacher->getByEmail(); 

if($teacher->reg_id == null){
    echo $_SESSION['mathapp']['error'] = 'wrong email or password';
    header('location: '.FILE_BASENAME);
    exit;
}
 
// if($teacher->status != 1){
//     echo $_SESSION['mathapp']['error'] = 'invalid request';
//     header('location: '.FILE_BASENAME);
//     exit;
// } 

if($teacher->reg_id != null AND $teacher->status==2)
{
    $_SESSION['mathapp']['resend']['teacher'] = $teacher->reg_id;
    $_SESSION['mathapp']['email'] = '';
    $_SESSION['mathapp']['error'] = 'unverified account - <br> <a href="resend.php">Click here resend email verification</a>';
    header('location: '.FILE_BASENAME);
    exit;
}  


if($teacher->reg_id != null AND $teacher->status==0)
{ 
    $_SESSION['mathapp']['error'] = 'invalid request';
    header('location: '.FILE_BASENAME);
    exit;
}  


if(!password_verify($_POST['password'], $teacher->password)){
    echo $_SESSION['mathapp']['error'] = 'wrong email or password';
    header('location: '.FILE_BASENAME);
    exit;
}
 

$logs = new ActivityLogs($db);
$logs->account_type = 'teacher'; 
$logs->account_id = $teacher->reg_id;   
$logs->activity = 'teacher login'; 
$logs->description = REQUEST_URI;
$logs->ip_address = '';
$logs->create(); 

$_SESSION['mathapp']['login']['teacher'] = $teacher->reg_id;
$_SESSION['mathapp']['login']['account_name'] = $teacher->first_name.' '.$teacher->last_name;

header('location: '.TEACHER_DASHBOARD);