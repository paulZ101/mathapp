<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/modules.php'; 

$page['title'] = 'Modules';
$page['label'] = "module";

authorize('teacher');

$database = new Database();
$db = $database->getConnection(); 

$module = new Modules($db);

$module->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];

$stmt = $module->get();  
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "module_title" => $module_title, 
            "category_name" => $category_name, 
            "status" => $status 
        );
        
        array_push($dataArray["body"], $e);
    } 
}

$page['datatable'] = true;

require_once DOCUMENT_ROOT . 'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT . 'system/pages/teacher/modules.php';
require_once DOCUMENT_ROOT . 'system/pages/teacher/footer.php';