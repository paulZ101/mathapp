<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/numeracyQuestions.php'; 

$page['title'] = 'Numeracy Questions';
$page['label'] = 'numeracy_question';

authorize('teacher');

$database = new Database();
$db = $database->getConnection(); 

$question = new numeracyQuestions($db);

$question->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];

$stmt = $question->get();  
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "number1" => $number1, 
            "number2" => $number2, 
            "answer" => $answer, 
            "operator" => $operator,
            "numeracy" => $numeracy,
            "status" => $status 
        );
        
        array_push($dataArray["body"], $e);
    } 
}
 
$page['datatable'] = true;

require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/numeracy_questions.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';