<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/numeracyResults.php'; 

$page['title'] = 'Pupils Numeracy Profile';
$label = 'results';

authorize('teacher'); 

$database = new Database();
$db = $database->getConnection(); 

$numeracyResults = new numeracyResults($db);



if(!empty($_GET['sid'])){
    $decryptedID = encrypt_decrypt('decrypt', $_GET['sid']);
    $numeracyResults->section_id = $decryptedID;
}
 
$numeracyResults->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];

$stmt = $numeracyResults->get();  
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
 

        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "first_name" => $first_name,
            "last_name" => $last_name,
            "section_name" => $section_name,
            "right_answer" => $right_answer,
            "addition" => $addition,
            "subtraction" => $subtraction,
            "multiplication" => $multiplication,
            "division" => $division
        );


        // ,
        //     "right_answer" => $right_answer,
        //     "wrong_answer" => $wrong_answer,
        //     "date_added " => $date_added  


        // // , 
        //     "wrong_answer" => $wrong_answer, 
        //     "unanswered" => $unanswered, 
        //     "username" => $username,

        //     "module_title" => $module_title,
        //     "lesson_title" => $lesson_title,
        //     "section_name" => $section_name,
        //     "date_added" => $date_added,
        //     "category_name" => $category_name 
        
        array_push($dataArray["body"], $e);
    } 
}

 
// $page['datatable'] = true;

require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/numeracy_results.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';
