<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 

$page['title'] = 'Pupils';
$page['label'] = 'pupil';

authorize('teacher');

$database = new Database();
$db = $database->getConnection(); 

$pupil = new Pupils($db);


if(!empty($_GET['sid'])){
    $decryptedID = encrypt_decrypt('decrypt', $_GET['sid']);
    $pupil->section_id = $decryptedID;
}


$pupil->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];

$stmt = $pupil->get();  
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "first_name" => $first_name,  
            "last_name" => $last_name,  
            "student_id" => $student_id,  
            "username" => $username,  
            "status" => $status,
            "section_name" => $section_name

            
        );
        
        array_push($dataArray["body"], $e);
    } 
}


$page['datatable'] = true;

require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/pupils.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';