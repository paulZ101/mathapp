<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/questions.php'; 

$page['title'] = 'Questions';
$page['label'] = 'question';

authorize('teacher');

$database = new Database();
$db = $database->getConnection(); 

$question = new Questions($db);

$question->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];

// if(isset($_GET['lid'])){
//     $decryptedID = encrypt_decrypt('decrypt', $_GET['lid']);
//     $question->lesson_id = $decryptedID;
// }

$stmt = $question->get();  
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "question" => $question, 
            "lesson_title" => $lesson_title, 
            "module_title" => $module_title,
            "category_name" => $category_name,
            "status" => $status 
        );
        
        array_push($dataArray["body"], $e);
    } 
}

$page['datatable'] = true;

require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/questions.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';