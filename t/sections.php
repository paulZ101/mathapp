<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/sections.php'; 

$page['title'] = 'Sections';
$page['label'] = 'section';

authorize('teacher');

$database = new Database();
$db = $database->getConnection(); 

$section = new Sections($db);

$section->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];

$stmt = $section->get();  
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "section_name" => $section_name, 
            "section_code" => $section_code, 
            "q1" => $q1, 
            "q2" => $q2, 
            "q3" => $q3, 
            "q4" => $q4, 
            "n1" => $n1, 
            "n2" => $n2, 
            "status" => $status 
        );
        
        array_push($dataArray["body"], $e);
    } 
}

$page['datatable'] = true;

require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/sections.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';