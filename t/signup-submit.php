<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php'; 
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/teachers.php'; 
require_once DOCUMENT_ROOT.'system/classes/emailNotification.php'; 
require_once DOCUMENT_ROOT.'assets/vendor/PHPMailer/PHPMailerAutoload.php'; 
 

if(
	empty($_POST['first_name']) || 
	empty($_POST['last_name']) || 
	empty($_POST['email']) || 
	empty($_POST['password'])
){
    $_SESSION['mathapp']['error'] = 'All field is required!';
    header('location: '.FILE_BASENAME);
    exit;
}


if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
    $_SESSION['mathapp']['error'] = 'Invalid Email Address';
    header('location: '.FILE_BASENAME);
    exit;
}


$database = new Database(); 
$db = $database->getConnection();

$teacher = new Teachers($db);
$teacher->email = $_POST['email'];  

$teacher->getByEmail();

if($teacher->reg_id != null AND $teacher->status==2)
{
    $_SESSION['mathapp']['resend']['teacher'] = $teacher->reg_id;
    $_SESSION['mathapp']['email'] = $teacher->email;
    // $_SESSION['mathapp']['error'] = '<a href="resend.php">Resend</a>';
	$_SESSION['mathapp']['error'] = 'unverified account - <br> <a href="resend.php">Click here resend email verification</a>';
    
    header('location: '.FILE_BASENAME);
    exit;
}  

if($teacher->first_name != null AND $teacher->status==1)
{

    $_SESSION['mathapp']['error'] = 'Email Address Already Exists';
    header('location: signup.php');
    exit;

} 


if($teacher->first_name != null AND $teacher->status==0)
{

    $_SESSION['mathapp']['error'] = 'Invalid Request';
    header('location: signup.php');
    exit;

} 

 

$teacher->email = $_POST['email'];
$teacher->first_name = $_POST['first_name'];
$teacher->last_name =  $_POST['last_name'];
$teacher->school_reg_id = $_POST['school_id'];  

$teacher->password = password_hash($_POST['password'], PASSWORD_DEFAULT);
$teacher->status = '2';
$teacher->reg_type = 'signup';
$teacher->admin_id = '1';




if ($teacher->create()) { 

    $verification_link = BASE_URL.'t/verify.php?token='.encrypt_decrypt('encrypt',$db->lastInsertId());

	$emailNotification = new emailNotification();
	$emailNotification->token = $verification_link; 

	$emailer = new PHPMailer();
 
	$emailer->IsSMTP();
	$emailer->SMTPAuth  = true;
	$emailer->SMTPSecure = $system_config['email_SMTPSecure']; 
	$emailer->Host       = EMAIL_HOST;
	$emailer->Port       = EMAIL_PORT;
	$emailer->Username   = EMAIL_USERNAME;
	$emailer->Password   = EMAIL_PASSWORD;

	$emailer->From 		= EMAIL_FROM;
	$emailer->FromName  = EMAIL_FROM_NAME;
	$emailer->Subject   = "E-mail Verification - MathApp3.com";
	$emailer->Body      = $emailNotification->emailVerification();
 
	$emailer->IsHTML(true);
	$emailer->AddAddress($teacher->email);
	
	if($emailer->Send()){

		$_SESSION['mathapp']['success'] = 'We have sent an email to '.$teacher->email.' , please click a link to verify your email<br> 
		<br><small>Did not receive an email? <a href="resend.php">Click here to resend email verification</a></small>';
    
		header('location: '.FILE_BASENAME);
		exit;

	}else{
		 
		$_SESSION['mathapp']['error'] = 'ERROR: CANNOT SEND EMAIL USING LOCAL SERVER'; 
		header('location: '.TEACHER_SIGNUP);
		exit;

	}  

}else{

    $_SESSION['mathapp']['error'] = 'Teacher could not be created.';
    header('location: '.FILE_BASENAME);
    exit;

}


exit;

 