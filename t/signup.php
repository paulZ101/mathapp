<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/schools.php'; 

$page['title'] =  'Teacher Sign Up';

$database = new Database();
$db = $database->getConnection(); 

$school = new Schools($db); 
$stmt = $school->getActive();  
$schoolCount = $stmt->rowCount();

$schoolArr = array();
$schoolArr["body"] = array();
$schoolArr["itemCount"] = $schoolCount;

if ($schoolCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "school_name" => $school_name
        );
        
        array_push($schoolArr["body"], $e);
    } 
}



require_once DOCUMENT_ROOT . 'system/pages/teacher/signup.php';