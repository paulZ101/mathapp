<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php'; 
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/teachers.php'; 

$main_page ='index.php';

if(empty($_GET['token'])){
    header('location:'.$main_page);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_GET['token']); 

$database = new Database();  // pag instantiate 
$db = $database->getConnection();  // get database connection

$teacher = new Teachers($db);
$teacher->reg_id = isset($decryptedID) ? $decryptedID : die();  

$teacher->getSingle(); 

if($teacher->first_name != null){ 
    
    if($teacher->status==2){
 
        if($teacher->verify()){
          
            $_SESSION['mathapp']['success'] = 'Your account email is succesfully verified. you can now login</a>'; 
            header('location: '.TEACHER_LOGIN);
            exit;
        
        }  

    }
     
}




$_SESSION['mathapp']['error'] = 'INVALID REQUEST'; 
            header('location: '.TEACHER_LOGIN);
            exit;